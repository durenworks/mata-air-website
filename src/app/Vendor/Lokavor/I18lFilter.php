<?php namespace App\Vendor\Lokavor;

use \NumberFormatter;
use \IntlDateFormatter;
use \Config;

class I18lFilter {
	public static function currency($value, $currency = null, $locale = null)
	{
		if ($locale == null)
			$locale = Config::get('app.locale');

		switch ($currency) {
		case 'id':
		default:
			$currency = 'IDR';
		}	

		$formater = new NumberFormatter($locale, NumberFormatter::CURRENCY);
		return $formater->formatCurrency(intval($value), $currency);
	}

	public static function spellout_number($value, $locale = null)
	{
		if ($locale == null)
			$locale = Config::get('app.locale');
		$formater = new NumberFormatter($locale, NumberFormatter::SPELLOUT);
		// ID_ID JUTA - JUTS BUGS
		if (strtolower(substr($locale, 0, 2)) == 'id')
			return strtr($formater->format($value), array('juts' => 'juta'));
		return $formater->format($value);
	}

	public static function date($value, $pattern = array(), $locale = null, $timezone = null)
	{
		if ($locale == null)
			$locale = Config::get('app.locale');

		if ($timezone == null)
			$timezone = Config::get('app.timezone');

		if (is_array($pattern)) {
			$dateformat = (isset($pattern['date']) ? $pattern['date'] : IntlDateFormatter::MEDIUM);
			$timeformat = (isset($pattern['time']) ? $pattern['time'] : IntlDateFormatter::MEDIUM);
			$formater = new IntlDateFormatter($locale, $dateformat, $timeformat, $timezone);
		} else {
			$formater = new IntlDateFormatter($locale, null, null, $timezone, null, $pattern);
		}

		return $formater->format($value);
	}

	public static function number($value, $pattern = null, $locale = null)
	{
		if ($locale == null)
			$locale = Config::get('app.locale');

		if ($pattern)
			$formater = new NumberFormatter($locale, $pattern);
		else
			$formater = new NumberFormatter($locale, NumberFormatter::DEFAULT_STYLE);

		return $formater->format($value);
	}
}
