<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'slug', 'content', 'published_at', 'category_id', 'status', 'author_id'];

    protected $dates = ['published_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function author() {
        return$this->belongsTo('App\Models\Author');
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    public function scopeApprove($query) {
        return $query->where('status', 'APR');
    }
}
