<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable  implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $appends = ['subscriptionStatus', 'rtrw', 'subscription_expired'];


    protected $fillable = [
        'name', 'email', 'password', 'email_token', 'street', 'residence', 'rw', 'rt', 'village', 'urban_village', 'subdistrict',
        'city', 'province', 'postal_code', 'phone', 'occupation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'expired_date',
        'subscription_expired'
    ];
    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'email'    => "required|email|unique:users,email,$id",
            'password' => 'nullable|confirmed',
            'avatar' => 'image',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
//    public function setPasswordAttribute($value='')
//    {
//        $this->attributes['password'] = bcrypt($value);
//    }
    
    public function getAvatarAttribute($value)
    {
        if (!$value) {
            return 'http://placehold.it/160x160';
        }
    
        return config('variables.avatar.public').$value;
    }
    public function setAvatarAttribute($photo)
    {
        $this->attributes['avatar'] = move_file($photo, 'avatar');
    }

    /*
    |------------------------------------------------------------------------------------
    | Boot
    |------------------------------------------------------------------------------------
    */
//    public static function boot()
//    {
//        parent::boot();
//        static::updating(function($user)
//        {
//            $original = $user->getOriginal();
//
//            if (\Hash::check('', $user->password)) {
//                $user->attributes['password'] = $original['password'];
//            }
//        });
//    }

    public function subscriptions()
    {
        return $this->hasMany('App\Models\Subscriptions', 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id');
    }

    public function getSubscriptionStatusAttribute()
    {
        $today = Carbon::now();
        $subscription = $this->subscriptions->sortByDesc('end_time')->first();

        if ($subscription != null) {
            return $today <= $subscription->end_time;
        }
        else {
            return false;
        }

    }

    public function getSubscriptionStatusTextAttribute()
    {
        $today = Carbon::now();
        $subscription = $this->subscriptions->sortByDesc('end_time')->first();

        if ($subscription != null) {
            if ($today <= $subscription->end_time) {
                return 'AKTIF';
            }
            else {
                return 'EXPIRED';
            }
        }
        else {
            return 'BELUM BERLANGGANAN';
        }

    }

    public function getExpiredDateAttribute()
    {
        //$today = Carbon::now();
        $subscription = $this->subscriptions->sortByDesc('end_time')->first();

        if ($subscription != null) {
            return $subscription->end_time->format('d M Y');
        }
        else {
            return '-';
        }
    }

    public function getSubscriptionExpiredAttribute()
    {
        //$today = Carbon::now();
        $subscription = $this->subscriptions->sortByDesc('end_time')->first();

        if ($subscription != null) {
            return $subscription->end_time;
        }
        else {
            return null;
        }
    }

    public function getRtrwAttribute()
    {
        return $this->rt . ' / ' . $this->rw;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
