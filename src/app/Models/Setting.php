<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key', 'content'
    ];

    public function scopeKey($query, $key) {
        return $query->where('key', $key);
    }
}
