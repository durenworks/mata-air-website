<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function articles() {
        return $this->hasMany('App\Models\Article', 'category_id');
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}
