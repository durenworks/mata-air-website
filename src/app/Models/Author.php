<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['email', 'type', 'name', 'phone_fixed', 'phone_mobile', 'occupation'];

    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'author_id');
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}
