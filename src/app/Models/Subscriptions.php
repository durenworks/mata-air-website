<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    protected $fillable = ['user_id', 'transaction_id', 'start_time', 'end_time', 'street', 'residence', 'rw', 'rt', 'village', 'urban_village', 'subdistrict',
        'city', 'province', 'postal_code', 'phone', 'occupation'];

    protected $dates = [
        'start_time', 'end_time'
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
