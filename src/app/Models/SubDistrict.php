<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

class SubDistrict extends Model
{
    protected $primaryKey = 'subdistrict_id';

    protected $fillable = [
        'subdistrict_id', 'province_id', 'city_id', 'subdistrict_name'
    ];

    public function province() {
        return $this->belongsTo('App\Models\Province', 'province_id', 'province_id');
    }

    public function city() {
        return $this->belongsTo('App\Models\City', 'city_id', 'city_id');
    }

    /**
     * Start of mutator function
     */

    public function getCompleteNameAttribute() {
        $city = $this->city->full_name;

        $completeName = $this->subdistrict_name . ', ' . $city . ', ' . $this->province->province;

        return $completeName;
    }

    /**
     * End of mutator function
     */
}
