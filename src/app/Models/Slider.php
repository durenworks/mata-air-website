<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'image', 'title', 'text', 'input_by', 'is_active', 'last_Edit_by','delete_by'
    ];


    public function scopeActive($query) {
        return $query->where('is_active', true);
    }
}
