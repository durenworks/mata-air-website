<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $primaryKey = 'province_id';

    protected $fillable = [
        'province_id', 'province'
    ];

    public function users() {
        return $this->hasMany('/App/Models/User', 'province_id', 'province_id');
    }

    public function cities() {
        return $this->hasMany('/App/Models/City', 'province_id', 'province_id');
    }

    public function sub_districts() {
        return $this->hasMany('/App/Models/SubDistrict', 'province_id', 'province_id');
    }
}
