<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Province;

class City extends Model
{
    protected $primaryKey = 'city_id';

    protected $fillable = [
        'city_id', 'province_id', 'type', 'city_name', 'postal_code'
    ];

    public function users() {
        return $this->hasMany('App\ModelsUser', 'city_id', 'city_id');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province', 'province_id', 'province_id');
    }

    public function sub_districts() {
        return $this->hasMany('App\Models\SubDistrict', 'city_id', 'city_id');
    }

    /**
     * Start of mutator function
     */

    public function getFullNameAttribute() {
        $fullName = $this->attributes['city_name'];
        if ($this->attributes['type'] != 'Kota') {
            $fullName = $this->attributes['type'] . ' ' . $fullName;
        }
        return $fullName;
    }

    public function getNameWithProvinceAttribute() {
        $city = $this->getFullNameAttribute();

        $cityWithProvince = $city . ', ' . $this->province->province;

        return $cityWithProvince;
    }

    /**
     * End of mutator function
     */

}
