<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MegazineArticle extends Model
{
    //
    protected $fillable = ['title', 'show_order', 'megazine_id', 'doc_id', 'audio_id'];

    public function megazine()
    {
        return $this->belongsTo('App\Models\Megazine', 'megazine_id');
    }

    public function document()
    {
        return $this->belongsTo('App\Models\ArticleFile', 'doc_id');
    }

    public function audio()
    {
        return $this->belongsTo('App\Models\ArticleFile', 'audio_id');
    }
}
