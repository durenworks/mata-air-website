<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['tittle', 'type', 'url', 'is_active'];

    public function items()
    {
        return $this->hasMany('App\Models\GalleryItem', 'gallery_id');
    }
}
