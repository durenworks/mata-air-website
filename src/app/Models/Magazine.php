<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $fillable = ['year', 'roman_edition', 'number_edition', 'published_at', 'cover', 'pdf', 'description'];

    protected $dates = ['published_at'];
    protected $appends = ['file_mame'];

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function getFileNameAttribute()
    {
        return 'Majalah-Mataair-Edisi-' . $this->roman_edition . '-' . $this->year;
    }

    public function articles()
    {
        return $this->hasMany('App\Models\MegazineArticle', 'megazine_id');
    }
}
