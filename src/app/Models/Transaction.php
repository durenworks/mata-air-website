<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'invoice_number',
        'status',
        'price',
        'total',
        'discount',
        'discount_type',
        'paid_at',
        'snap_token',
        'email',
        'handphone'
    ];

    protected $appends = ['status_text'];

    protected $dates = ['paid_at'];

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'campaign_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id', 'id');
    }

    public function scopePaid($query)
    {
        return $query->where('status', 'PAI');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\TransactionAddress', 'transaction_id');
    }

    public function magazines()
    {
        return $this
            ->belongsToMany(
                'App\Models\Magazine',
                'physic_magazine_transaction',
                'transaction_id',
                'magazine_id'
            )
            ->withPivot('qty', 'price', 'status');
    }

    public function getStatusTextAttribute()
    {
        if ($this->status == 'INV') {
            return 'Menunggu Pembayaran';
        } else if ($this->status == 'PAI') {
            return 'Sudah Dibayar';
        } else if ($this->status == 'CHK') {
            return 'Dalam Proses Pemeriksaan';
        } else if ($this->status == 'REJ') {
            return 'Pembayaran Ditolak';
        } else if ($this->status == 'EXP') {
            return 'Pembayaran Kadaluarsa';
        } else {
            return $this->status;
        }
    }
}
