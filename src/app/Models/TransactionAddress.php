<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionAddress extends Model
{
    //
    protected $fillable = [
        'transaction_id',
        'name',
        'street',
        'residence',
        'rt',
        'rw',
        'village',
        'urban_village',
        'subdistrict',
        'city',
        'province',
        'postal_code',
        'handphone',
        'status',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id');
    }
}
