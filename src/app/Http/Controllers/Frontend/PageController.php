<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Magazine;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function index()
    {
        $articles = Article::approve()
            ->active()
            ->latest()
            ->limit(30)
            ->get();
        $categories = Category::active()->orderBy('name')->get();
        $editorPicks = Article::approve()
            ->active()
            ->latest()
            ->limit(6)
            ->get();

        //return response($articles);
        return view('web.index')
            ->with('articles', $articles)
            ->with('editorPicks', $editorPicks)
            ->with('categories', $categories);
    }
    public function edition()
    {
        $magazines = Magazine::active()
            ->orderBy('year', 'desc')
            ->orderBy('number_edition', 'desc')
            ->get();

        //return response($magazines);
        return view('web.edition')
            ->with('magazines', $magazines);
    }
    public function category($slug)
    {
        $articles = Article::wherehas('category', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->active()->get();

        $category = $articles->first()->category;

        return view('web.category')
            ->with('articles', $articles)
            ->with('category', $category);
    }
    public function article($slug)
    {
        $article = Article::where('slug', $slug)->first();

        // get previous user id
        $previous = Article::where('id', '<', $article->id)->max('id');
        if ($previous) {
            $previous = Article::find($previous);
            $previous = $previous->slug;
        } else {
            $previous = '';
        }

        // get next user id
        $next = Article::where('id', '>', $article->id)->min('id');
        if ($next) {
            $next = Article::find($next);
            $next = $next->slug;
        } else {
            $next = '';
        }

        $article->reader_count += 1;
        $article->save();

        return view('web.article_detail')
            ->with('article', $article)
            ->with('next', $next)
            ->with('previous', $previous);
    }
    public function writer($slug)
    {
        return view('web.writer');
    }
    public function readerletter()
    {
        return view('web.reader_letter');
    }
    public function readerletterdetail($slug)
    {
        return view('web.reader_letter_detail');
    }
    public function readerlettersubmit()
    {
        return view('web.reader_letter_agreement');
    }
    public function readerletterform()
    {
        return view('web.reader_letter_form');
    }
    public function gallery()
    {
        $photos = Gallery::where('type', 'image')
            ->where('is_active', 1)
            ->latest()
            ->with('items')
            ->get();

        $videos = Gallery::where('type', 'video')
            ->where('is_active', 1)
            ->latest()
            ->get();

        //return response($photos);
        return view('web.gallery')
            ->with('photos', $photos)
            ->with('videos', $videos);
    }
    public function subscribe()
    {
        $user = User::find(Auth::guard('web')->user()->id);

        return view('web.subscribe')
            ->with('user', $user);
    }
    public function career()
    {
        return view('web.career');
    }
    public function careerform()
    {
        return view('web.career_form');
    }
    public function faq()
    {
        return view('web.faq');
    }
    public function tos()
    {
        return view('web.tos');
    }
    public function contact()
    {
        return view('web.contact');
    }
    public function about()
    {
        return view('web.about');
    }


    public function account()
    {
        $magazines = Magazine::active()
            ->orderby('year')
            ->orderby('number_edition')
            ->get();

        $transactions = Transaction::where('user_id', Auth::guard('web')->user()->id)
            ->latest()
            ->get();

        $user = User::find(Auth::guard('web')->user()->id);

        return view('web.account')
            ->with('magazines', $magazines)
            ->with('transactions', $transactions)
            ->with('user', $user);
    }


    public function login()
    {

        //return view('web.login');
    }
    public function signup()
    {

        return view('web.signup');
    }
    public function resetpassword()
    {

        return view('web.forgot-pass');
    }
}
