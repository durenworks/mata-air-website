<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Magazine;
use App\Models\User;
use DB;
use Session;
use Auth;

class CartController extends Controller
{
  public function index()
  {
    $auth = Auth::guard('web')->user();
    $carts = $this->getSessionCart();
    return view('web.cart')
      ->with('auth', $auth)
      ->with('carts', $carts);
  }

  public function add(Request $request, $id)
  {
    DB::beginTransaction();
    $carts = $this->getSessionCart();
    $qty = $request->qty;

    $key = array_search($id, array_column($carts, 'id'));
    if ($key !== false) {
      $carts[$key]->qty = $carts[$key]->qty + $qty;
    } else {
      $magazine = Magazine::find($id);
      $magazine->qty = $qty;
      $magazine->price = getSubscriptionPrice();
      $carts[] = $magazine;
    }
    $this->addCartSession($carts);
    DB::commit();

    return redirect()->back()
      ->withNotification(true)
      ->withSuccess('success');
  }

  public function addCartSession($magazine)
  {
    Session::put('carts', $magazine);
  }

  static function getSessionCart()
  {
    $carts = Session::get('carts', array());

    return $carts;
  }

  public function removeAll()
  {
    Session::forget('carts');

    return redirect()->route('f.cart');
  }
}
