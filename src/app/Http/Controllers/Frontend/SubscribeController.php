<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Veritrans\Midtrans;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;

class SubscribeController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;

        // Set midtrans configuration
        Midtrans::$serverKey = env('VERITRANS_SERVER_KEY', null);
        //set is production to true for production mode
        Midtrans::$isProduction = env('VERITRANS_PRODUCTION', false);
    }

    public function doCheckout(Request $request){
        $customer = Auth::guard('web')->user();

        \DB::beginTransaction();

        $user = User::find($customer->id);

        $user->name = $request->name;
        $user->street = $request->street;
        $user->residence = $request->residence;
        $user->rw = $request->rw;
        $user->rt = $request->rt;
        $user->village = $request->village;
        $user->urban_village = $request->urban_village;
        $user->subdistrict = $request->subdistrict;
        $user->city = $request->city;
        $user->province = $request->province;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;
        $user->occupation = $request->occupation;

        $user->save();

        $transaction = new Transaction();

        $transaction->fill([
            'user_id' => $customer->id,
            'invoice_number' => Carbon::now()->format('YmdHi'),
            'status' => 'INV',
            'price' => getSubscriptionPrice(),
            'total' => getSubscriptionPrice()
        ]);

        //$transaction->save();
        //order number
        $now = Carbon::now()->format('YmdHi');
        $transaction->invoice_number = 'INV' . $now . substr('00' . e($customer->id), -2, 2);

        //return response()->json($transaction);
        $transaction->save();
        //$transaction->sendNotif();

        $payload = [
            'transaction_details' => [
                'order_id'      => $transaction->invoice_number,
                'gross_amount'  => $transaction->total,
            ],
            'customer_details' => [
                'first_name'    => $customer->name,
                'email'         => $customer->email,
                'phone'         => $customer->phone,
            ],
            'item_details' => [
                [
                    'id'       => '1',
                    'price'    => $transaction->price,
                    'quantity' => 1,
                    'name'     => 'subscription'
                ]
            ]
        ];
//        $midtrans = new Midtrans;
//        $midtrans::$serverKey = env('VERITRANS_SERVER_KEY', null);
//        $midtrans::$isProduction = env('VERITRANS_PRODUCTION', false);

        //var_dump( env('VERITRANS_SERVER_KEY', null));die();

        //return response()->json($midtrans);
        $snapToken = Midtrans::getSnapToken($payload);
        $transaction->snap_token = $snapToken;
        $transaction->save();

        // Beri response snap token
        $this->response['snap_token'] = $snapToken;

        \DB::commit();

        return response()->json($this->response);
    }
}
