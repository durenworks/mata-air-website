<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Author;
use App\Models\Article;
use App\Models\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserArticle\StoreRequest as userArticleRequest;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    public function storeArticle(userArticleRequest $request) {

        //var_dump('foo');die();
        $author = Author::where('email', $request->email)->first();

        if (empty($author)) {
            $author = new Author();

            $author->email = $request->email;
            $author->type = 'USR';
            $author->name = $request->name;
            $author->phone_fixed = $request->phone_fixed;
            $author->phone_mobile = $request->phone_mobile;
            $author->occupation = $request->occupation;

            $author->save();
        }

        $author_id = $author->id;

        $slug = str_slug($request->title);

        if ($count = Article::where('slug', 'like', "$slug%")->count())
            $slug = str_finish($slug, "-$count");

        $item = new Article();
        $item->fill([
            'title' => $request->title,
            'slug' => $slug,
            'category_id' => $request->category,
            'status' => 'APR',
            'author_id' => $author_id,
            'published_at' => Carbon::now()
        ]);

        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $item->thumbnail = upload_image($file, 'articles');;
        }

        $item->content = extractSummernoteContent($request->content);

        $item->save();

        return redirect(route('home'));
    }

    public function storeProfile(Request $request)
    {
        $user = User::find(Auth::guard('web')->user()->id);

        $user->name = $request->name;
        $user->street = $request->street;
        $user->residence = $request->residence;
        $user->rw = $request->rw;
        $user->rt = $request->rt;
        $user->village = $request->village;
        $user->urban_village = $request->urban_village;
        $user->subdistrict = $request->subdistrict;
        $user->city = $request->city;
        $user->province = $request->province;
        $user->postal_code = $request->postal_code;
        $user->phone = $request->phone;
        $user->occupation = $request->occupation;

        $user->save();

        return redirect(route('f.account'));
    }
}
