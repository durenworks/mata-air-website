<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionAddress;
use App\Imports\AddressImport;
use Maatwebsite\Excel\Facades\Excel;

use Session;
use stdClass;
use DB;
use Auth;
use Carbon\Carbon;
use App\Veritrans\Midtrans;

class CheckoutController extends Controller
{

  public function __construct(Request $request)
  {
    $this->request = $request;

    // Set midtrans configuration
    Midtrans::$serverKey = env('VERITRANS_SERVER_KEY', null);
    //set is production to true for production mode
    Midtrans::$isProduction = env('VERITRANS_PRODUCTION', false);
  }

  public function index()
  {
    $carts = $this->getSessionCart();
    $addresses = $this->getSessionAddresses();
    return view('web.checkout')
      ->with('carts', $carts)
      ->with('addresses', $addresses);
  }

  static function getSessionCart()
  {
    $carts = Session::get('carts', array());

    return $carts;
  }

  public function storeEmail(Request $request)
  {
    $store = new stdClass();
    $store->email = $request->email;
    $store->phone = $request->phone;

    Session::put('buyer', $store);

    return redirect()->route('f.checkout');
  }

  public function doCheckout(Request $request)
  {
    DB::beginTransaction();
    $user_id = null;
    $auth = Auth::guard('web')->user();
    if ($auth) {
      $user = User::find($auth->id);
      $user_id = $auth->id;
      $user_name = $user->name;
    } else {
      $user = Session::get('buyer');
      $user_name = $user->email;
    }
    $transaction = new Transaction();
    $transaction->fill([
      'user_id' => $user_id,
      'invoice_number' => Carbon::now()->format('YmdHi'),
      'status' => 'INV',
      'price' => 0,
      'total' => 0,
      'email' => $user->email,
      'handphone' => $user->phone,
    ]);

    $transaction->save();

    $carts = $this->getSessionCart();
    $items = [];
    $total = 0;
    foreach ($carts as $key => $cart) {
      # code...
      $price = getSubscriptionPrice();
      $subtotal = $cart->qty * $price;
      $total += $subtotal;
      $magazine = [
        'price' => $price,
        'qty' => $cart->qty,
      ];
      $transaction->magazines()->attach($cart->id, $magazine);
      $item = [
        'id'       => $key + 1,
        'price'    => $price,
        'quantity' => $cart->qty,
        'name'     => 'Edisi ' . $cart->roman_edition . ' tahun ' . $cart->year,
      ];
      $items[] = $item;
    }

    $payload = [
      'transaction_details' => [
        'order_id'      => $transaction->invoice_number,
        'gross_amount'  => $total,
      ],
      'customer_details' => [
        'first_name'    => $user_name,
        'email'         => $user->email,
        'phone'         => $user->phone,
      ],
      'item_details' => $items
    ];

    // var_dump($payload);
    // die;

    $snapToken = Midtrans::getSnapToken($payload);
    $transaction->snap_token = $snapToken;
    $transaction->total = $total;
    $transaction->save();

    $addresses = json_decode($request->addresses);
    foreach ($addresses as $key => $address) {
      $transactionAddress = new TransactionAddress();
      $transactionAddress->fill([
        'transaction_id' => $transaction->id,
        'name' => $address->name,
        'street' => $address->street,
        'residence' => $address->residence,
        'rt' => $address->rt,
        'rw' => $address->rw,
        'village' => $address->village,
        'urban_village' => $address->urban_village,
        'subdistrict' => $address->subdistrict,
        'city' => $address->city,
        'province'  => $address->province,
        'postal_code' => $address->postal_code,
        'handphone' => $address->handphone,
      ]);
      $transactionAddress->save();
    }

    Session::forget('carts');
    Session::forget('addresses');
    Session::forget('buyer');
    DB::commit();

    return redirect()->route('f.checkout.payment', $transaction->invoice_number);
  }

  public function address()
  {
    $addresses = $this->getSessionAddresses();
    return view('web.checkout_address')
      ->with('addresses', $addresses);
  }

  public function removeAddress()
  {
    Session::forget('addresses');

    return redirect()->back();
  }

  static function getSessionAddresses()
  {
    $addresses = Session::get('addresses', array());

    return $addresses;
  }

  public function importAddress(Request $request)
  {
    Excel::import(new AddressImport, $request->file('file'));

    return back();
  }

  public function payment($invoice_number)
  {
    $transaction = Transaction::where('invoice_number', $invoice_number)->first();

    $magazines = $transaction->magazines()->get();
    $addresses = $transaction->addresses()->get();

    return view('web.checkout_payment')
      ->with('transaction', $transaction)
      ->with('magazines', $magazines)
      ->with('addresses', $addresses);
  }
}
