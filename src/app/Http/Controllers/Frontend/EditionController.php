<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Magazine;

class EditionController extends Controller
{
  public function index(Request $request)
  {
    $latestMagz = Magazine::active()
      ->orderBy('year', 'desc')
      ->orderBy('number_edition', 'desc')
      ->first();

    $yearFilter = $request->year;
    $editionFilter = $request->edition;
    $magazines = new Magazine();
    if ($yearFilter && $editionFilter) {
      $magazines = $magazines
        ->where('year', intval($yearFilter))
        ->where('number_edition', intval($editionFilter));
    } else if ($yearFilter) {
      $magazines = $magazines
        ->where('year', intval($yearFilter));
    } else if ($editionFilter) {
      $magazines = $magazines
        ->where('number_edition', intval($editionFilter));
    }
    $magazines = $magazines->active()
      ->orderBy('year', 'desc')
      ->orderBy('number_edition', 'desc')
      ->paginate(4);
    $years = Magazine::select('year')->distinct()->get();
    $editions = Magazine::select('number_edition')->distinct()->get();

    //return response($magazines);
    return view('web.edition')
      ->with('latestMagz', $latestMagz)
      ->with('magazines', $magazines)
      ->with('years', $years)
      ->with('editions', $editions);
  }
}
