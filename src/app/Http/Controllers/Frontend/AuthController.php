<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Register\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Jobs\SendVerificationEmail;
use Illuminate\Http\Request;
use App\Http\Requests;
use Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => [
            'getLogout',
            'doRegister'
        ]]);
    }

    public function doRegister(Request $request)
    {
        //var_dump('foo');die();
        $user = new User();

        $user->fill([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_token' => base64_encode($request->email)
        ]);

        //return response($request->password);
        if ($user->save()) {
            dispatch(new SendVerificationEmail($user));
        }


        return redirect('/')
            ->with('show_login', true);

    }

    public function activate($token)
    {
        $user = User::where('email_token', $token)->first();
        $user->is_active = 1;
        $user->save();

        return redirect()->route('login');
    }
}
