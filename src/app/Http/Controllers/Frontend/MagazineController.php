<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Magazine;

class MagazineController extends Controller
{
  public function index(Request $request, $id)
  {
    $magazine = Magazine::find($id);

    $yearFilter = $request->year;
    $editionFilter = $request->edition;
    $magazines = new Magazine();
    if ($yearFilter && $editionFilter) {
      $magazines = $magazines
        ->where('year', intval($yearFilter))
        ->where('number_edition', intval($editionFilter));
    } else if ($yearFilter) {
      $magazines = $magazines
        ->where('year', intval($yearFilter));
    } else if ($editionFilter) {
      $magazines = $magazines
        ->where('number_edition', intval($editionFilter));
    }
    $magazines = $magazines->where('id', '!=', $id)->active()
      ->orderBy('year', 'desc')
      ->orderBy('number_edition', 'desc')
      ->paginate(3);

    $years = Magazine::select('year')->distinct()->get();
    $editions = Magazine::select('number_edition')->distinct()->get();
    //return response($magazines);
    return view('web.magazine_detail')
      ->with('magazine', $magazine)
      ->with('magazines', $magazines)
      ->with('years', $years)
      ->with('editions', $editions);
  }
}
