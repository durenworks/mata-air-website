<?php

/**
 * Created by PhpStorm.
 * User: Dadittya
 * Date: 15/12/2018
 * Time: 22:32
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Magazine;
use Illuminate\Support\Facades\Config;
use App\Models\Publication;

class FileController extends Controller
{
    public function showImage($location, $filename)
    {
        $location = 'storage.' . $location;
        $path = Config::get($location);

        ob_end_clean();
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function downloadPDF($id)
    {
        $magazine = Magazine::findorfail($id);

        $path = Config::get('storage.magazines_file');
        //var_dump($magazine->file_name);die();

        $fileName = $magazine->pdf;
        ob_end_clean();
        $resp = response()->download($path . '/' . $fileName, $magazine->file_name);

        return $resp;
    }

    public function articleDoc($filename)
    {

        $path = Config::get('storage.article_file');
        //var_dump($magazine->file_name);die();
        ob_end_clean();
        $resp = response()->download($path . '/' . $filename);

        return $resp;
    }

    public function articleAudio($filename)
    {

        $path = Config::get('storage.article_audio');
        //var_dump($magazine->file_name);die();
        ob_end_clean();
        $resp = response()->download($path . '/' . $filename);

        return $resp;
    }
}
