<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\Models\Magazine;
use Illuminate\Support\Facades\Input;
use stdClass;

class MagazineController extends Controller
{
    public $successStatus = 200;

    public function magazineList()
    {
        $limit = Input::get('limit');
        $magazines = Magazine::active()
            ->orderby('year')
            ->orderby('number_edition');
        //
        if ($limit != null) {
            $magazines = $magazines->limit($limit);
        }
        $magazines = $magazines->get();

        //return $magazines;
        $result = array();
        foreach ($magazines as $magazine) {
            $data['id'] = $magazine->id;
            $data['edition'] = $magazine->roman_edition . ' ' . $magazine->year;
            $data['image_url'] = route('show.image', ['magazines_cover', $magazine->cover]);
            $result[] = $data;
        }

        return $result;
    }

    public function subscribePrice()
    {
        $result['subscription_price'] = getSubscriptionPrice();

        return $result;
    }

    public function articleLists($id)
    {
        $magazine = Magazine::find($id);

        if (!$magazine) {
            $error = new StdClass();
            $error->message = 'Data not found!';
            $error->code = 203;
            return response()->json(['error' => $error], 203);
        }

        $articles = $magazine->articles()->select('id', 'title')->get();

        return response()->json(['results' => $articles], $this->successStatus);
    }

    public function article($id, $article_id)
    {
        $magazine = Magazine::find($id);

        if (!$magazine) {
            $error = new StdClass();
            $error->message = 'Data not found!';
            $error->code = 203;
            return response()->json(['error' => $error], 203);
        }

        $article = $magazine->articles()->find($article_id);

        if (!$article) {
            $error = new StdClass();
            $error->message = 'Data not found!';
            $error->code = 203;
            return response()->json(['error' => $error], 203);
        }

        $doc = $article->document;
        $audio = $article->audio;
        $res = new stdClass();
        $res->title = $article->title;
        $res->doc_url = route('media.article_doc', $doc->file_name);
        $res->audio_url = route('media.article_audio', $audio->file_name);

        return response()->json(['results' => $res], $this->successStatus);
    }
}
