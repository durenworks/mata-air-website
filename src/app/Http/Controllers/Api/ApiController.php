<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RegisterAuthRequest;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Magazine;
use Carbon\Carbon;
use App\Veritrans\Midtrans;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;
    public function __construct()
    {
        // Set midtrans configuration

        Midtrans::$serverKey = env('VERITRANS_SERVER_KEY', null);
        //set is production to true for production mode
        Midtrans::$isProduction = env('VERITRANS_PRODUCTION', false);
    }

    public function register(Request $request)
    {
        $user= User::where('email', $request->email)->first();

        if (!$user) {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            if ($this->loginAfterSignUp) {
                return $this->login($request);
            }

            return response()->json([
                'success' => true,
                'data' => $user
            ], 200);
        }
        else {
            return response()->json([
                'success' => false,
                'data' => 'User already registered'
            ], 401);
        }

    }

    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        //var_dump('foo');die();
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            $user = JWTAuth::authenticate($request->token);

            return response()->json(['user' => $user]);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        }

    }

    public function getMagazineList(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);

        if ($user->subscriptionStatus) {
            $magazines = Magazine::active()
                ->orderby('year')
                ->orderby('number_edition');
            //
//            if ($limit != null) {
//                $magazines = $magazines->limit($limit);
//            }
            $magazines = $magazines->get();

            //return $magazines;
            $result = array();
            foreach ($magazines as $magazine) {
                $data['id'] = $magazine->id;
                $data['edition'] = $magazine->roman_edition . ' ' . $magazine->year;
                $data['image_url'] = route('show.image', ['magazines_cover', $magazine->cover]);
                $data['download_url'] = route('download.file', [$magazine->id]);
                $result[] = $data;
            }

            return $result;
        }
        else {
            return null;
        }
    }

    public function updateProfile(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);

        $user = User::find($user->id);
        if (isset($request->nama)) {
            $user->name = $request->nama;
        }
        if (isset($request->phone)) {
            $user->phone = $request->phone;
        }
        if (isset($request->alamat)) {
            $user->residence = $request->alamat;
        }
        if (isset($request->perumahan)) {
            $user->residence = $request->perumahan;
        }
        if (isset($request->provinsi)) {
            $user->province = $request->provinsi;
        }
        if (isset($request->kota)) {
            $user->city = $request->kota;
        }
        if (isset($request->kecamatan)) {
            $user->subdistrict = $request->kecamatan;
        }
        if (isset($request->kelurahan)) {
            $user->urban_village = $request->kelurahan;
        }
        if (isset($request->kodepos)) {
            $user->postal_code = $request->kodepos;
        }

        if ($user->save()) {
            return response()->json([
                'success' => true,
                'data' => $user
            ], 200);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Failed to Update Profil'
            ], 500);
        }
    }

    public function transactionHistory(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);

        try {
            $transactions = Transaction::where('user_id', $user->id)
                ->orderby('created_at', 'desc')
                ->get();

            $result = array();
            foreach ($transactions as $transaction) {
                $data['id'] = $transaction->id;
                $data['invoice_number'] = $transaction->invoice_number;
                $data['invoice_date'] = indonesianDate($transaction->created_at);
                $data['status'] = $transaction->status_text;
                $data['total']  = $transaction->total;
                $data['paid_at'] = indonesianDate($transaction->paid_at);
                $data['midtrans_snap_token'] = $transaction->snap_token;

                $result[] = $data;
            }

            return response()->json([
                'success' => true,
                'data' => $result
            ], 200);


        }
        catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Failed to get data'
            ], 500);
        }
    }


    public function transaction(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);

        $result['total_amount'] = getSubscriptionPrice();
        $result['first_name'] = $user->name;
        $result['last_name'] = '';
        $result['email'] = $user->email;
        $result['phone'] = $user->phone;
        $result['item_id'] = 1;
        $result['item_price'] = getSubscriptionPrice();
        $result['item_name'] = 'Berlangganan 1 Tahun Majalah Digital Mataair';
        $result['item_quantity'] = 1;



        return response()->json([
            'success' => true,
            'data' => $result
        ], 200);
    }

    public function charge(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);

        \DB::beginTransaction();

        $transaction = new Transaction();

        $transaction->fill([
            'user_id' => $user->id,
            'invoice_number' => Carbon::now()->format('YmdHi'),
            'status' => 'INV',
            'price' => getSubscriptionPrice(),
            'total' => getSubscriptionPrice()
        ]);

        //$transaction->save();
        //order number
        $now = Carbon::now()->format('YmdHi');
        $transaction->invoice_number = 'INV' . $now . substr('00' . e($user->id), -2, 2);

        //return response()->json($transaction);
        $transaction->save();
        //$transaction->sendNotif();

        $payload = [
            'transaction_details' => [
                'order_id'      => $transaction->invoice_number,
                'gross_amount'  => $transaction->total,
            ],
            'customer_details' => [
                'first_name'    => $user->name,
                'email'         => $user->email,
                'phone'         => $user->phone,
            ],
            'item_details' => [
                [
                    'id'       => '1',
                    'price'    => $transaction->price,
                    'quantity' => 1,
                    'name'     => 'Berlangganan 1 Tahun Majalah Digital Mataair'
                ]
            ]
        ];

        //return response()->json($midtrans);
        try {
            $snapToken = Midtrans::getSnapToken($payload);

            $url = Midtrans::getBaseUrl() . '/vtweb/' . $snapToken;
            $url = str_replace('v1', 'v2', $url);
        }
        catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Failed Connect to Midtrans Server'
            ], 500);
        }

        $transaction->snap_token = $snapToken;
        $transaction->save();

        // Beri response snap token
        //$this->response['snap_token'] = $snapToken;

        \DB::commit();

        return response()->json([
            'token' => $snapToken,
            'redirect_url' => $url
        ]);
    }
    public function charge2($token)
    {
        $user = JWTAuth::authenticate($token);

        \DB::beginTransaction();

        $transaction = new Transaction();

        $transaction->fill([
            'user_id' => $user->id,
            'invoice_number' => Carbon::now()->format('YmdHi'),
            'status' => 'INV',
            'price' => getSubscriptionPrice(),
            'total' => getSubscriptionPrice()
        ]);

        //$transaction->save();
        //order number
        $now = Carbon::now()->format('YmdHi');
        $transaction->invoice_number = 'INV' . $now . substr('00' . e($user->id), -2, 2);

        //return response()->json($transaction);
        $transaction->save();
        //$transaction->sendNotif();

        $payload = [
            'transaction_details' => [
                'order_id'      => $transaction->invoice_number,
                'gross_amount'  => $transaction->total,
            ],
            'customer_details' => [
                'first_name'    => $user->name,
                'email'         => $user->email,
                'phone'         => $user->phone,
            ],
            'item_details' => [
                [
                    'id'       => '1',
                    'price'    => $transaction->price,
                    'quantity' => 1,
                    'name'     => 'Berlangganan 1 Tahun Majalah Digital Mataair'
                ]
            ]
        ];

        //return response()->json($midtrans);
        try {
            $snapToken = Midtrans::getSnapToken($payload);

            $url = Midtrans::getBaseUrl() . '/vtweb/' . $snapToken;
            $url = str_replace('v1', 'v2', $url);
        }
        catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Failed Connect to Midtrans Server'
            ], 500);
        }

        $transaction->snap_token = $snapToken;
        $transaction->save();

        // Beri response snap token
        //$this->response['snap_token'] = $snapToken;

        \DB::commit();

        return response()->json([
            'token' => $snapToken,
            'redirect_url' => $url
        ]);
    }

    public function transaction_callback(Request $request, $invoice_number)
    {
        $user = JWTAuth::authenticate($request->token);

        $trasaction = Transaction::where('invoice_number', $invoice_number)
                                ->where('user_id', $user->id)
                                ->first();

        $trasaction->snap_token = $request->snap_token;

        if ($trasaction->save()) {
            return response()->json([
                'success' => true,
                'data' => $trasaction
            ], 200);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Failed to Update Transaction'
            ], 500);
        }
    }
}
