<?php

namespace App\Http\Controllers;

class ImageController extends Controller
{
    public function showCampaignImage($filename)
    {
        $path = Config::get('storage.campaign-image');


        ob_end_clean();
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }
}
