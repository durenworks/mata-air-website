<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use StdClass;
use App;
use Config;

class RegionController extends Controller {

	public function getCity(Request $request){
		$aaa = array('city_id' => '0', 'province_id' => '', 'province' => '', 'type' => '', 'city_name' => '-- Select City --', 'postal_code' => '');

		if($request->province == null) {
            $cities = $aaa;
        }
		else{
			$getCity = json_decode(getCity($request->province), true);

			array_unshift( $getCity['rajaongkir']['results'], $aaa );
		
			$cities = [];
			// $cities[0] = ['','','','','-- Select City --',''];
			if($getCity['rajaongkir']['status']['code'] === 200){
                    $cities = $getCity['rajaongkir']['results'];
			}
		}

		$cities = json_encode($cities);
		return $cities;
	}

	public function getSubdistrict(Request $request){
		$aaa = array('subdistrict_id' => '0', 'province_id' => '', 'province' => '', 'city_id' => '', 'city' => '', 'type'=> '', 'subdistrict_name'=> "-- Select Subdistrict --");
      	if($request->city == null)
      		$subdistricts = $aaa;
      	else{
			$getSubdistrict = json_decode(getSubdistrict($request->city), true);

      		array_unshift( $getSubdistrict['rajaongkir']['results'], $aaa );
		
			$subdistricts = [];
			// $cities[0] = ['','','','','-- Select City --',''];
			if($getSubdistrict['rajaongkir']['status']['code'] === 200){
				$subdistricts = $getSubdistrict['rajaongkir']['results'];
			}
      	}

		$subdistricts = json_encode($subdistricts);
		return $subdistricts;
	}

	public function getCost(Request $request){
		$get_origin = intval($request->origin);
		$origin = Origin::find($get_origin);
		if($origin->origin_type == 'city'){
			$origin_id = $origin->city_id;
		}else{
			$origin_id = $origin->subdistrict_id;
		}

		$originType = $origin->origin_type;

		$city = $request->city;
		if($request->subdistrict == '' || $request == null){
			$destination = $request->city;
			$destinationType = 'city';
		}else{
			$destination = $request->subdistrict;
			$destinationType = 'subdistrict';
		}
		$courier = $request->courier;
		$weight  = $request->weight;
		$getCost = json_decode(getCost($origin_id, $originType, $destination, $destinationType, $weight, $courier), true);
		
		$costs = [];
		// $cities[0] = ['','','','','-- Select City --',''];
		if($getCost['rajaongkir']['status']['code'] === 200){
			$costs = $getCost['rajaongkir']['results'];

		}

		// return json_encode($destination);


		$asawau = [];
		foreach ($costs as $cost) {
			# code...
            // var_dump($cost);die;

            if ($cost['costs'] == null) {
                $obj = new StdClass();
                $obj->code = 'null';
                $asawau[] = $obj;
            }
			foreach ($cost['costs'] as $subCosts) {
				# code...
				// var_dump($subCosts);
				foreach ($subCosts['cost'] as $subCost) {
					$obj = new StdClass();
					$obj->code = $cost['code'];
					$obj->name = $cost['name'];
					$obj->service = $subCosts['service'];
					$obj->description = $subCosts['description'];
					$obj->value = $subCost['value'];
					$obj->etd = $subCost['etd'];
					$obj->note = $subCost['note'];
					$asawau[] = $obj;
				}
			}
		}
        //var_dump($cost);die();
		$costs = json_encode($asawau);
		return $costs;
	}

	public function getAddress(Request $request){
		$get_address = intval($request->id);
		$address = CustomerAddress::find($get_address);

		$getCity = json_decode(getCity($address->lv1_region_id), true);
		$aaa = array('city_id' => '', 'province_id' => '', 'province' => '', 'type' => '', 'city_name' => '-- Select City --', 'postal_code' => '');
      	array_unshift( $getCity['rajaongkir']['results'], $aaa );
		
		$cities = [];
		// $cities[0] = ['','','','','-- Select City --',''];
		if($getCity['rajaongkir']['status']['code'] === 200){
			$cities = $getCity['rajaongkir']['results'];
		}

		$getSubdistrict = json_decode(getSubdistrict($address->lv2_region_id), true);
		$aaa = array('subdistrict_id' => '', 'province_id' => '', 'province' => '', 'city_id' => '', 'city' => '', 'type'=> '', 'subdistrict_name'=> "-- Select Subdistrict");
      	array_unshift( $getSubdistrict['rajaongkir']['results'], $aaa );
		
		$subdistricts = [];
		// $cities[0] = ['','','','','-- Select City --',''];
		if($getSubdistrict['rajaongkir']['status']['code'] === 200){
			$subdistricts = $getSubdistrict['rajaongkir']['results'];
		}

		$data = [];
		$data['address'] = $address;
		$data['cities'] = $cities; 
		$data['subdistricts'] = $subdistricts; 
		$data = json_encode($data);
		return $data;
	}

	public function getCityFromDb(Request $request) {

        $all = array('id' => 'All', 'province_id' => '', 'type' => '', 'city_name' => 'All', 'postal_code' => '', 'created_at' => '', 'updated_at' => '');
	    $cities = getCityFromDb($request->province);
	    $cities = $cities->toArray();
        array_unshift( $cities, $all );
        return json_encode($cities);
    }

    public function getSubDistrictFromDb(Request $request) {
        $all = array('id' => 'All', 'province_id' => '', 'city_id' => '', 'subdistrict_name' => 'All', 'created_at' => '', 'updated_at' => '');
	    $subDistricts = getSubDistrictFromDb($request->city);
        $subDistricts = $subDistricts->toArray();
        array_unshift( $subDistricts, $all );

	    return json_encode($subDistricts);

    }
}
