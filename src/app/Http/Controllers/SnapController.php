<?php namespace App\Http\Controllers;

use App\Models\Subscriptions;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Veritrans\Midtrans;
use App\Models\CustomerRace;
use App\Models\Race;
use App\Models\Activity;
use App\Models\Customer;
use Carbon\Carbon;

use Auth;

class SnapController extends Controller
{
    public function __construct()
    {   
        Midtrans::$serverKey = env('VERITRANS_SERVER_KEY', null);
        //set is production to true for production mode
        Midtrans::$isProduction = env('VERITRANS_PRODUCTION', false);

        /*$this->middleware('auth', ['only' => [
            'snap',
            'token',
            'finish',
            'notification'
        ]]);*/
        // $this->middleware('session.id');
    }

    public function snap(Request $request, $id)
    {
        $transaction = Transaction::find(intval($id));
        if(count($transaction) == 0){
            return redirect()->action('HomeController@index');
        }

        return redirect()->action('AccountController@detailRace', $customer_race->order_number);
    }

    public function token(Request $request, $id) 
    {
        $customer_race = CustomerRace::find($id);
        $customer = Customer::find($customer_race->customer_id);
        // error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;

        $transaction_details = [
            'order_id' => $customer_race->order_number,
            'gross_amount' => floor($customer_race->total),
        ];

        // Populate items
        $items = [];
        $total = 0;

        $subtotal = ceil($customer_race->price - ($customer_race->discount));
        $items[] = [
            'id' => $customer_race->id,
            'price' => intval($subtotal),
            'quantity' => 1,
            'name' => substr($customer_race->race->name, 0, 50)
        ];

        $donation_amount = 0;
        if ($customer_race->foundation_id != '') {
            $items[] = [
                'id' => $customer_race->foundation_id,
                'price' => intval($customer_race->donation_amount),
                'quantity' => 1,
                'name' => substr('Extra Donation To ' . $customer_race->foundation->name, 0, 50)
            ];
            $donation_amount = $customer_race->donation;
            //array_push($items, $donation);
        }


        $total += $subtotal * 1;
        $total += $donation_amount;
        
        $voucher_disc = $customer_race->voucher_discount;
        $disc_name = 'Voucher Discount ('.$customer_race->voucher_promo.')';
        $items[] = [
            'id' => 'voucher_discount',
            'price' => -(int)$voucher_disc,
            'quantity' => 1,
            'name' => str_limit($disc_name, 45, '...')
        ];
        $total -= $voucher_disc;
        $items[] = [
            'id' => 'shipping_price',
            'price' => (int)$customer_race->shipping_cost,
            'quantity' => 1,
            'name' => 'SHIPPING PRICE'
        ];
        $subtotal = (int)$customer_race->shipping_cost;

        $total += $subtotal;


        $bill = explode(', ', $customer_race->bill_to_address);
        $postal_code = end($bill);
        $addr = array_pop($bill);
        $bill_addr = implode(', ', $bill);
        $billing_address = array(
            'first_name'        => $customer->first_name,
            'last_name'         => $customer->last_name,
            'address'           => $bill_addr,
            'city'                  => "",
            'postal_code'   => $postal_code,
            'phone'                 => $customer->phone,
            'country_code'  => 'IDN'
            );

        $ship = explode(', ', $customer_race->ship_to_address);
        $postal_code = end($ship);
        $addr = array_pop($ship);
        $ship_addr = implode(', ', $ship);

        // Populate customer's shipping address
        $shipping_address = array(
            'first_name'    => $customer->first_name,
            'last_name'     => $customer->last_name,
            'address'       => $ship_addr,
            'city'              => "",
            'postal_code' => $postal_code,
            'phone'             => $customer->phone,
            'country_code'=> 'IDN'
            );

        // Populate customer's Info
        $customer_details = array(
            'first_name' => $customer->first_name,
            'last_name' => "",
            'email' => $customer->email,
            'phone' => $customer->phone,
            'billing_address' => $billing_address,
            'shipping_address'=> $shipping_address
            );

        //var_dump(json_encode($items));die;

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

/*        return response()->json($items);
        var_dump($items);die;*/
        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );


    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            echo $snap_token;
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        //$result = $request->input();
        $result = file_get_contents('php://input');
        //var_dump($result);die;
        $order_id = $request->input('order_id');
        //var_dump($order_id);die();
        $result = json_decode($result);
        $_transaction = Transaction::where('invoice_number', $order_id)->first();

        //var_dump($result);die();
        /*$user['id'] = Auth::user()->id;
        $user['first_name'] = Auth::user()->first_name;
        $user['last_name'] = Auth::user()->last_name;*/

        if($result->status_code == 200){
            $_transaction->status = 'PAI';
            $_transaction->paid_amount = $result->gross_amount;
            $_transaction->paid_at = Carbon::now();
            $this->subscribe($_transaction->user_id, $_transaction->id);
            // Activity::save_activity('UPDATE.SUCCESS|ORDER', $user, $order, null, $order, 'customers');
            //$this->setPoint($customer_race->customer_id, $customer_race->race_id, $customer_race->race_race_type_id, $customer_race->referal_code);
            $_transaction->save();
            //$customer_race->sendNotif();
        }else if($result->status_code == 201){
           /* $customer_race->status = 'CHK';
            // $customer_race->paid_amount = $result->gross_amount;
            $customer_race->paid_at = Carbon::now();
            // Activity::save_activity('UPDATE.SUCCESS|ORDER', $user, $order, null, $order, 'customers');
            $customer_race->save();
            $customer_race->sendNotif();*/
        }else{
            $_transaction->status = 'REJ';
            // Activity::save_activity('UPDATE.SUCCESS|ORDER', $user, $order, null, $order, 'customers');
            $_transaction->save();
            //$customer_race->sendNotif();
        }
        // echo $result->status_message . '<br>';
        // echo 'RESULT <br><pre>';
        
        // echo '</pre>' ;

        return redirect()->route('f.account')
                        ->with('fromPayment', true);
    }

    public function notification()
    {
        $midtrans = new Midtrans;
        //echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if($result){
            //$notif = $midtrans->status($result->order_id);
            //return response()->json($result);
        }
        else{
            return false;
        }

        $transaction = $result->transaction_status;
        $type = $result->payment_type;
        $order_id = $result->order_id;
        $fraud = $result->fraud_status;
        $_transaction = Transaction::where('invoice_number', $order_id)->first();

        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card'){
                if($fraud == 'challenge'){
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $_transaction->status = 'CHK';
                    $_transaction->paid_at = Carbon::now();
                    $_transaction->save();
                    //$_transaction->sendNotif();
                } 
                else {
                    $_transaction->status = 'PAI';
                    $_transaction->paid_amount = $result->gross_amount;
                    $_transaction->paid_at = Carbon::now();
                    //$this->setPoint($customer_race->customer_id, $customer_race->race_id, $customer_race->race_race_type_id, $customer_race->referal_code);
                    $_transaction->save();

                    $this->subscribe($_transaction->user_id, $_transaction->id);
                    //$_transaction->sendNotif();
                }
            }
        }

        else if ($transaction == 'settlement'){
          // TODO set payment status in merchant's database to 'Settlement'
            $_transaction->status = 'PAI';
            $_transaction->paid_amount = $result->gross_amount;
            $_transaction->paid_at = Carbon::now();
            // Activity::save_activity('UPDATE.SUCCESS|ORDER', $user, $order, null, $order, 'customers');
            //$this->setPoint($customer_race->customer_id, $customer_race->race_id, $customer_race->race_race_type_id, $customer_race->referal_code);
            $_transaction->save();
            //$_transaction->sendNotif();
        } 
        else if($transaction == 'pending'){
          // TODO set payment status in merchant's database to 'Pending'
            $_transaction->status = 'CHK';
            $_transaction->paid_at = Carbon::now();
            $_transaction->save();
            //$_transaction->sendNotif();
        } 
        else if ($transaction == 'deny') {
          // TODO set payment status in merchant's database to 'Denied'
            $_transaction->status = 'REJ';
            $_transaction->save();
            //$_transaction->sendNotif();
        }
        else if ($transaction == 'expire') {
          // TODO set payment status in merchant's database to 'Expired'
            $_transaction->status = 'EXP';
            $_transaction->save();
            //$_transaction->sendNotif();
        }

        $_transaction = Transaction::where('invoice_number', $order_id)->first();

        if ($_transaction->status == 'PAI') {

            //$user = User::find($_transaction->user_id);

            $this->subscribe($_transaction->user_id, $_transaction->id);
        }
        //return Carbon::now()->format('YmdHi');
        return response()->json($_transaction);
    }

    public function subscribe($user_id, $transaction_id) {
        $subscribtion = new Subscriptions();
        $subscribtion->user_id = $user_id;
        $subscribtion->transaction_id = $transaction_id;
        $subscribtion->start_time = Carbon::now();
        $subscribtion->end_time = Carbon::now()->addYear();
        $subscribtion->save();
    }
}    