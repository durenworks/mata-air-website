<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Http\Requests\Gallery\StoreRequest;
use App\Http\Requests\Gallery\UpdateRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Gallery::latest('updated_at');
        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function ($q) use($keyword) {
                $q->where('tittle', 'like', "%$keyword%");
            });
        }

        $items = $items->where('type', 'video');
        $items = $items->get();

        //return response()->json($items);
        return view('admin.videos.index')
            ->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        //return response()->json($categories);

        return view('admin.videos.create')
            ->with(compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $item = new Gallery();
        $item->fill([
            'tittle' => $request->tittle,
            'type' => 'video',
            'url' => $this->getYoutubeEmbedUrl($request->url)

        ]);

        $item->save();

        return redirect(route(ADMIN .'.videos.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Gallery::findorfail($id);
        //return response()->json($item);

        return view('admin.videos.edit')
            ->with(compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $item = Gallery::findorfail($id);
        $item->fill([
            'name' => $request->name,
            'url' => $this->getYoutubeEmbedUrl($request->url)
        ]);

        $item->save();

        return redirect(route(ADMIN .'.videos.index'))->withSuccess('Item updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Gallery::findorfail($id);

        if ($item->image != '') {
            $file_path = Config::get('storage.galleries');
            $full_path = $file_path.'/'.$item->image;
            File::delete($full_path);
        }

        $item->delete();
        return redirect(route(ADMIN . '.videos.index'))->withSuccess('Item Deleted');
    }

    function getYoutubeEmbedUrl($url)
    {
        $finalUrl = '';
        if(strpos($url, 'facebook.com/') !== false) {
            //it is FB video
            $finalUrl.='https://www.facebook.com/plugins/video.php?href='.rawurlencode($url).'&show_text=1&width=200';
        }else if(strpos($url, 'vimeo.com/') !== false) {
            //it is Vimeo video
            $videoId = explode("vimeo.com/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
            $finalUrl.='https://player.vimeo.com/video/'.$videoId;
        }else if(strpos($url, 'youtube.com/') !== false) {
            //it is Youtube video
            $videoId = explode("v=",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;
        }else if(strpos($url, 'youtu.be/') !== false){
            //it is Youtube video
            $videoId = explode("youtu.be/",$url)[1];
            if(strpos($videoId, '&') !== false){
                $videoId = explode("&",$videoId)[0];
            }
            $finalUrl.='https://www.youtube.com/embed/'.$videoId;
        }else{
            //Enter valid video URL
        }
        return $finalUrl;
    }

}
