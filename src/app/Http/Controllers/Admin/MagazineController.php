<?php

namespace App\Http\Controllers\Admin;

use App\Models\Magazine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Magazine\StoreRequest;
use App\Http\Requests\Magazine\UpdateRequest;
use App\Models\ArticleFile;
use App\Models\MegazineArticle;
use Illuminate\Session\Store;
use DB;
use stdClass;

class MagazineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $items = Magazine::orderby('year', 'desc')
            ->orderby('number_edition');

        if (isset($request->edition) && ($request->edition != 'All')) {
            $items = $items->where('roman_edition', $request->edition);
        }

        if (isset($request->year)) {
            $items = $items->where('year', $request->year);
        }

        //        if (isset($request->status) && ($request->status != 'All')) {
        //
        //            $items = $items->where('status', $request->status);
        //        }

        $items = $items->active()->get();


        return view('admin.magazines.index')
            ->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;
        $articles = [];
        //return response()->json($categories);

        return view('admin.magazines.create')
            ->with(compact('item', 'articles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        $number_edition = romanToNumber($request->roman_edition);
        $item = new Magazine();
        $item->fill([
            'roman_edition' => $request->roman_edition,
            'number_edition' => $number_edition,
            'year' => $request->year,
            'description' => $request->description,
            'published_at' => Carbon::createFromFormat('m/d/Y', $request->date)
        ]);

        if ($request->hasFile('cover')) {
            $file = $request->file('cover');
            $item->cover = upload_image($file, 'magazines_cover');;
        }

        if ($request->hasFile('pdf')) {
            $file = $request->file('pdf');
            $item->pdf = upload_image($file, 'magazines_file');;
        }

        if ($request->hasFile('pdf_preview')) {
            $file = $request->file('pdf_preview');
            $item->pdf_preview = upload_image($file, 'magazines_file');;
        }

        $item->save();

        $articles = json_decode($request->articles);

        foreach ($articles as $key => $article) {
            $megazineArticle = new MegazineArticle();
            $megazineArticle->fill([
                'megazine_id' => $item->id,
                'title' => $article->title,
                'doc_id' => $article->doc_id,
                'audio_id' => $article->audio_id,
                'show_order' => $key + 1,
            ]);

            $megazineArticle->save();
        }

        DB::commit();
        //$item->categories()->sync($request->categories);
        //return response()->json($item);

        return redirect(route(ADMIN . '.magazines.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Magazine::findorfail($id);
        $itemArticles = $item->articles;

        $articles = [];

        foreach ($itemArticles as $key => $value) {
            $doc = $value->document;
            $audio = $value->audio;

            $article = new stdClass();
            $article->article_id = $value->id;
            $article->title = $value->title;
            $article->doc_id = $value->doc_id;
            $article->doc_url = route('media.article_doc', $doc->file_name);
            $article->audio_id = $value->audio_id;
            $article->audio_url = route('media.article_audio', $audio->file_name);
            $articles[] = $article;
        }

        return view('admin.magazines.edit')
            ->with(compact('item', 'articles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        DB::beginTransaction();

        $item = Magazine::findorfail($id);
        $number_edition = romanToNumber($request->roman_edition);

        $item->roman_edition = $request->roman_edition;
        $item->number_edition = $number_edition;
        $item->year = $request->year;
        $item->description = $request->description;
        $item->published_at = Carbon::createFromFormat('m/d/Y', $request->date);

        if ($request->hasFile('cover')) {
            $file = $request->file('cover');
            $item->cover != '' ? $oldFile = $item->cover : $oldFile = null;
            $item->cover = upload_image($file, 'magazines_cover', $oldFile);;
        }

        if ($request->hasFile('pdf')) {
            $file = $request->file('pdf');
            $item->pdf != '' ? $oldFile = $item->pdf : $oldFile = null;
            $item->pdf = upload_image($file, 'magazines_file', $oldFile);;
        }

        if ($request->hasFile('pdf_preview')) {
            $file = $request->file('pdf_preview');
            $item->pdf_preview != '' ? $oldFile = $item->pdf : $oldFile = null;
            $item->pdf_preview = upload_image($file, 'magazines_file', $oldFile);;
        }

        $item->save();

        $articles = json_decode($request->articles);
        $oldArticles = [];
        foreach ($item->articles()->select('id')->get() as $key => $value) {
            # code...
            $oldArticles[] = $value->id;
        }

        foreach ($articles as $key => $article) {
            if ($article->article_id) {
                // var_dump(MegazineArticle::find(intval($article->article_id)));
                // die;
                $megazineArticle = MegazineArticle::find(intval($article->article_id));
            } else
                $megazineArticle = new MegazineArticle();

            $megazineArticle->fill([
                'megazine_id' => $item->id,
                'title' => $article->title,
                'doc_id' => $article->doc_id,
                'audio_id' => $article->audio_id,
                'show_order' => $key + 1,
            ]);

            $megazineArticle->save();

            if ($oldArticles != null) {
                if (in_array($article->article_id, $oldArticles))
                    $oldArticles = array_diff($oldArticles, [$article->article_id]);
            }
        }
        foreach ($oldArticles as $key => $value) {
            # code...
            MegazineArticle::where('id', $value)->delete();
        }

        DB::commit();

        return redirect(route(ADMIN . '.magazines.index'))->withSuccess('Item updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Magazine::find($id);
        $item->is_active = 0;
        $item->save();

        return back()->withSuccess('Updated successfully');
    }

    public function uploadFile(Request $request)
    {

        DB::beginTransaction();

        // echo dd($request->file);
        $ArticleFile = new ArticleFile();
        $filename = null;
        $fileType = '';
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $file = $request->file('file');
                $ext = $file->getClientOriginalExtension();
                if ($ext == 'pdf') {
                    $fileType = 'doc';
                    $filename = upload_image($file, 'article_file');
                } else if ($ext == 'waf' || $ext == 'mp3') {
                    $filename = upload_image($file, 'article_audio');
                    $fileType = 'audio';
                } else {
                    return response()->json(['message' => 'extensi file salah'], 203);
                }
            }
        } else {
            return response()->json(['urls' => 'salah'], 200);
        }

        $ArticleFile->fill([
            'file_name' => $filename,
            'file_type' => $fileType,
        ]);

        $ArticleFile->save();

        $resp = [
            'url' => $fileType == 'doc' ?
                route('media.article_doc', $ArticleFile->file_name) : route('media.article_audio', $ArticleFile->file_name),
            'id' => $ArticleFile->id,
            'type' => $fileType,
        ];

        DB::commit();

        return response()->json($resp, 200);
    }
}
