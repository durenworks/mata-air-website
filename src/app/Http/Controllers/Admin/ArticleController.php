<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Article\StoreRequest;
use App\Http\Requests\Article\UpdateRequest;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::pluck('name', 'id')->toArray();

        //return response()->json($categories);

        $items = Article::latest('updated_at');

        if (isset($request->category) && ($request->category != 'All')) {
            $cat_id = $request->category;
            $items = $items->where('categories', $cat_id);
        }

        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function ($q) use($keyword) {
               $q->where('title', 'like', "%$keyword%");
               $q->orwhere('content', 'like, "%$keyword%"') ;
            });
        }

        if (isset($request->status) && ($request->status != 'All')) {

            $items = $items->where('status', $request->status);
        }

        $items = $items->active()->get();

        return view('admin.articles.index')
            ->with(compact('items'))
            ->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;
        $categories = Category::pluck('name', 'id')->toArray();
        //return response()->json($categories);

        return view('admin.articles.create')
            ->with(compact('categories'))
            ->with(compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $slug = str_slug($request->title);

        if ($count = Article::where('slug', 'like', "$slug%")->count())
            $slug = str_finish($slug, "-$count");

        $admin_email = Auth::guard('admin')->user()->email;
        $author = Author::where('email', $admin_email)->first();

        if ($author == null) {
            $author = new Author();
            $author->fill([
               'email' => $admin_email,
               'name' => Auth::guard('admin')->user()->name,
               'type' => 'ADM',
                'phone_fixed' => 'phone',
                'phone_mobile' => 'phone',
                'occupation' => '-'
            ]);

            $author->save();
        }

        $author_id = $author->id;


        $admin = Admin::find(Auth::guard('admin')->user()->id);
        $admin->author_id = $author_id;
        $admin->save();

        $item = new Article();
        $item->fill([
            'title' => $request->title,
            'slug' => $slug,
            'category_id' => $request->category,
            'status' => 'APR',
            'author_id' => $author_id,
            'published_at' => Carbon::now()
        ]);

        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $item->thumbnail = upload_image($file, 'articles');;
        }

        $item->content = extractSummernoteContent($request->content);

        $item->save();

        //$item->categories()->sync($request->categories);
        //return response()->json($item);

        return redirect(route(ADMIN .'.articles.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Article::findorfail($id);
        $categories = Category::pluck('name', 'id')->toArray();

        return view('admin.articles.edit')
            ->with(compact('categories'))
            ->with(compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (!isset($request->is_published)) {
            $is_published = 1;
        }
        else {
            $is_published = 0;
        }

        $item = Article::findorfail($id);
        if ($is_published == 1 && $item->is_published == 0) {
            $item->published_at = Carbon::now();
        }
        $slug = str_slug($request->title);

        if ($item->slug != 'slug') {
            if ($count = Article::where('slug', 'like', "$slug%")->count())
                $slug = str_finish($slug, "-$count");
        }

        $item->title = $request->title;
        $item->slug = $slug;
        $item->category_id = $request->category;
        $item->status = 'APR';


        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $item->thumbnail = $item->thumbnail = upload_image($file, 'articles', $item->thumbnail);
        }

        $item->content = extractSummernoteContent($request->content);

        $item->save();

        //$item->categories()->sync($request->categories);
        //return response()->json($item);

        return redirect(route(ADMIN .'.articles.index'))->withSuccess('Item added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Article::find($id);
        $item->is_active = 0;
        $item->save();

        return back()->withSuccess('Updated successfully');
    }

    public function reject($id)
    {
        $item = Article::find($id);
        $item->status = 'REJ';
        $item->save();

        return redirect(route(ADMIN .'.articles.index'))->withSuccess('Item updated');
    }
}
