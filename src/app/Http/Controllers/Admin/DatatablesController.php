<?php

namespace App\Http\Controllers\Admin;

use App\CustomClass\Collaborator;
use App\CustomClass\Donator;
use App\CustomClass\LaratableCampaign;
use App\CustomClass\LaratablePublication;
use App\CustomClass\LaratableSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\CustomClass\Volunteer;
use App\CustomClass\LaratableEvent;
use Freshbitsweb\Laratables\Laratables;

class DatatablesController extends Controller
{
    public function getVolunteersDatatablesData(Request $request)
    {

        return Laratables::recordsOf(Volunteer::class, function ($query) use ($request) {
            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->subdistrict) && $request->subdistrict != 'All') {
                $query = $query->where('subdistrict_id', '=', $request->subdistrict);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->verified) && $request->verified != 'All') {
                $query = $query->where('is_verified', '=', $request->verified);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                    $query->orwhere('email', 'like', "%$keyword%");
                });
            }

            return $query;
        });
    }

    public function getDonatorsDatatablesData(Request $request)
    {

        return Laratables::recordsOf(Donator::class, function ($query) use ($request) {
            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->subdistrict) && $request->subdistrict != 'All') {
                $query = $query->where('subdistrict_id', '=', $request->subdistrict);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                    $query->orwhere('email', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getCollaboratorsDatatablesData(Request $request)
    {

        return Laratables::recordsOf(Collaborator::class, function ($query) use ($request) {
            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->subdistrict) && $request->subdistrict != 'All') {
                $query = $query->where('subdistrict_id', '=', $request->subdistrict);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                    $query->orwhere('email', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getEventsDatatablesData(Request $request)
    {

        return Laratables::recordsOf(LaratableEvent::class, function ($query) use ($request) {
            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                    $query->orwhere('description', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getCampaignsDatatablesData(Request $request)
    {

        return Laratables::recordsOf(LaratableCampaign::class, function ($query) use ($request) {
            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->subdistrict) && $request->subdistrict != 'All') {
                $query = $query->where('subdistrict_id', '=', $request->subdistrict);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('status', '=', $request->status);
            }

            if (isset($request->type) && $request->type != 'All') {
                $query = $query->where('type', '=', $request->type);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                    $query->orwhere('occupation', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getSlidersDatatablesData(Request $request)
    {

        return Laratables::recordsOf(LaratableSlider::class, function ($query) use ($request) {
            $query = $query->where('is_active', true);
            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('title', 'like', "%$keyword%");
                    $query->orwhere('text', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getActivitiesPublicationsDatatablesData(Request $request)
    {

        return Laratables::recordsOf(LaratablePublication::class, function ($query) use ($request) {
            $query = $query->where('type', '=', 'activities');

            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->month) && $request->month != 'All') {
                $query = $query->where('month', '=', $request->month);
            }

            if (isset($request->year) && $request->year != 'All') {
                $query = $query->where('year', '=', $request->year);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

    public function getFinancePublicationsDatatablesData(Request $request)
    {
        return Laratables::recordsOf(LaratablePublication::class, function ($query) use ($request) {
            $query = $query->where('type', '=', 'finance');

            if (isset($request->province) && $request->province != 'All') {
                $query = $query->where('province_id', '=', $request->province);
            }

            if (isset($request->city) && $request->city != 'All') {
                $query = $query->where('city_id', '=', $request->city);
            }

            if (isset($request->status) && $request->status != 'All') {
                $query = $query->where('is_active', '=', $request->status);
            }

            if (isset($request->month) && $request->month != 'All') {
                $query = $query->where('month', '=', $request->month);
            }

            if (isset($request->year) && $request->year != 'All') {
                $query = $query->where('year', '=', $request->year);
            }

            if (isset($request->keyword)) {
                $keyword = $request->keyword;
                $query = $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', "%$keyword%");
                });
            }

            return $query;

        });
    }

}

