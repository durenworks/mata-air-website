<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreRequest;
use App\Http\Requests\Admin\UpdateRequest;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Models\User;
use Config;
use File;
use Illuminate\Support\Facades\Auth;
use Image;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = User::orderby('name');

        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function ($q) use($keyword) {
                $q->where('name', 'like', "%$keyword%");
                $q->orwhere('email', 'like, "%$keyword%"') ;
            });
        }
        $items = $items->get();

        if (isset($request->status) && $request->status != 'ALL') {
            //var_dump($request->status);die();
            if($request->status == 'ACT') {
                $items = $items->filter(function ($model){
                   return $model->subscription_status == true;
                });
            }
            else if($request->status = 'EXP') {
                //var_dump($request->status);die();
                $items = $items->filter(function ($model){
                    return $model->subscription_status == false;
                });
            }
        }

        return view('admin.users.index')
            ->with('items', $items);
    }

    public function show($id) {
        $item = User::find($id);

        //var_dump($item);
        return view('admin.users.detail')
            ->with('item', $item);
    }


    public function showAvatar($avatar) {
        $path = Config::get('storage.avatar_admin');


        ob_end_clean();
        $resp = response()->download($path . '/'. $avatar);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function activate($id) {

    }

    public function verified(Request $request) {
        $item = User::findOrFail($request->id);

        $item->is_verified = 1;
        $item->verified_by = Auth::guard('admin')->user()->id;
        $item->verified_at = now();
        if ($item->save()) {
            return response()->json(["status"=>'success']);
        }
        else {
            return response()->json(["status"=>'error']);
        }
    }
}
