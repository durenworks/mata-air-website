<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $items = Transaction::latest('updated_at');

        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function ($q) use($keyword) {
                $q->where('invoice_number', 'like', "%$keyword%");
            });
        }

        $items = $items->paid()->get();



        return view('admin.transactions.index')
            ->with(compact('items'));
    }
}
