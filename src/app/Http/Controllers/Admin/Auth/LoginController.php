<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['logout', 'login', 'showLoginForm']);
    }

    public function guard()
    {
        return Auth::guard('admin');
    }

    public function login(Request $request)
    {
        //var_dump('foo');die();
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $this->credentials($request);
        //var_dump($credentials);die();
        // This section is the only change
        if (Auth::guard('admin')->attempt($credentials)) {
            //var_dump('foo');die();
            $user = Auth::guard('admin')->getLastAttempted();
            $usr = $user->first();
                 //echo Auth::guard('admin')->check(); die();

            return redirect()->intended($this->redirectPath());
        }

        return redirect('/admin/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);

    }

    protected function credentials(Request $request)
    {
        // return $request->only($this->username(), 'password');
        return ['email'=>$request->{$this->username()}, 'password'=>$request->password,'is_active'=>'1'];
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect(ADMIN . '/login');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

}
