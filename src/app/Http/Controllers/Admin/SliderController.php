<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Slider\UpdateRequest;
use App\Http\Requests\Slider\StoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sliders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $item = new Slider();
        $item->fill([
            'title' => $request->title,
            'text' => $request->text,
            'input_by' => Auth::guard('admin')->user()->id
        ]);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $item->image = upload_image($file, 'sliders');
        }

        if($item->save()) {
            return redirect(route(ADMIN .'.sliders.index'))->withSuccess('Item added');
        }
        else {
            return redirect(route(ADMIN .'.sliders.index'))->withErrors('Failed to add');
            // echo 'foo';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider::findOrFail($id);

        return view('admin.sliders.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $item = Slider::findOrFail($id);

        $item->title = $request->title;
        $item->text = $request->text;
        $item->last_edit_by = Auth::guard('admin')->user()->id;


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $item->image =  upload_image($file, 'sliders', $item->image);;
        }

        if($item->save()) {
            return redirect(route(ADMIN .'.sliders.index'))->withSuccess('Item updated');
        }
        else {
            return redirect(route(ADMIN .'.sliders.index'))->withErrors('Failed to update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        $item = Slider::findOrFail($request->id);

        $item->is_active = 0;
        $item->delete_by = Auth::guard('admin')->user()->id;
        if ($item->save()) {
            return response()->json(["status"=>'success']);
        }
        else {
            return response()->json(["status"=>'error']);
        }
    }
}
