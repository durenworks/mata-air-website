<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Models\Admin;
use Config;
use File;
use Image;
use Hash;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Category::orderby('name');

        if (isset($request->status)) {
            $items = $items->where('is_active', $request->status);
        }
        else {
            $items = $items->where('is_active', 1);
        }

        $items = $items->get();

        return view('admin.categories.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $slug = str_slug($request->name);

        if ($count =Category::where('slug', 'like', "$slug%")->count())
            $slug = str_finish($slug, "-$count");

        $item = new Category();
        $item->fill([
            'name' => $request->name,
            'slug' => $slug
        ]);

        $item->save();

        return redirect(route(ADMIN .'.category.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Category::findOrFail($id);

        return view(ADMIN . '.categories.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $item = Category::findOrFail($id);
        $slug = str_slug($request->name);

        if ($count =Category::where('slug', 'like', "$slug%")->count())
            $slug = str_finish($slug, "-$count");


        $item->name = $request->name;
        $item->slug = $slug;

        $item->save();

        return redirect()->route(ADMIN . '.category.index')->withSuccess('Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Category::find($id);
        $item->is_active = 0;
        $item->save();

        return back()->withSuccess('Updated successfully');
    }

}
