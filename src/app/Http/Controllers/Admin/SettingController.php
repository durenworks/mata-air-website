<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
    Public function social() {
        $facebook = Setting::key('facebook')->first();
        if ($facebook) {
            $facebook = $facebook->content;
        }
        else {
            $facebook = '';
        }

        $twitter = Setting::key('twitter')->first();
        if ($twitter) {
            $twitter = $twitter->content;
        }
        else {
            $twitter = '';
        }

        $instagram = Setting::key('instagram')->first();
        if ($twitter) {
            $instagram = $instagram->content;
        }
        else {
            $instagram = '';
        }

        $linkedin = Setting::key('linkedin')->first();
        if ($linkedin) {
            $linkedin = $linkedin->content;
        }
        else {
            $linkedin = '';
        }

        return view('admin.socials.form')
            ->with('facebook', $facebook)
            ->with('twitter', $twitter)
            ->with('instagram', $instagram)
            ->with('linkedin', $linkedin);
    }

    public function socialStore(Request $request) {
        if (isset($request->facebook)) {
            $item = Setting::key('facebook')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'facebook',
                    'content' => $request->facebook
                ]);
            }
            else {
                $item->content = $request->facebook;
            }

            $item->save();
        }

        if (isset($request->twitter)) {
            $item = Setting::key('twitter')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'twitter',
                    'content' => $request->twitter
                ]);
            }
            else {
                $item->content = $request->twitter;
            }

            $item->save();
        }

        if (isset($request->instagram)) {
            $item = Setting::key('instagram')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'instagram',
                    'content' => $request->instagram
                ]);
            }
            else {
                $item->content = $request->instagram;
            }

            $item->save();
        }

        if (isset($request->linkedin)) {
            $item = Setting::key('linkedin')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'linkedin',
                    'content' => $request->linkedin
                ]);
            }
            else {
                $item->content = $request->linkedin;
            }

            $item->save();
        }

        return redirect(route(ADMIN . '.settings.social'));
    }

    public function prices() {
        $subscribePrice = Setting::key('subscribePrice')->first();
        if ($subscribePrice) {
            $subscribePrice = $subscribePrice->content;
        }
        else {
            $subscribePrice = '0';
        }

        $magazinePrice = Setting::key('magazinePrice')->first();
        if ($magazinePrice) {
            $magazinePrice = $magazinePrice->content;
        }
        else {
            $magazinePrice = '0';
        }

        return view('admin.prices.form')
            ->with('subscribePrice', $subscribePrice)
            ->with('magazinePrice', $magazinePrice);
    }

    public function pricesStore(Request $request) {
        if (isset($request->subscribePrice)) {
            $item = Setting::key('subscribePrice')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'subscribePrice',
                    'content' => $request->subscribePrice
                ]);
            }
            else {
                $item->content = $request->subscribePrice;
            }

            $item->save();
        }

        if (isset($request->magazinePrice)) {
            $item = Setting::key('magazinePrice')->first();

            if ($item == null) {
                $item = new Setting();
                $item->fill([
                    'key' => 'magazinePrice',
                    'content' => $request->magazinePrice
                ]);
            }
            else {
                $item->content = $request->magazinePrice;
            }

            $item->save();
        }

        return redirect(route(ADMIN . '.settings.prices'));
    }

}
