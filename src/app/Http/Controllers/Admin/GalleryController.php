<?php

namespace App\Http\Controllers\Admin;

use App\Models\GalleryItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Http\Requests\Gallery\StoreRequest;
use App\Http\Requests\Gallery\UpdateRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Gallery::latest('updated_at');
        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function ($q) use($keyword) {
                $q->where('tittle', 'like', "%$keyword%");
            });
        }
        $items = $items->where('type', 'image');

        $items = $items->get();

        //return response()->json($items);
        return view('admin.galleries.index')
            ->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        //return response()->json($categories);

        return view('admin.galleries.create')
            ->with(compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $item = new Gallery();
        $item->fill([
            'tittle' => $request->tittle,
            'type' => 'image',

        ]);

        $item->save();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $count = count($file);
            // dd($count, $file);
            for ($i=0; $i < $count; $i++) {
                // get image name
                if ($file[$i] != null) {
                    $galleryItem = new GalleryItem();
                    $galleryItem->fill([
                        'gallery_id' => $item->id,
                        'image' => upload_image($file[$i], 'galleries')
                    ]);
                    $galleryItem->save();
                }
            }
        }

        return redirect(route(ADMIN .'.galleries.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Gallery::findorfail($id);
        //return response()->json($item);

        return view('admin.galleries.edit')
            ->with(compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $item = Gallery::findorfail($id);
        $item->fill([
            'name' => $request->name
        ]);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $item->image = upload_image($file, 'galleries', $item->image);
        }

        $item->save();

        return redirect(route(ADMIN .'.galleries.index'))->withSuccess('Item updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Gallery::findorfail($id);

        if ($item->image != '') {
            $file_path = Config::get('storage.galleries');
            $full_path = $file_path.'/'.$item->image;
            File::delete($full_path);
        }

        $item->delete();
        return redirect(route(ADMIN . '.galleries.index'))->withSuccess('Item Deleted');
    }
}
