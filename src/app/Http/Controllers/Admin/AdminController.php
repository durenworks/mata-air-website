<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreRequest;
use App\Http\Requests\Admin\UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Models\Admin;
use Config;
use File;
use Image;
use Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Admin::latest('updated_at')
                ->where('is_hidden', 0);

        if (isset($request->status)) {
            $items = $items->where('is_active', $request->status);
        }
        else {
            $items = $items->where('is_active', 1);
        }

        if ($request->keyword != '') {
            $keyword = $request->keyword;
            $items = $items->where(function($query) use ($keyword){
                $query->where('full_name', 'like', "%$keyword%");
                $query->orwhere('email', 'like', "%$keyword%");
            });
        }

        $items = $items->get();

        return view('admin.admins.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $item = new Admin();
        $item->fill([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone
        ]);

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $avatar = upload_image($file, 'avatar_admin', $item, 'avatar');
            $item->avatar = $avatar;
        }


        $item->save();

        return redirect(route(ADMIN .'.admins.index'))->withSuccess('Item added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Admin::findOrFail($id);

        return view(ADMIN . '.admins.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $item = Admin::findOrFail($id);

        $item->full_name = $request->full_name;
        $item->email = $request->email;

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $avatar = upload_image($file, 'avatar_admin', $item, 'avatar', $item->avatar);
            $item->avatar = $avatar;
        }

        if ($request->password != '') {
            $item->password = Hash::make($request->password);
        }

        $item->save();

        return redirect()->route(ADMIN . '.admins.index')->withSuccess('Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Admin::find($id);
        $item->is_active = 0;
        $item->save();

        return back()->withSuccess('Updated successfully');
    }

    public function showAvatar($avatar) {
        $path = Config::get('storage.avatar_admin');


        ob_end_clean();
        $resp = response()->download($path . '/'. $avatar);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }
}
