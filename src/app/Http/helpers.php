<?php
use App\Models\Province;
use App\Models\City;
use App\Models\SubDistrict;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Models\Setting;
use App\Models\Category;
use App\Models\Magazine;
use App\Models\Article;

$facebook = '';

if (! function_exists('move_file')) {
    function move_file($file, $type='avatar', $withWatermark = false)
    {
        // Grab all variables
        $destinationPath = config('variables.'.$type.'.folder');
        $width           = config('variables.' . $type . '.width');
        $height          = config('variables.' . $type . '.height');
        $full_name       = str_random(16) . '.' . $file->getClientOriginalExtension();
        
        if ($width == null && $height == null) { // Just move the file
            $file->storeAs($destinationPath, $full_name);
            return $full_name;
        }


        // Create the Image
        $image           = Image::make($file->getRealPath());

        if ($width == null || $height == null) {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }else{
            $image->fit($width, $height);
        }

        if ($withWatermark) {
            $watermark = Image::make(public_path() . '/img/watermark.png')->resize($width * 0.5, null);

            $image->insert($watermark, 'center');
        }

        Storage::put($destinationPath . '/' . $full_name, (string) $image->encode());

        return $full_name;
    }
}

if (!function_exists('getProvince'))
{
    function getProvince($id = null) {

        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/province";
        if($id != null){
            $url .= '?id='.$id;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            // var_dump($response);die;
            return $response;
        }
    }
}

if (!function_exists('getProvinceForInsert'))
{
    function getProvinceForInsert() {
        $getProvince = json_decode(getProvince(), true);

        $aaa = array('province_id' => '0', 'province' => '-- Select Province --');

        if ($getProvince != null) {
            array_unshift( $getProvince['rajaongkir']['results'], $aaa );
            $provinces = [];
            // $cities[0] = ['','','','','-- Select City --',''];
            if ($getProvince['rajaongkir']['status']['code'] === 200) {
                $provinces = $getProvince['rajaongkir']['results'];
            }
        }
        else {
            $provinces = $aaa;
        }

        //$provinces = json_encode($provinces);

        //return $provinces;
        return collect($provinces)->pluck('province', 'province_id');
    }
}

if (!function_exists('getCity'))
{
    function getCity($province_id = null, $city_id = null) {
        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/city";
        if($province_id != null && $city_id == null){
            $url .= '?province='.$province_id;
        }elseif ($province_id != null && $city_id != null) {
            $url .= '?id='.$city_id.'&province='.$province_id;
        }elseif ($city_id != null) {
            $url .= '?id='.$city_id;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

if(!function_exists('getSubdistrict')){

    function getSubdistrict($city_id = null, $subdistrict_id = null){

        $key = Config::get('app.rajaongkir');
        $url = "http://pro.rajaongkir.com/api/subdistrict";
        if($city_id != null && $subdistrict_id == null){
            $url .= '?city='.$city_id;
        }elseif ($city_id != null && $subdistrict_id != null) {
            $url .= '?id='.$subdistrict_id.'&city='.$city_id;
        }elseif ($subdistrict_id != null) {
            $url .= '?id='.$subdistrict_id;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key:".$key
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}

if (!function_exists('i18l_date'))
{
    function i18l_date($value, $pattern = array(), $locale = null, $timezone = null) {
        return I18lFilter::date($value, $pattern, $locale, $timezone);
    }
}

if (!function_exists('i18l_currency'))
{
    function i18l_currency($value, $locale = null) {
        $a = str_replace("IDR", "IDR ", I18lFilter::currency($value, $locale));
        $a = str_replace("SGD", "SGD ", $a);
        return $a;
    }
}

if (!function_exists('i18l_number'))
{
    function i18l_number($value, $pattern = null, $locale = null) {
        return I18lFilter::number($value, $pattern, $locale);
    }
}


if (!function_exists('getCityFromDb'))
{
    function getCityFromDb($province_id) {
        $cities = City::where('province_id', '=', $province_id)
            ->get();
        return $cities;
    }
}

if (!function_exists('getSubDistrictFromDb'))
{
    function getSubDistrictFromDb($city_id) {

        $subDistricts = SubDistrict::where('city_id', '=', $city_id)
                        ->get();
        return $subDistricts;
    }
}

if (!function_exists('upload_image'))
{
    function upload_image($files, $location, $oldfile=null) {
        $upload_path = Config::get('storage.'.$location);


        //var_dump(Config::get('storage.'.$location)); die();
        //$file_name = $files->getClientOriginalName();
        $extension = $files->getClientOriginalExtension();

        // rename image name if duplicate
        /*while ($model::where($field, $file_name)->first() != null) {
            $file_name = rand(11111,99999).'.'.$extension;
        }*/
        $ret = new StdClass();
        $try = 250;
        do {
            if ($try <= 0)
                throw Exception("Storage::random fails to produce randomized filename");
            $hash = str_random(32);
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $upload_path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));
        //$ret = array($upload_path, $hash, $file);

        //$arrImage = $file_name;
        $files->move($upload_path, $hash);
        if ($oldfile) {
            $full_path = $upload_path . '/' . $oldfile;
            File::delete($full_path);
        }

        return $hash;
    }
}

if (!function_exists('getEnumValues'))
{
    function getEnumValues($table, $column) {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum = array_add($enum, $v, ucfirst($v));
        }
        return $enum;
    }
}

if (!function_exists('extractSummernoteContent'))
{
    function extractSummernoteContent($input) {
        $dom = new \DomDocument();
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHtml( mb_convert_encoding($input, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        libxml_use_internal_errors($internalErrors);
        $images = $dom->getElementsByTagName('img');

        //var_dump($images);die();

        foreach($images as $img){
            $src = $img->getAttribute('src');
            if(preg_match('/data:image/', $src)){
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                $filename = uniqid();
                $filepath = "/images/summernote/". $filename . '.' . $mimetype;

                $image = Image::make($src)
                    ->encode($mimetype, 100)
                    ->save(public_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            }
        }

        return $dom->saveHTML();
    }
}

if (!function_exists('checkIfProvinceExist')) {
    function checkIfProvinceExist($id)
    {
        if (!Province::find($id)) {
            $province = json_decode(getProvince($id), true);

            if($province['rajaongkir']['status']['code'] === 200){
                $province = $province['rajaongkir']['results'];

                Province::create([
                    'province_id' => $province['province_id'],
                    'province' => $province['province']
                ]);
            }
        }
    }
}

if (!function_exists('checkIfCityExist')) {
    function checkIfCityExist($id)
    {
        if (!City::find($id)) {
            $city = json_decode(getCity(null, $id), true);

            //var_dump($city);die();
            if($city['rajaongkir']['status']['code'] === 200){
                $city = $city['rajaongkir']['results'];

                City::create([
                    'city_id' => $city['city_id'],
                    'province_id' => $city['province_id'],
                    'type' => $city['type'],
                    'city_name' => $city['city_name'],
                    'postal_code' => $city['postal_code']
                ]);
            }
        }
    }
}

if (!function_exists('checkIfSubdistrictExist')) {
    function checkIfSubdistrictExist($id)
    {
        if (!SubDistrict::find($id)) {
            $subDistrict = json_decode(getSubdistrict(null, $id), true);


            if($subDistrict['rajaongkir']['status']['code'] === 200){
                $subDistrict = $subDistrict['rajaongkir']['results'];

                SubDistrict::create([
                    'subdistrict_id' => $subDistrict['subdistrict_id'],
                    'province_id' => $subDistrict['province_id'],
                    'city_id' => $subDistrict['city_id'],
                    'subdistrict_name' => $subDistrict['subdistrict_name'],
                ]);

            }
        }
    }
}

if (!function_exists('getFacebookAccount')) {

    function getFacebookAccount()
    {
        $item = Setting::key('facebook')->first();

        $account = '';
        if ($item != null) {
            $account = $item->content;
        }

        return $account;
    }
}

if (!function_exists('getTwitterAccount')) {

    function getTwitterAccount()
    {
        $item = Setting::key('twitter')->first();

        $account = '';
        if ($item != null) {
            $account = $item->content;
        }

        return $account;
    }
}

if (!function_exists('getInstagramAccount')) {

    function getInstagramAccount()
    {
        $item = Setting::key('instagram')->first();

        $account = '';
        if ($item != null) {
            $account = $item->content;
        }

        return $account;
    }
}

if (!function_exists('getLinkedinAccount')) {

    function getLinkedinAccount()
    {
        $item = Setting::key('linkedin')->first();

        $account = '';
        if ($item != null) {
            $account = $item->content;
        }

        return $account;
    }
}

if (!function_exists('getSubscriptionPrice')) {

    function getSubscriptionPrice()
    {
        $item = Setting::key('subscribePrice')->first();

        $price = '0';
        if ($item != null) {
            $price = $item->content;
        }

        return $price;
    }
}


if (!function_exists('indonesianDate')) {

    function indonesianDate($date, $day_name = true)
    {
        $dt = new  \Carbon\Carbon($date);
        setlocale(LC_TIME, 'IND');

        if ($day_name) {
            return $dt->formatLocalized('%A, %e %B %Y'); // Senin, 3 September 2018
        }
        else {
            return $dt->formatLocalized('%e %B %Y %H:%I:%S'); // 3 September 2018
        }

    }
}

if (!function_exists('getYearForSelect')) {

    function getYearForSelect()
    {
        $dt = Carbon::today();
        $curr_year = (int)$dt->format('Y');
        $year_arr = array();

        for ($i = 2018; $i <= $curr_year + 1; $i++) {
            $year_arr = array_add($year_arr, $i, $i);
        }

        return $year_arr;
    }
}

if (!function_exists('romanToNumber')) {

    function romanToNumber($roman)
    {
        $romans = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        );

        $result = 0;

        foreach ($romans as $key => $value) {
            while (strpos($roman, $key) === 0) {
                $result += $value;
                $roman = substr($roman, strlen($key));
            }
        }
        return $result;
    }
}

if (!function_exists('getCategories')) {
    function getCategories()
    {
        return Category::active()->orderBy('name')->get();
    }
}

if (!function_exists('getLatestMagazines')) {
    function getLatestMagazines()
    {
        return Magazine::active()->latest()->limit(5)->get();
    }
}

if (!function_exists('indonesianDate')) {

    function indonesianDate($date)
    {
        $dt = new  \Carbon\Carbon($date);
        setlocale(LC_TIME, 'IND');

        return $dt->formatLocalized('%A, %e %B %Y'); // Senin, 3 September 2018
    }
}

if (!function_exists('getMonth')) {

    function getMonth($date)
    {
        $dt = new  \Carbon\Carbon($date);
        setlocale(LC_TIME, 'IND');

        return ucfirst($dt->formatLocalized('%B')); // Senin, 3 September 2018
    }
}

if (!function_exists('getMostViewArticles')) {
    function getLatestArticles()
    {
        return Article::active()->orderby('reader_count', 'desc')->latest()->limit(5)->get();
    }
}






