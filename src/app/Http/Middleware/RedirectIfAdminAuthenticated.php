<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //var_dump(Auth::guard('web')->check());die();
        //var_dump($auth->check());die();
        if (Auth::guard('admin')->check()) {
            return $next($request);
        }
        else {
            return redirect(ADMIN.'/login');
        }
    }
}
