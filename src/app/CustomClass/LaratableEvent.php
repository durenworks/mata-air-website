<?php
/**
 * Created by PhpStorm.
 * User: Dadittya
 * Date: 21/11/2018
 * Time: 10:51
 */

namespace App\CustomClass;

use App\Models\Event;

class LaratableEvent extends Event
{
    protected $table = 'events';

    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

    }

    public static function laratablesCustomAction($event)
    {

        if ($event->is_active) {
            return view('admin.includes.datatables.action-edit-delete')
                ->with('id', $event->id)
                ->with('route_edit', '.events.edit')
                ->with('activate', $event->is_active)
                ->render();
        }
    }

    public static function laratablesCustomBeautyDate($event) {
        return $event->date->format('d M Y');
    }

}