<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use stdClass;
use Session;

class AddressImport implements ToCollection
{
  /**
   * @param array $row
   *
   * @return \Illuminate\Database\Eloquent\Model|null
   */
  public function collection(Collection $rows)
  {
    $items = [];
    foreach ($rows as $key => $row) {
      // var_dump($row);
      if ($key && $row[0]) {
        $item = new stdClass();
        $item->name = $row[0];
        $item->street = $row[1];
        $item->residence = $row[2];
        $item->rt = $row[3];
        $item->rw = $row[4];
        $item->village = $row[5];
        $item->urban_village = $row[6];
        $item->subdistrict = $row[7];
        $item->city = $row[8];
        $item->province = $row[9];
        $item->postal_code = $row[10];
        $item->handphone = $row[11];
        $items[] = $item;
      }
    }
    Session::put('addresses', $items);
  }
}
