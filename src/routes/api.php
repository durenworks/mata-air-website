<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\ApiController@login');
Route::post('register', 'Api\ApiController@register');

Route::get('magazines', 'Api\MagazineController@magazineList');
Route::get('subscription_price', 'Api\MagazineController@subscribePrice');
Route::get('magazine/{id}/toc', 'Api\MagazineController@articleLists');
Route::get('magazine/{id}/{article_id}', 'Api\MagazineController@article');

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'Api\ApiController@logout');

    Route::get('user', 'Api\ApiController@getAuthUser');
    Route::get('my_magazine', 'Api\ApiController@getMagazineList');
    Route::get('transaction_history', 'Api\ApiController@transactionHistory');
    Route::get('transaction', 'Api\ApiController@transaction');
    Route::post('charge', 'Api\ApiController@charge');
    Route::post('user/update', 'Api\ApiController@updateProfile');
    Route::post('/{token}/charge', 'Api\ApiController@charge2');

    //Route::put('transaction/{invoice_number}', 'Api\ApiController@transaction_callback');


    //    Route::get('products', 'ProductController@index');
    //    Route::get('products/{id}', 'ProductController@show');
    //    Route::post('products', 'ProductController@store');
    //    Route::put('products/{id}', 'ProductController@update');
    //    Route::delete('products/{id}', 'ProductController@destroy');
});
