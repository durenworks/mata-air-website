<?php

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
//Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::get(ADMIN . '/login', 'Admin\Auth\LoginController@showLoginForm')->name('auth.login.admin');
Route::post(ADMIN . '/login', 'Admin\Auth\LoginController@login')->name('auth.login.admin');
Route::get(ADMIN . '/logout', 'Admin\Auth\LoginController@logout')->name('auth.logout.admin');

Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware' => ['admin']], function () {
  Route::get('/', 'DashboardController@index')->name('dash');
  Route::resource('admins', 'Admin\AdminController');

  Route::resource('category', 'Admin\CategoryController');
  Route::resource('articles', 'Admin\ArticleController');
  Route::resource('galleries', 'Admin\GalleryController');
  Route::resource('videos', 'Admin\VideoController');
  Route::get('/articles/{id}/reject', 'Admin\ArticleController@reject')->name('articles.reject');

  Route::resource('magazines', 'Admin\MagazineController');
  Route::post('megazines/upload_file', 'Admin\MagazineController@uploadFile')->name('megazine.upload_file');
  /**
   * @users route
   */
  Route::get('/users/', 'Admin\UserController@index')->name('users.index');
  Route::get('/users/{id}/activate', 'Admin\UserController@activate')->name('users.activate');
  Route::put('users/verify', 'Admin\UserController@verified')->name('users.verify');
  Route::get('users/{id}', 'Admin\UserController@show')->name('users.detail');

  /**
   * @slider route
   */
  //    Route::get('/articles/', 'Admin\ArticleController@index')->name('articles.index');
  //    Route::get('/articles/create/', 'Admin\ArticleController@create')->name('articles.create');
  //    Route::post('/articles/', 'Admin\ArticleController@store')->name('articles.store');
  //    Route::get('/articles/{id}/edit', 'Admin\ArticleController@edit')->name('articles.edit');
  //    Route::put('/articles/{id}/edit', 'Admin\ArticleController@update')->name('articles.update');
  //    Route::put('/articles/delete', 'Admin\ArticleController@delete')->name('articles.delete');

  /**
   * @setting route
   */
  Route::get('/setting/socials', 'Admin\SettingController@social')->name('settings.social');
  Route::post('/setting/socials', 'Admin\SettingController@socialStore')->name('settings.social.store');

  Route::get('/setting/prices', 'Admin\SettingController@prices')->name('settings.prices');
  Route::post('/setting/prices', 'Admin\SettingController@pricesStore')->name('settings.settings.store');

  /**
   * @transaction route
   */
  Route::get('/transaction', 'Admin\TransactionController@index')->name('transaction.index');

  /**
   * Route for ajax datatabales
   */
  Route::get('/get-datatables-data/volunteer', 'Admin\DatatablesController@getVolunteersDatatablesData')->name('datatables.volunteer');
  Route::get('/get-datatables-data/donator', 'Admin\DatatablesController@getDonatorsDatatablesData')->name('datatables.donator');
  Route::get('/get-datatables-data/collaborator', 'Admin\DatatablesController@getCollaboratorsDatatablesData')->name('datatables.collaborator');
  Route::get('/get-datatables-data/event', 'Admin\DatatablesController@getEventsDatatablesData')->name('datatables.event');
  Route::get('/get-datatables-data/campaigns', 'Admin\DatatablesController@getCampaignsDatatablesData')->name('datatables.campaign');
  Route::get('/get-datatables-data/slider', 'Admin\DatatablesController@getSlidersDatatablesData')->name('datatables.slider');
  Route::get('/get-datatables-data/publication/activities', 'Admin\DatatablesController@getActivitiesPublicationsDatatablesData')->name('datatables.publication.activities');
  Route::get('/get-datatables-data/publication/finance', 'Admin\DatatablesController@getFinancePublicationsDatatablesData')->name('datatables.publication.finance');
});

Route::get('/', 'Frontend\PageController@index')->name('home');
Route::get('/edition', 'Frontend\EditionController@index')->name('f.edition');
Route::get('/category/{slug}', 'Frontend\PageController@category')->name('f.category');
Route::get('/article/{slug}', 'Frontend\PageController@article')->name('f.article');
Route::get('/writer/{slug}', 'Frontend\PageController@writer')->name('f.writer');
Route::get('/reader-letter', 'Frontend\PageController@readerletter')->name('f.readerletter');
Route::post('/reader-letter', 'Frontend\FormController@storeArticle')->name('f.readerletter.post');
Route::get('/reader-letter/{slug}', 'Frontend\PageController@readerletterdetail')->name('f.readerletterdetail');
Route::get('/reader-letter-submit', 'Frontend\PageController@readerlettersubmit')->name('f.readerlettersubmit');
Route::get('/reader-letter-form', 'Frontend\PageController@readerletterform')->name('f.readerletterform');
Route::get('/gallery', 'Frontend\PageController@gallery')->name('f.gallery');
Route::get('/career', 'Frontend\PageController@career')->name('f.career');
Route::get('/careerform', 'Frontend\PageController@careerform')->name('f.career.form');
Route::get('/faq', 'Frontend\PageController@faq')->name('f.faq');
Route::get('/tos', 'Frontend\PageController@tos')->name('f.tos');
Route::get('/contact', 'Frontend\PageController@contact')->name('f.contact');
Route::get('/about', 'Frontend\PageController@about')->name('f.about');
Route::get('/magazine/{id}', 'Frontend\MagazineController@index')
  ->name('f.magazine_detail');

Route::get('/cart', 'Frontend\CartController@index')->name('f.cart');
Route::post('/cart/{id}/add', 'Frontend\CartController@add')->name('f.cart_add');
Route::get('/cart/remove_all', 'Frontend\CartController@removeAll')
  ->name('f.cart_remove_all');
Route::get('/checkout', 'Frontend\CheckoutController@index')->name('f.checkout');
Route::get('/checkout/add_address', 'Frontend\CheckoutController@address')
  ->name('f.checkout.add_address');
Route::get('/checkout/remove_address', 'Frontend\CheckoutController@removeAddress')
  ->name('f.checkout.remove_address');
Route::get('/checkout/{invoice_number}/payment', 'Frontend\CheckoutController@payment')
  ->name('f.checkout.payment');
Route::post('/checkout/add_address', 'Frontend\CheckoutController@importAddress')
  ->name('f.checkout.add_address');
Route::post('/checkout/store_email', 'Frontend\CheckoutController@storeEmail')
  ->name('f.checkout.store_email');
Route::post('/checkout', 'Frontend\CheckoutController@doCheckout')
  ->name('f.do_checkout');

Route::group(['prefix' => 'account', 'middleware' => ['guest']], function () {
  //my account
  Route::get('/', 'Frontend\PageController@account')->name('f.account');
  Route::get('/subscribe', 'Frontend\PageController@subscribe')->name('f.subscribe');
  Route::post('/subscribe', 'Frontend\SubscribeController@doCheckout')->name('f.subscribe.post');
  Route::post('/profile', 'Frontend\FormController@storeProfile')->name('f.profile.post');
});


/**
 * @auth route
 */
Route::get('/login', 'Frontend\PageController@login')->name('login');
Route::post('/login', 'Frontend\LoginController@login')->name('login.post');
Route::get('/signup', 'Frontend\PageController@signup')->name('signup');
Route::post('/register', 'Frontend\AuthController@doRegister')->name('register.post');
Route::get('/resetpassword', 'Frontend\PageController@resetpassword')->name('resetpassword');
Route::get('/activate/{token}', 'Frontend\AuthController@activate')->name('user.activate');

/**
 * Route to get region
 */
Route::get('/get-regions/city/db', 'RegionController@getCityFromDb')->name('region.get_city.db');
Route::get('/get-regions/subdistrict/db', 'RegionController@getSubDistrictFromDb')->name('region.get_subdistrict.db');

Route::get('/get-regions/city/external', 'RegionController@getCity')->name('region.get_city');
Route::get('/get-regions/subdistrict/external', 'RegionController@getSubDistrict')->name('region.get_subdistrict');

/**
 * Route to show picture
 */
Route::get('/image/{location}/{filename}', 'Frontend\FileController@showImage')->name('show.image');

/**
 * Route to file download
 */
Route::get('/magazine/{id}/download', 'Frontend\FileController@downloadPDF')->name('download.file');
Route::get('/article/doc/{filename}', 'Frontend\FileController@articleDoc')->name('media.article_doc');
Route::get('/article/audio/{filename}', 'Frontend\FileController@articleAudio')->name('media.article_audio');

/**
 * Route to midtrans API
 */
Route::get('/api/snap/{id}', 'SnapController@snap');
Route::get('/api/snaptoken/{id}', 'SnapController@token');
Route::post('/api/snapfinish', 'SnapController@finish');
Route::post('/api/notification', 'SnapController@notification');
