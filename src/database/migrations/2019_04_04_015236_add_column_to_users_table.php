<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('occupation')->nullable()->after('email');
            $table->string('phone')->nullable()->after('email');
            $table->string('postal_code')->nullable()->after('email');
            $table->string('province')->nullable()->after('email');
            $table->unsignedInteger('province_id')->nullable()->after('email');
            $table->string('city')->nullable()->after('email');
            $table->unsignedInteger('city_id')->nullable()->after('email');
            $table->string('subdistrict')->nullable()->after('email');
            $table->unsignedInteger('subdistrict_id')->nullable()->after('email');
            $table->string('urban_village')->nullable()->after('email');
            $table->string('village')->nullable()->after('email');
            $table->string('rt')->nullable()->after('email');
            $table->string('rw')->nullable()->after('email');
            $table->string('residence')->nullable()->after('email');
            $table->string('street')->nullable()->after('email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('occupation');
            $table->dropColumn('phone');
            $table->dropColumn('postal_code');
            $table->dropColumn('province');
            $table->dropColumn('province_id');
            $table->dropColumn('city');
            $table->dropColumn('city_id');
            $table->dropColumn('subdistrict');
            $table->dropColumn('subdistrict_id');
            $table->dropColumn('urban_village');
            $table->dropColumn('village');
            $table->dropColumn('rt');
            $table->dropColumn('rw');
            $table->dropColumn('residence');
            $table->dropColumn('street');

        });
    }
}
