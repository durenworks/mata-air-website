<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('title')->nullable();
            $table->string('text')->nullable();
            $table->integer('input_by')->unsigned();
            $table->boolean('is_active')->default(1);
            $table->integer('last_edit_by')->unsigned()->nullable();
            $table->integer('delete_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('input_by')->references('id')->on('admins');
            $table->foreign('delete_by')->references('id')->on('admins');
            $table->foreign('last_edit_by')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
