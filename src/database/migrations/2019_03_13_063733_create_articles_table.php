<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique;
            $table->unsignedInteger('category_id');
            $table->text('content');
            $table->string('thumbnail');
            $table->enum('status',['WAI', 'APR', 'REJ'])->default('WAI');
            $table->boolean('is_active')->default(1);
            $table->unsignedInteger('author_id');
            $table->integer('reader_count')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('author_id')->references('id')->on('authors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}

