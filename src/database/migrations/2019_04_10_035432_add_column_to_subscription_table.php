<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('postal_code')->nullable()->after('user_id');
            $table->string('province')->nullable()->after('user_id');
            $table->unsignedInteger('province_id')->nullable()->after('user_id');
            $table->string('city')->nullable()->after('user_id');
            $table->unsignedInteger('city_id')->nullable()->after('user_id');
            $table->string('subdistrict')->nullable()->after('user_id');
            $table->unsignedInteger('subdistrict_id')->nullable()->after('user_id');
            $table->string('urban_village')->nullable()->after('user_id');
            $table->string('village')->nullable()->after('user_id');
            $table->string('rt')->nullable()->after('user_id');
            $table->string('rw')->nullable()->after('user_id');
            $table->string('residence')->nullable()->after('user_id');
            $table->string('street')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('postal_code');
            $table->dropColumn('province');
            $table->dropColumn('province_id');
            $table->dropColumn('city');
            $table->dropColumn('city_id');
            $table->dropColumn('subdistrict');
            $table->dropColumn('subdistrict_id');
            $table->dropColumn('urban_village');
            $table->dropColumn('village');
            $table->dropColumn('rt');
            $table->dropColumn('rw');
            $table->dropColumn('residence');
            $table->dropColumn('street');
        });
    }
}
