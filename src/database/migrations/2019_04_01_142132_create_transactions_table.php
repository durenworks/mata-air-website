<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number')->unique();
            $table->unsignedInteger('user_id');
            $table->enum('status', ['INV', 'PAI', 'CHK', 'REJ', 'EXP']);// invoice, paid, check, rejected, expired
            $table->decimal('price',15,2);
            $table->decimal('discount',15,2)->default(0);
            $table->enum('discount_type', ['percent', 'nominal'])->default('nominal');
            $table->decimal('total',15,2);
            $table->dateTime('paid_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
