<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id');
            $table->string('name', 120);
            $table->text('street');
            $table->string('residence', 100)->nullable();
            $table->string('rt', 8)->nullable();
            $table->string('rw', 8)->nullable();
            $table->string('village', 50)->nullable();
            $table->string('urban_village', 50)->nullable();
            $table->string('subdistrict', 50)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('province', 50)->nullable();
            $table->string('postal_code', 12);
            $table->string('handphone', 20);
            $table->enum('status', ['pending', 'in_delivery', 'delivered', 'failed'])->default('pending');
            $table->timestamps();

            $table->foreign('transaction_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_addresses');
    }
}
