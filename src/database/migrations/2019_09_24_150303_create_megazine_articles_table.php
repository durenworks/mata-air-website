<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMegazineArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('megazine_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('show_order')->default(1);
            $table->unsignedInteger('megazine_id');
            $table->unsignedInteger('audio_id')->nullable();
            $table->unsignedInteger('doc_id');
            $table->timestamps();

            $table->foreign('megazine_id')
                ->references('id')
                ->on('magazines')
                ->onDelete('restrict');
            $table->foreign('audio_id')
                ->references('id')
                ->on('article_files')
                ->onDelete('restrict');
            $table->foreign('doc_id')
                ->references('id')
                ->on('article_files')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('megazine_articles');
    }
}
