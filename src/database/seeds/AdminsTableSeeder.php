<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'Lokavor Engineer',
            'email' => 'engineer@lokavor.studio',
            'phone' => '0856xxxxxx',
            'password' => Hash::make('lok4vor2134'),
            'is_active' =>'1',
            'is_hidden' => '1'
        ]);

        Admin::create([
            'name' => 'Admin Example',
            'email' => 'admin@example.com',
            'phone' => '0856xxxxxx',
            'password' => Hash::make('Admin2134'),
            'is_active' =>'1',
            'is_hidden' => '0'
        ]);
    }
}
