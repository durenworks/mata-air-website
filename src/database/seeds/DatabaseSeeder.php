<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('admins')->delete();
        DB::table('sub_districts')->delete();
        DB::table('cities')->delete();
        DB::table('provinces')->delete();
        DB::table('categories')->delete();

        $this->call(AdminsTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
    }
}
