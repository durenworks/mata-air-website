<?php

return [

    'avatar-admin' => storage_path() . '/app/avatar/admin',
    'avatar-user' => storage_path() . '/app/public/avatar',
    'articles' => storage_path() . '/app/public/article',
    'article_file' => storage_path() . '/app/public/article_file',
    'article_audio' => storage_path() . '/app/public/article_audio',
    'magazines_cover' => storage_path() . '/app/public/magazine/cover',
    'magazines_file' => storage_path() . '/app/public/magazine/file',
    'galleries' => storage_path() . '/app/public/galleries'
];
