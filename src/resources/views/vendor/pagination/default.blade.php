@if ($paginator->hasPages())
<ul class="pagination justify-content-center">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li class="page-item disabled">
        <a class="page-link" href="#" tabindex="-1">&laquo; Previous</a>
    </li>
    @else
    <li class="page-item">
        <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="page-link">
            &laquo; Previous
        </a>
    </li>
    @endif

    @if($paginator->currentPage() > 3)
    <li class="page-item hidden-xs">
        <a href="{{ $paginator->url(1) }}" class="page-link">
            1
        </a>
    </li>
    @endif
    @if($paginator->currentPage() > 4)
    <li class=" page-item disabled hidden-xs">
        <span class="page-link">...</span>
    </li>
    @endif
    @foreach(range(1, $paginator->lastPage()) as $i)

    @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
        @if ($i == $paginator->currentPage())
        <li class="page-item active">
            <span class="page-link">{{ $i }}</span>
        </li>
        @else
        <li class="page-item">
            <a href="{{ $paginator->url($i) }}" class="page-link">
                {{ $i }}
            </a>
        </li>
        @endif

        @endif
        @endforeach
        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li class="page-item disabled hidden-xs">
                <span class="page-link">...</span>
            </li>
            @endif

            @if($paginator->currentPage() < $paginator->lastPage() - 2)
                <li class="page-item hidden-xs">
                    <a href="{{ $paginator->url($paginator->lastPage()) }}" class="page-link">
                        {{ $paginator->lastPage() }}
                    </a>
                </li>
                @endif

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link" rel="next">
                        Next &raquo;
                    </a>
                </li>
                @else
                <li class="page-item disabled">
                    <span class="page-link">Next &raquo;</span>
                </li>
                @endif
</ul>
@endif