@extends('admin.default')

@section('page-header')
  Tambah Kategori
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.category.index') }}>Kategori</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\CategoryController@store']
      ])
    !!}

    @include('admin.categories.form')

    <button type="submit" class="btn btn-primary">Simpan</button>
    
  {!! Form::close() !!}
  

@stop

