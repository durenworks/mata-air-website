@extends('admin.default')

@section('page-header')
  Ubah Kategori
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.category.index') }}>Kategori</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\CategoryController@update', $item->id],
        'method' => 'put',
      ])
    !!}

    @include('admin.categories.form')

    <button type="submit" class="btn btn-primary">Simpan</button>
    
  {!! Form::close() !!}
  

@stop

