@extends('admin.default')

@section('page-header')
    Manajemen Kategori
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Kategpri</li>
@endsection

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.category.create') }}" class="btn btn-info">
       Tambah
    </a>
</div>

<div class="row">
    <div class="col-md-12">

    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
             
                <tfoot >
                <tr>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
                </tfoot>
             
                <tbody>
                    @forelse($items as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route(ADMIN . '.category.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open([
                                            'class'=>'delete',
                                            'url'  => route(ADMIN . '.category.destroy', $item->id),
                                            'method' => 'DELETE',
                                            ]) 
                                        !!}

                                            <button class="btn btn-danger btn-sm" title="delete}"><i class="ti-trash"></i></button>
                                            
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2" align="center">No Data Found</td>
                        </tr>
                    @endforelse
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection