@extends('admin.default')

@section('page-header')
    Social Media Setting
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Socials</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\SettingController@socialStore']
      ])
    !!}

    <div class="row mB-40">
        <div class="col-sm-12">
            <div class="bgc-white p-20 bd">
                <div class='form-group row'>
                    <label class="col-md-1">Facebook </label>
                    <div class="col-md">
                        {!!  Form::input(
                              'text',
                              'facebook',
                              $facebook != '' ? $facebook : '',
                              [
                                    'class' => 'form-control',
                                    'id' => 'facebook',
                                    'placeholder' => 'i.e https://www.facebook.com/username'
                              ]
                            )
                         !!}
                    </div>
                </div>

                <div class='form-group row'>
                    <label class="col-md-1">Twitter </label>
                    <div class="col-md">
                        {!!  Form::input(
                              'text',
                              'twitter',
                              $twitter != '' ? $twitter : '',
                              [
                                    'class' => 'form-control',
                                    'id' => 'twitter',
                                    'placeholder' => 'i.e https://twitter.com/username'
                              ]
                            )
                         !!}
                    </div>
                </div>

                <div class='form-group row'>
                    <label class="col-md-1">Instagram </label>
                    <div class="col-md">
                        {!!  Form::input(
                              'text',
                              'instagram',
                              $instagram != '' ? $instagram : '',
                              [
                                    'class' => 'form-control',
                                    'id' => 'instagram',
                                    'placeholder' => 'i.e https://www.instagram.com/username'
                              ]
                            )
                         !!}
                    </div>
                </div>

                <div class='form-group row'>
                    <label class="col-md-1">LinkedIn </label>
                    <div class="col-md">
                        {!!  Form::input(
                              'text',
                              'linkedin',
                              $linkedin != '' ? $linkedin : '',
                              [
                                    'class' => 'form-control',
                                    'id' => 'linkedin',
                                    'placeholder' => 'i.e https://id.linkedin.com/in/username'
                              ]
                            )
                         !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    <button type="submit" class="btn btn-primary">Save</button>

    {!! Form::close() !!}


@stop

@section('content-js')
    <script src="{{ mix('/js/region.js') }}"></script>
@endsection

