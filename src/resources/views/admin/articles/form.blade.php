<div class="row mB-40">
  <div class="col-sm-12">
    <div class="bgc-white p-20 bd">
        {!! Form::myInput('text', 'title', 'Judul') !!}

        {!! Form::mySelect('category', 'Kategori', $categories,
            (request()->input('category') ? request()->input('category') : 'All')) !!}

        {!! Form::myTextArea('content', 'Isi', ['id' => 'content']) !!}
    
        {!! Form::myFile('thumbnail', 'Thumbnail') !!}


    </div>  
  </div>
</div>