@extends('admin.default')

@section('page-header')
  Tambah Artikel
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.articles.index') }}>Artikel</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\ArticleController@store'],
        'files' => true
      ])
    !!}

    @include('admin.articles.form')

    <button type="submit" class="btn btn-primary">Save</button>
    
  {!! Form::close() !!}
  

@stop



