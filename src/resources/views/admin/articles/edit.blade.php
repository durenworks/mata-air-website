@extends('admin.default')

@section('page-header')
  Ubah Artikel
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.articles.index') }}>Artikel</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\ArticleController@update', $item->id],
        'method' => 'put', 
        'files' => true
      ])
    !!}

    @include('admin.articles.form')

    <div class="form-group row">
        <div class='col-md-1'>
            <button type="submit" class="btn btn-primary">{{ $item->status == 'WAI' ? 'Publish' : 'Simpan' }}</button>
        </div>
        @if ($item->status != 'APR')
            <div class='col-md-1'>
                <a href="{{ route(ADMIN . '.articles.reject', $item->id) }}" class="btn btn-danger" role="button" aria-pressed="true">Reject</a>
            </div>
        @endif
    </div>

    
  {!! Form::close() !!}
@stop


