@extends('admin.default')

@section('page-header')
    Manajemen Artikel
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Artikel</li>
@endsection

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.articles.create') }}" class="btn btn-info">
        Tambah
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.articles.index'),
                'method' => 'get'
              ])
            !!}
            <div class='form-group row'>
                <label for="type" class='col-md-1 col-form-label'>Kategori</label>
                <div class='col-md-3'>
                    {!!
                        Form::select(
                            'category',
                            $categories != [] ? ['All' => 'All'] + $categories : ['All' => 'All'],
                            (request()->input('category') ? request()->input('category') : 'All'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>
                <label for="type" class='col-md-1 col-form-label'>Status</label>
                <div class='col-md-3'>
                    {!!
                        Form::select(
                            'status',
                            ['All' => 'All', 'APR' => 'Diterbitkan', 'WAI' => 'Moderasi', 'REJ' => 'Ditolak'],
                            (request()->input('status') ? request()->input('status') : 'All'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="keyword" class='col-md-1 col-form-label'>Kata Kunci</label>
                <div class='col-md-7'>
                    <input class="form-control" placeholder="Cari judul atau isi artikel" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">Cari</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Tanggal Terbit</th>
                        <th>Kategori</th>
                        <th>Status</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Judul</th>
                        <th>Tanggal Terbit</th>
                        <th>Kategori</th>
                        <th>Status</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </tfoot>

                <tbody>
                    @if(count($items) == 0)
                        <tr>
                            <td colspan="5">Tidak ada data yang ditemukan.</td>
                        </tr>
                    @endif
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->title }}</td>
                            <td>{{  $item->published_at != '' ? $item->published_at->format('d M Y H:i') : '' }}</td>
                            <td>
                                {{ ucwords($item->category->name) }}
                            </td>
                            <td>
                                @if ($item->status == 'APR')
                                    Terbit
                                @elseif ($item->status == 'WAI')
                                    Moderasi
                                @elseif ($item->status == 'REJ')
                                    Ditolak
                                @endif
                            </td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route(ADMIN . '.articles.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        {!! Form::open([
                                            'class'=>'delete',
                                            'url'  => route(ADMIN . '.articles.destroy', $item->id),
                                            'method' => 'DELETE',
                                            ]) 
                                        !!}

                                            <button class="btn btn-danger btn-sm" title="delete"><i class="ti-trash"></i></button>
                                            
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection