@extends('admin.default')

@section('page-header')
    Manajemen Transaksi
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Transaksi</li>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.transaction.index'),
                'method' => 'get'
              ])
            !!}
            {{--<div class='form-group row'>--}}
                {{--<label for="type" class='col-md-1 col-form-label'>Kategori</label>--}}
                {{--<div class='col-md-3'>--}}
                    {{--{!!--}}
                        {{--Form::select(--}}
                            {{--'category',--}}
                            {{--$categories != [] ? ['All' => 'All'] + $categories : ['All' => 'All'],--}}
                            {{--(request()->input('category') ? request()->input('category') : 'All'),--}}
                            {{--[--}}
                                {{--'class' => 'form-control'--}}
                            {{--]--}}
                        {{--)--}}
                    {{--!!}--}}
                {{--</div>--}}
                {{--<label for="type" class='col-md-1 col-form-label'>Status</label>--}}
                {{--<div class='col-md-3'>--}}
                    {{--{!!--}}
                        {{--Form::select(--}}
                            {{--'status',--}}
                            {{--['All' => 'All', 'APR' => 'Diterbitkan', 'WAI' => 'Moderasi', 'REJ' => 'Ditolak'],--}}
                            {{--(request()->input('status') ? request()->input('status') : 'All'),--}}
                            {{--[--}}
                                {{--'class' => 'form-control'--}}
                            {{--]--}}
                        {{--)--}}
                    {{--!!}--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group row">
                <label for="keyword" class='col-md-1 col-form-label'>Kata Kunci</label>
                <div class='col-md-7'>
                    <input class="form-control" placeholder="Cari no. invoice" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">Cari</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>No.Invoice</th>
                        <th>Tanggal</th>
                        <th>User</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Tanggal Bayar</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>No.Invoice</th>
                        <th>Tanggal</th>
                        <th>User</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Tanggal Bayar</th>
                    </tr>
                </tfoot>

                <tbody>
                    @if(count($items) == 0)
                        <tr>
                            <td colspan="6">Tidak ada data yang ditemukan.</td>
                        </tr>
                    @endif
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->invoice_number }}</td>
                            <td>{{ $item->created_at->format('d-m-Y h:i:s') }}</td>
                            <td>{{  ucwords($item->user->name) }}</td>
                            <td>
                                {{ ucwords($item->total) }}
                            </td>
                            <td>
                                {{ ucwords($item->status_text   ) }}
                            </td>
                            <td>
                                {{ $item->paid_at != null ? $item->paid_at->format('d-m-Y h:i:s') : '-' }}
                            </td>
                            {{--<td>--}}
                                {{--<ul class="list-inline">--}}
                                    {{--<li class="list-inline-item">--}}
                                        {{--<a href="{{ route(ADMIN . '.articles.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a>--}}
                                    {{--</li>--}}
                                    {{--<li class="list-inline-item">--}}
                                        {{--{!! Form::open([--}}
                                            {{--'class'=>'delete',--}}
                                            {{--'url'  => route(ADMIN . '.articles.destroy', $item->id),--}}
                                            {{--'method' => 'DELETE',--}}
                                            {{--]) --}}
                                        {{--!!}--}}

                                            {{--<button class="btn btn-danger btn-sm" title="delete"><i class="ti-trash"></i></button>--}}
                                            {{----}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection