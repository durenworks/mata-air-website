
      <li class="nav-item mT-30 active">
        <a class='sidebar-link' href="{{ route(ADMIN . '.dash') }}" default>
          <span class="icon-holder">
            <i class="c-blue-500 ti-home"></i>
          </span>
          <span class="title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
          <a class='sidebar-link' href="{{ route(ADMIN . '.admins.index') }}">
          <span class="icon-holder">
            <i class="c-brown-500 ti-user"></i>
          </span>
              <span class="title">Admins</span>
          </a>
      </li>
      <li class="nav-item">
          <a class='sidebar-link' href="{{ route(ADMIN . '.users.index') }}">
          <span class="icon-holder">
            <i class="c-brown-500 ti-user"></i>
          </span>
              <span class="title">User</span>
          </a>
      </li>

      <li class="nav-item">
          <a class='sidebar-link' href="{{ route(ADMIN . '.transaction.index') }}">
          <span class="icon-holder">
            <i class="c-brown-500 ti-money"></i>
          </span>
              <span class="title">Transaksi</span>
          </a>
      </li>
      <li class="nav-item dropdown">
          <a class="dropdown-toggle" href="javascript:void(0);">
              <span class="icon-holder"><i class="c-black-500 ti-write"></i> </span>
              <span class="title">Artikel</span>
              <span class="arrow"><i class="ti-angle-right"></i></span>
          </a>
          <ul class="dropdown-menu">
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.category.index') }}">Kategori</a>
              </li>
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.articles.index') }}">Artikel</a>
              </li>
          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="dropdown-toggle" href="javascript:void(0);">
              <span class="icon-holder"><i class="c-black-500 ti-gallery"></i> </span>
              <span class="title">Galeri</span>
              <span class="arrow"><i class="ti-angle-right"></i></span>
          </a>
          <ul class="dropdown-menu">
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.galleries.index') }}">Foto</a>
              </li>
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.videos.index') }}">Video</a>
              </li>
          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="dropdown-toggle" href="javascript:void(0);">
              <span class="icon-holder"><i class="c-black-500 ti-book"></i> </span>
              <span class="title">Majalah</span>
              <span class="arrow"><i class="ti-angle-right"></i></span>
          </a>
          <ul class="dropdown-menu">
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.magazines.index') }}">Majalah</a>
              </li>

              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.settings.prices') }}">Harga</a>
              </li>

          </ul>
      </li>

      <li class="nav-item dropdown">
          <a class="dropdown-toggle" href="javascript:void(0);">
              <span class="icon-holder"><i class="c-black-500 ti-settings"></i> </span>
              <span class="title">Setting</span>
              <span class="arrow"><i class="ti-angle-right"></i></span>
          </a>
          <ul class="dropdown-menu">
              <li>
                  <a class="sidebar-link" href="{{ route(ADMIN . '.settings.social') }}">Socials</a>
              </li>
          </ul>
      </li>
