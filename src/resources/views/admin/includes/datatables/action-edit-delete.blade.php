<ul class="list-inline">
    <li class="list-inline-item">
        <a href="{{ route(ADMIN . $route_edit, $id) }}" title="" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
    <li class="list-inline-item">

    <li class="list-inline-item">
        <button class="btn btn-danger btn-sm" title="Verified User" onclick="deleteItem({{ $id }})"><i class="ti-trash"></i></button>
    </li>
</ul>