<ul class="list-inline">
    @if(!$activate)
        <li class="list-inline-item">
            <a href="{{ route(ADMIN . $route_activate, $id) }}" data-toggle="tooltip" title="Activate User" class="btn btn-success btn-sm"><span class="ti-unlock"></span></a></li>
        <li class="list-inline-item">
    @endif

    @if(!$verified)
            <li class="list-inline-item">
                <button class="btn btn-main btn-sm" title="Verified User" onclick="verifyUser({{ $id }})"><i class="ti-check"></i></button>
            </li>
    @endif
</ul>