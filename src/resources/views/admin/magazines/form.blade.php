<div class="row mB-40">
  <div class="col-sm-12">
    <div class="bgc-white p-20 bd">
        {!! Form::myInput('text', 'year', 'Tahun') !!}

        {!! Form::mySelect('roman_edition', 'Edisi', ['I' => 'I', 'II' => 'II', 'III' => 'III', 'IV' => 'IV']) !!}

      @if (isset($item))
        {!! Form::myDate($item->published_at->format('m/d/Y')) !!}
      @else
        {!! Form::myDate() !!}
      @endif

      {!! Form::myTextArea('description', 'Deskripsi', ['id' => 'description']) !!}

      {!! Form::myFile('cover', 'Cover') !!}

      {!! Form::myFile('pdf_preview', 'Preview File') !!}

      {!! Form::myFile('pdf', 'File') !!}

    <input type="hidden" id="articles" value="{{{ json_encode($articles) }}}" name="articles">

      <div id="articles_result">
      </div>
    </div>  
  </div>
</div>