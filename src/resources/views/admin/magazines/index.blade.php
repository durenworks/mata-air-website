@extends('admin.default')

@section('page-header')
    Manajemen Majalah
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Majalah</li>
@endsection

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.magazines.create') }}" class="btn btn-info">
        Tambah
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.magazines.index'),
                'method' => 'get'
              ])
            !!}
            <div class='form-group row'>
                <label for="type" class='col-md-1 col-form-label'>Tahun</label>
                <div class='col-md-3'>
                    <input class="form-control" placeholder="Tahun" name="year" type="text" id="keyword">
                </div>
                <label for="type" class='col-md-1 col-form-label'>Edisi</label>
                <div class='col-md-3'>
                    {!!
                        Form::select(
                            'edition',
                            ['All'=> 'All', 'I' => 'I', 'II' => 'II', 'III' => 'III', 'IV' => 'IV'],
                            (request()->input('status') ? request()->input('status') : 'All'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Tahun</th>
                        <th>Edisi</th>
                        <th>Tanggal Terbit</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Tahun</th>
                        <th>Edisi</th>
                        <th>Tanggal Terbit</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </tfoot>

                <tbody>
                    @if(count($items) == 0)
                        <tr>
                            <td colspan="5">Tidak ada data yang ditemukan.</td>
                        </tr>
                    @endif
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->year }}</td>
                            <td>{{ $item->roman_edition}}</td>
                            <td>{{  $item->published_at != '' ? $item->published_at->format('d M Y') : '' }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route(ADMIN . '.magazines.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a>
                                    </li>
                                    <li class="list-inline-item">
                                        {!! Form::open([
                                            'class'=>'delete',
                                            'url'  => route(ADMIN . '.magazines.destroy', $item->id),
                                            'method' => 'DELETE',
                                            ])
                                        !!}

                                        <button class="btn btn-danger btn-sm" title="delete"><i class="ti-trash"></i></button>

                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection