<script type="x-tmpl-mustache" id="apply_articles">
  <table class="table">
    <thead>
        <tr>
            <th class="d-none d-sm-table-cell" style="width: 300px;">Title</th>
            <th class="d-none d-sm-table-cell" style="width: 300px;">
              Document File
            </th>
            <th class="d-none d-sm-table-cell" style="width: 300px;">
              Audio File
            </th>
            <th style="width: 150px;">Action</th>
        </tr>
    </thead>
    <tbody>
      {% #articles %}
      <tr>
        <td>
          <span class="listText_{% _idx %}">{% title %}</span>
          <input
            type='text'
            value="{% title %}"
            id="editTitle_{% _idx %}"
            class="form-control hidden listForm_{% _idx %}"
          />
        </td>
        <td>
          <div class='w-300'>
            <a href={% doc_url %} class="listText_{% _idx %}" target="_blank">
              {% doc_url %}
            </a>
          </div>
          <input 
            type="file"
            class="form-control input-doc hidden listForm_{% _idx %}"
            id="EditDocumentFile_{% _idx %}"
            accept=".pdf"
            data-type='doc'
            data-action='edit'
            data-id="{% _idx %}"
          />
          <img
            src='/images/spinner.gif'
            class='img-spinner hidden'
            id='docSpinner_{% _idx %}'
          />
        </td>
        <td>
          <div class='w-300'>
            <a href={% audio_url %} class="listText_{% _idx %}" target="_blank">
              {% audio_url %}
            </a>
          </div>
          <input 
            type="file"
            class="form-control input-audio hidden listForm_{% _idx %}"
            id="EditAudioFile_{% _idx %}"
            data-action='edit'
            data-id="{% _idx %}"
          />
          <img
            src='/images/spinner.gif'
            class='img-spinner hidden'
            id='audioSpinner_{% _idx %}'
          />
        </td>
        <td>
          <div class='default-button-{% _idx %}'>
            <button
              type="button"
              class="btn btn-sm btn-primary btn-edit-article mr-3"
              data-id="{% _idx %}"
            >
              <span class="si si-pencil"></span> Edit
            </button>
            <button
              type="button"
              class="btn btn-sm btn-danger btn-remove-article"
              data-id="{% _idx %}"
            >
              <span class="si si-pencil"></span> Remove
            </button>
          </div>
          <div class='additonal-button-{% _idx %} hidden'>
            <button
              type="button"
              class="btn btn-sm btn-success btn-save-article"
              data-id="{% _idx %}"><span class="si si-pencil"></span> Save
            </button>
          </div>
        </td>
      </tr>
      {% /articles %}
    </tbody>
    <tfooter>
      <tr id='table_attribute_footer'>
        <td>
          <input type='text' id="addArticleTitle" class="form-control" />
          <span class='text-danger hidden'>Title is required</span>
        </td>
        <td>
            <input type="file"
              class="form-control input-doc"
              id="AddDocumentFile"
              accept=".pdf"
              data-action='add'
            />
            <span class='text-danger hidden'>File is required</span>
            <img
              src='/images/spinner.gif'
              class='img-spinner hidden'
              id='docSpinner'
            />
        </td>
        <td>
            <input
              type="file"
              class="form-control input-audio"
              id="AddAudioFile"
              data-action='add'
            />
            <img
              src='/images/spinner.gif'
              class='img-spinner hidden'
              id='audioSpinner'
            />
        </td>
        <td>
          <button
            class="btn btn-sm btn-success"
            type="button"
            id="btnAddArticle"
          >
            <span class="fa fa-plus"></span>
          </button>
        </td>
      </tr>
    </tfooter>
  </table>
</script>