@extends('admin.default')

@section('content-css')
  <link href="{{ mix('css/custom.css')}}" rel="stylesheet" />
@endsection
@section('page-header')
  Tambah Majalah
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.articles.index') }}>Majalah</a></li>
    <li class="active">Tambah</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\MagazineController@store'],
        'files' => true
      ])
    !!}

    @include('admin.magazines.form')

    <button type="submit" class="btn btn-primary">Simpan</button>
    
  {!! Form::close() !!}
  

@stop

@section('content-js')
  <script src="{{ asset('js/mustache.js') }}"></script>
  <script type="text/javascript">
      // set global moustache tags
      Mustache.tags = ['{%', '%}'];
  </script>
  @include('admin.magazines._article_mustache_template')
  <script type="text/javascript">
    $(function(){
        var articles = JSON.parse($('#articles').val());
        let doc_id = 0
        let doc_url = ''
        let audio_id = 0
        let audio_url = ''
        var save_id
        function render()
        {
            getIndex();
            $('#articles').val(JSON.stringify(articles));
            var tmpl = $('#apply_articles').html();
            Mustache.parse(tmpl);
            var data = {  'articles' : articles };
            var html = Mustache.render(tmpl, data);
            $('#articles_result').html(html);

            bind();
        }

        function bind()
        {
          $('#btnAddArticle').off('click', addArticle).on('click', addArticle);
          $('.btn-remove-article').off('click', removeArticle).on('click', removeArticle);
          $('.btn-edit-article').off('click', editArticle).on('click', editArticle);
          $('.btn-save-article').off('click', saveArticle).on('click', saveArticle);
          $('.input-doc').off('change', uploadDoc).on('change', uploadDoc);
          $('.input-audio').off('change', uploadAudio).on('change', uploadAudio);
        }

        function getIndex() 
        {   
            for (idx in articles) {
                articles[idx]['_idx'] = idx;
            }
        }

        function addArticle()
        {
          const title = $('#addArticleTitle').val()

          const input = {
            'article_id': '',
            'doc_id': doc_id,
            'doc_url': doc_url,
            'audio_id': audio_id,
            'audio_url': audio_url,
            'title': title
          };

          articles.push(input);
          resetState();
          render ();
        }

        function editArticle(){
            const id = $(this).data('id')

            $('.listText_' + id).addClass('hidden')
            $('.default-button-' + id).addClass('hidden')
            $('#table_attribute_footer').addClass('hidden')
            $('.listForm_' + id).removeClass('hidden')
            $('.additonal-button-' + id).removeClass('hidden')
        }

        function saveArticle(){
            const id = $(this).data('id')
            const title = $('#editTitle_' + id).val()
            
            articles[id].title = title
            if(doc_id){
              articles[id].doc_id = doc_id
              articles[id].doc_url = doc_url
            }

            if(audio_id){
              articles[id].audio_id = audio_id
              articles[id].audio_url = audio_url
            }
            resetState()
            render ();
        }

        function removeArticle(){
            const id = $(this).data('id')
            
            articles.splice(id, 1);

            render ();
        }

        function resetState(){
          let doc_id = 0
          let doc_url = ''
          let audio_id = 0
          let audio_url = ''
        }

        render();

        function uploadDoc(){
          const action = $(this).data('action')
          const idx = $(this).data('id')
          let formData = new window.FormData()
          $(this).addClass('hidden')
          if(idx !== undefined){
            formData.append('file', $('#EditDocumentFile_' + idx).prop('files')[0])
            $('#docSpinner_' + idx).removeClass('hidden')
          }
          else{
            $('#docSpinner').removeClass('hidden')
            formData.append('file', $('#AddDocumentFile').prop('files')[0])
          }
          uploadFile(formData, 'doc', action, idx)
        }

        function uploadAudio(){
          const action = $(this).data('action')
          const idx = $(this).data('id')
          let formData = new window.FormData()
          $(this).addClass('hidden')
          if(idx !== undefined){
            formData.append('file', $('#EditAudioFile_' + idx).prop('files')[0])
            $('#audioSpinner_' + idx).removeClass('hidden')
          }
          else{
            $('#audioSpinner').removeClass('hidden')
            formData.append('file', $('#AddAudioFile').prop('files')[0])
          }
          uploadFile(formData, 'audio', action, idx)
        }

        function uploadFile(formData, type, action, idx=0){
          $.ajax({
            url: '{!! route(ADMIN . '.megazine.upload_file') !!}',
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: 'post',
            data: formData,
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            dataType: 'json',
            success: function (data) {
              if(data.type == 'doc'){
                doc_id = data.id
                doc_url = data.url
                if(action == 'add'){
                  $('#AddDocumentFile').removeClass('hidden')
                }else if(action == 'edit'){
                  $('#EditDocumentFile_' + idx).removeClass('hidden')
                }
                $('.img-spinner').addClass('hidden')
              }else if(data.type == 'audio'){
                audio_id = data.id
                audio_url = data.url
                if(action == 'add'){
                  $('#AddAudioFile').removeClass('hidden')
                }else if(action == 'edit'){
                  $('#EditAudioFile_' + idx).removeClass('hidden')
                }
                $('.img-spinner').addClass('hidden')
              }
            },
            error: function (data){
                console.log(data)
            },
          });
        }
    })
</script>
@endsection



