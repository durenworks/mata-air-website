@extends('admin.default')

@section('page-header')
  User edit
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.admins.index') }}>Admin</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\AdminController@update', $item->id],
        'method' => 'put', 
        'files' => true
      ])
    !!}

    @include('admin.admins.form')

    <button type="submit" class="btn btn-primary">Edit</button>
    
  {!! Form::close() !!}
  

@stop

