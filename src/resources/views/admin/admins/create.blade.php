@extends('admin.default')

@section('page-header')
  Add new admin
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.admins.index') }}>Admin</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\AdminController@store'],
        'files' => true
      ])
    !!}

    @include('admin.admins.form')

    <button type="submit" class="btn btn-primary">Add</button>
    
  {!! Form::close() !!}
  

@stop

