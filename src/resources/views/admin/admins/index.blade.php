@extends('admin.default')

@section('page-header')
    Admins management
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Admins</li>
@endsection

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.admins.create') }}" class="btn btn-info">
       Add
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.admins.index'),
                'method' => 'get'
              ])
            !!}
            <div class='form-group row'>
                <label for="status" class='col-md-1 col-form-label'>Status</label>
                <div class='col-md-3'>
                    {!!
                        Form::select(
                            'status',
                            ['1' => 'Active', '0' => 'Inactive'],
                            (request()->input('is_active') ? request()->input('is_active') : 'Active'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>
                <label for="keyword" class='col-md-1 col-form-label'>Keyword</label>
                <div class='col-md-3'>
                    <input class="form-control" placeholder="Search with username or email" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">Search</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
             
                <tfoot >
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
             
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route(ADMIN . '.admins.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open([
                                            'class'=>'delete',
                                            'url'  => route(ADMIN . '.admins.destroy', $item->id),
                                            'method' => 'DELETE',
                                            ]) 
                                        !!}

                                            <button class="btn btn-danger btn-sm" title="delete}"><i class="ti-trash"></i></button>
                                            
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection