<div class="row mB-40">
  <div class="col-sm-12">
    <div class="bgc-white p-20 bd">
      {!! Form::myInput('text', 'name', 'Name') !!}
    
      {!! Form::myInput('email', 'email', 'Email') !!}
    
      {!! Form::myInput('password', 'password', 'Password') !!}

      {!! Form::myInput('password', 'password_confirmation', 'Password again') !!}

      {!! Form::myInput('text', 'phone', 'Phone') !!}

      {!! Form::myFile('avatar', 'Avatar') !!}

    </div>  
  </div>
</div>