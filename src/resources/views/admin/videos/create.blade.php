@extends('admin.default')

@section('page-header')
  Tambah Galeri
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.videos.index') }}>Video</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\VideoController@store'],
        'files' => true
      ])
    !!}

    @include('admin.videos.form')

    <button type="submit" class="btn btn-primary">Simpan</button>
    
  {!! Form::close() !!}
  

@stop

