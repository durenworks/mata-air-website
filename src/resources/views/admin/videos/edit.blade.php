@extends('admin.default')

@section('page-header')
  Ubah
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.videos.index') }}>Video</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\VideoController@update', $item->id],
        'method' => 'put', 
        'files' => true
      ])
    !!}

    @include('admin.videos.form')

    <button type="submit" class="btn btn-primary">Simpan</button>
    
  {!! Form::close() !!}
  

@stop

