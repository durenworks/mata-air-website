<div class="row mB-40">
  <div class="col-sm-12">
    <div class="bgc-white p-20 bd">
        {!! Form::myInput('text', 'name', 'Nama') !!}

      {!! Form::myInput('text', 'email', 'Email') !!}

      {!! Form::myInput('text', 'phone', 'Telepon') !!}
      {!! Form::myInput('text', 'occupation', 'Pekerjaan') !!}
      {!! Form::myInput('text', 'subscription_status_text', 'Status Berlangganan') !!}
      {!! Form::myInput('text', 'expired_date', 'Expired') !!}

      {!! Form::myTextArea('street', 'Alamat') !!}

      {!! Form::myInput('text', 'residence', 'Nama Perumahan') !!}

      {!! Form::myInput('text', 'rtrw', 'RT / RW') !!}
      {!! Form::myInput('text', 'urban_village', 'Kelurahan') !!}
      {!! Form::myInput('text', 'subdistrict', 'Kecamatan') !!}
      {!! Form::myInput('text', 'city', 'Kota') !!}
      {!! Form::myInput('text', 'province', 'Provinsi') !!}


    </div>  
  </div>
</div>