@extends('admin.default')

@section('page-header')
    Detail Pengguna
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.users.index') }}>User</a></li>
    <li class="active">Detail</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'method' => ''
      ])
    !!}

    @include('admin.users.form')

    <div class="form-group row">
        <div class='col-md-1'>
            <a href="{{ route(ADMIN . '.users.index', $item->id) }}" class="btn btn-danger" role="button" aria-pressed="true">Kembali</a>
        </div>
    </div>


    {!! Form::close() !!}
@stop


