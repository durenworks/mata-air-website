@extends('admin.default')

@section('page-header')
    Manajemen Pelanggan
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">User</li>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.users.index'),
                'method' => 'get'
              ])
            !!}
            {{--<div class='form-group row'>--}}
                {{--<label for="type" class='col-md-1 col-form-label'>Kategori</label>--}}
                {{--<div class='col-md-3'>--}}
                    {{--{!!--}}
                        {{--Form::select(--}}
                            {{--'category',--}}
                            {{--$categories != [] ? ['All' => 'All'] + $categories : ['All' => 'All'],--}}
                            {{--(request()->input('category') ? request()->input('category') : 'All'),--}}
                            {{--[--}}
                                {{--'class' => 'form-control'--}}
                            {{--]--}}
                        {{--)--}}
                    {{--!!}--}}
                {{--</div>--}}
                {{--<label for="type" class='col-md-1 col-form-label'>Status</label>--}}
                {{--<div class='col-md-3'>--}}
                    {{--{!!--}}
                        {{--Form::select(--}}
                            {{--'status',--}}
                            {{--['All' => 'All', 'APR' => 'Diterbitkan', 'WAI' => 'Moderasi', 'REJ' => 'Ditolak'],--}}
                            {{--(request()->input('status') ? request()->input('status') : 'All'),--}}
                            {{--[--}}
                                {{--'class' => 'form-control'--}}
                            {{--]--}}
                        {{--)--}}
                    {{--!!}--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group row">
                <label for="type" class='col-md-1 col-form-label'>Berlangganan</label>
                <div class='col-md-3'>
                    {!!
                        Form::select(
                            'status',
                            ['ALL' => 'All', 'ACT' => 'AKTIF', 'EXP' => 'TIDAK AKTIF'],
                            (request()->input('status') ? request()->input('status') : 'All'),
                            [
                                'class' => 'form-control'
                            ]
                        )
                    !!}
                </div>
                <label for="keyword" class='col-md-1 col-form-label'>Kata Kunci</label>
                <div class='col-md-4'>
                    <input class="form-control" placeholder="Cari nama atau email" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary">Cari</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>Status Berlangganan</th>
                        <th>Expired</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>Status Berlangganan</th>
                        <th>Expired</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </tfoot>

                <tbody>
                    @if(count($items) == 0)
                        <tr>
                            <td colspan="7">Tidak ada data yang ditemukan.</td>
                        </tr>
                    @endif
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ ucwords($item->name) }}</td>
                            <td>{{  $item->email }}</td>
                            <td>
                                {{ ucwords($item->city) }}
                            </td>
                            <td>
                                {{ ucwords($item->province) }}
                            </td>
                            <td>
                                {{ $item->subscription_status ? 'Aktif' : 'Tidak Aktif' }}
                            </td>
                            <td>
                                {{ $item->expired_date != '-' ? $item->expired_date : '-' }}
                            </td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="{{ route(ADMIN . '.users.detail', $item->id) }}" title="detail" class="btn btn-primary btn-sm"><span class="ti-zoom-out"></span></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            
        </table>
      </div>
    </div>
  </div>

@endsection