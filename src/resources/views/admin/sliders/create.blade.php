@extends('admin.default')

@section('page-header')
    Add new slider
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.sliders.index') }}>Slider</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\SliderController@store'],
        'files' => true
      ])
    !!}

    @include('admin.sliders.form')

    <button type="submit" class="btn btn-primary">Save</button>

    {!! Form::close() !!}


@stop

