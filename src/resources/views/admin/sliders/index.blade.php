@extends('admin.default')

@section('page-header')
   Sliders Management
@endsection

@push('styles')
    <style>
        #extra-data-datatables-attributes tbody tr {
            cursor: pointer;
        }
    </style>
@endpush

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.sliders.create') }}" class="btn btn-info">
        Add
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            <div class="form-group row" >
                <label for="keyword" class='col-md-1 col-form-label'>Keyword</label>
                <div class='col-md-3'>
                    <input class="form-control" placeholder="Search with title or text" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary" id="btn-search">Search</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            
                <thead>
                    <tr>
                        <th>Title</th>
                        <th width="10%">Actions</th>
                        <th width="0">is-active</th>
                    </tr>
                </thead>
        </table>
      </div>
    </div>
  </div>

@endsection

@section('content-js')
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#data-table').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                searching: false,
                lengthChange: false,
                pageLength: 50,
                ajax: {
                    "url" : "{{ route(ADMIN . '.datatables.slider') }}",
                    "data": function(d){
                        d.keyword = getKeywordSearch();
                    }
                },
                columns: [
                    { name: 'title' },
                    { name: 'action', orderable: false, searchable: false },
                    { name: 'is_active', visible: false },
                ]
            });

            $( "#btn-search" ).click(function() {
                 table.ajax.reload();
            });
        });

        function getKeywordSearch() {
            return $('#keyword').val();
        }

        function deleteItem(id) {
            swal({
                title: 'Delete this item ?',
                text: 'This Action Cannot Be Undone',
                icon: 'warning',
                buttons: {
                    cancel: true,
                    confirm: true,
                },
            }).then(function(isConfirm) {
                if (isConfirm === true) {
                    $.ajax({
                        type: "put",
                        url: "{{ route(ADMIN . '.sliders.delete') }}",
                        data: {
                            _token : "{{ csrf_token() }}",
                            id: id
                        },
                    }).done(function (response) {
                        if (response.status === 'success') {
                            swal({
                                icon: 'success',
                                title: 'Success',
                                text: 'Done'
                            }).then(function() {
                                $('#data-table').DataTable().ajax.reload();
                            })
                        }
                        else {
                            swal({
                                icon: 'error',
                                title: 'Failed',
                                text: 'Something went wrong!'
                            }).then(function() {
                                $('#data-table').DataTable().ajax.reload();
                            })
                        }
                    });
                }
            });
        }

    </script>

@endsection