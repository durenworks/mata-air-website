@extends('admin.default')

@section('page-header')
  Edit Slider
@stop

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\SliderController@update', $item->id],
        'method' => 'put', 
        'files' => true
      ])
    !!}

    @include('admin.sliders.form')

    <button type="submit" class="btn btn-primary">Save</button>
    
  {!! Form::close() !!}
  

@stop

