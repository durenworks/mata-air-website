@extends('admin.default')

@section('page-header')
    Setting Harga
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Setting Harga</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\SettingController@pricesStore']
      ])
    !!}

    <div class="row mB-40">
        <div class="col-sm-12">
            <div class="bgc-white p-20 bd">
                <div class='form-group row'>
                    <label class="col-md-1">Harga Subscribe </label>
                    <div class="col-md">
                        {!!  Form::input(
                              'number',
                              'subscribePrice',
                              $subscribePrice != '0' ? $subscribePrice : '0',
                              [
                                    'class' => 'form-control',
                                    'id' => 'subscribePrice',
                                    'placeholder' => 'Harga subscribe per tahun'
                              ]
                            )
                         !!}
                    </div>
                </div>

                {{--<div class='form-group row'>--}}
                    {{--<label class="col-md-1">Harga Majalah </label>--}}
                    {{--<div class="col-md">--}}
                        {{--{!!  Form::input(--}}
                              {{--'number',--}}
                              {{--'magazinePrice',--}}
                              {{--$magazinePrice != '0' ? $magazinePrice : '0',--}}
                              {{--[--}}
                                    {{--'class' => 'form-control',--}}
                                    {{--'id' => 'magazinePrice',--}}
                                    {{--'placeholder' => 'Harga majalah'--}}
                              {{--]--}}
                            {{--)--}}
                         {{--!!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>



    <button type="submit" class="btn btn-primary">Save</button>

    {!! Form::close() !!}


@stop

@section('content-js')
    <script src="{{ mix('/js/region.js') }}"></script>
@endsection

