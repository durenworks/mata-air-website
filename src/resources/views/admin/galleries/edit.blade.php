@extends('admin.default')

@section('page-header')
  Edit Photo
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.galleries.index') }}>Galleries</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    {!! Form::model($item, [
        'action' => ['Admin\GalleryController@update', $item->id],
        'method' => 'put', 
        'files' => true
      ])
    !!}

    @include('admin.galleries.form')

    <button type="submit" class="btn btn-primary">Save</button>
    
  {!! Form::close() !!}
@stop

