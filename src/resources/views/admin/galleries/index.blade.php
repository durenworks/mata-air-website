@extends('admin.default')

@section('page-header')
    Manajemen Galeri
@endsection

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li class="active">Gallery</li>
@endsection

@section('content')

<div class="mB-20">
    <a href="{{ route(ADMIN . '.galleries.create') }}" class="btn btn-info">
        Tambah
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
            {!! Form::open([
                'role' => 'form',
                'url' => route(ADMIN . '.galleries.index'),
                'method' => 'get'
              ])
            !!}
            <div class='form-group row'>
                <label for="keyword" class='col-md-1 col-form-label'>Keyword</label>
                <div class='col-md-3'>
                    <input class="form-control" placeholder="Search with title" name="keyword" type="text" id="keyword">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">Search</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    @foreach($items as $item)
        <div class="col-md-4" id="div{{$item->id}}">
            <div class="thumbnail">
                <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="{{ route('show.image',['galleries', $item->items->first()->image])}}" alt="image">
                </div>
                <div class="caption text-center">
                    {{$item->tittle}}<br>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="{{ route(ADMIN . '.galleries.edit', $item->id) }}" title="edit" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a>
                        </li>
                        <li class="list-inline-item">
                            {!! Form::open([
                                'class'=>'delete',
                                'url'  => route(ADMIN . '.galleries.destroy', $item->id),
                                'method' => 'DELETE',
                                ])
                            !!}

                            <button class="btn btn-danger btn-sm" title="delete"><i class="ti-trash"></i></button>

                            {!! Form::close() !!}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
</div>

@endsection