<div class="row mB-40">
  <div class="col-sm-12">
    <div class="bgc-white p-20 bd">
      {!! Form::myInput('text', 'tittle', 'Judul') !!}

        @if($item == '')
            <div class='form-group'>
                <label>Foto</label>
                <div class="image-input">
                    <div class="input-wrapper form-group">
                        <input type="file" name="image[]">
                        <button type="button" class="btn-remove"><span class="ti-trash"></span></button>
                    </div>
                </div>
                <button id="add-image" type="button" class="btn btn-success">Tambah Foto</button>
                <br><br>
            </div>
        @else
            <div class="form-group">
                <label for="image" class="control-label">
                    Foto
                </label>
                <div class="control-input {{ $errors->has('image') ? 'has-error' : '' }}">
                    @if ($item->items != '')
                        <div class="row">
                            @foreach ($item->items as $image)
                                {{--<div class="thumbnail">
                                    <div class="image view view-first">
                                        <img class="thumbnail" src="{{ action('HomeController@showProductImage', [$image->image]) }}" alt="image">
                                    </div>
                                    <div class="caption">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <button type="button" data="{{ $image->id }}" class="btn btn-danger btn-sm btn-delete"><span class="ti-trash"></span></button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>--}}
                                <div class="gallery-image">
                                    <img class="thumbnail" src="{{ route('show.image', $image->image) }}" height="170">
                                    <button type="button" data="{{ $image->id }}" class="btn-delete btn btn-md btn-danger"><span class="ti-close"></span></button>
                                </div>
                            @endforeach
                        </div>
                    @endif
                <!-- input for soft delete image -->
                    <input id="delete_image" type="hidden" name="delete_image">

                    <div class="image-input">
                        <div class="input-wrapper form-group">
                            <input type="file" name="image[]">
                            <button type="button" class="btn-remove"><span class="ti-trash  "></span></button>
                        </div>
                    </div>
                    <button id="add-image" type="button" class="btn btn-success">Tambah Foto</button>
                    <br><br>

                    @if ($errors->has('image'))
                        <span class="help-block text-danger">{{ $errors->first('image') }}</span>
                    @endif
                </div>
            </div>
        @endif


    </div>  
  </div>
</div>