@extends('admin.default')

@section('page-header')
    Tambah Galeri Foto
@stop

@section('breadcrumb')
    <li><a href={{ route(ADMIN . '.dash') }}>Dashboard</a></li>
    <li><a href={{ route(ADMIN . '.galleries.index') }}>Galeri</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    {!! Form::open([
        'action' => ['Admin\GalleryController@store'],
        'files' => true
      ])
    !!}

    @include('admin.galleries.form')

    <button type="submit" class="btn btn-primary">Save</button>

    {!! Form::close() !!}

@stop

@section('content-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.0.1/mustache.min.js"></script>
    <script>
        // add image
        $('#add-image').click(function(){
            $('.image-input').append('<div class="input-wrapper form-group"><input type="file" name="image[]"><button type="button" class="btn-remove"><span class="ti-trash"></span></button></div>');
        });
        // remove input file
        $('.image-input').on('click', '.btn-remove', function(){
            $(this).parent().remove();
        });
    </script>
@endsection

