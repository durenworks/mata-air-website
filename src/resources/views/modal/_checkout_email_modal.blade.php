<div class="modal fade auth-modal" id="checkout-email-modal" tabindex="-1" role="dialog" aria-labelledby="Daftar"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content bg-white">
      <div class="modal-header pb-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="container">
          <div class="row">
            <div class="col-12 form-title text-center border-bottom-1">
              <h2>Masukan Email dan No Handphone</h2>
            </div>
            <form method="post" action="{{ route('f.checkout.store_email') }}" class="w-100">
              <div class="col-12 form-auth">
                {{ csrf_field() }}
                <div class="form-group row">
                  <label for="email" class="col-sm-4 col-form-label">Email *</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" value="" id="email" placeholder="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="hp" class="col-sm-4 col-form-label">Nomor Handphone *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="phone" value="" id="phone" placeholder="" required>
                  </div>
                </div>
              </div>
              <div class="col-12 border-top-1 pt-3">
                <div class="d-flex justify-content-end">
                  <button type="button" class="btn btn-outline-primary mr-2" data-dismiss="modal">
                    Batal
                  </button>
                  <button type="submit" class="btn btn-primary">
                    Submit
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>