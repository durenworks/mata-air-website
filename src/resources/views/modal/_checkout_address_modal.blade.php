<div class="modal fade auth-modal" id="checkout-address-modal" tabindex="-1" role="dialog" aria-labelledby="Daftar"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content bg-white">
      <div class="modal-header pb-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="container">
          <div class="row">
            <div class="col-12 form-title text-center border-bottom-1">
              <h2>Tambah Alamat Baru</h2>
            </div>
            <div class="col-12 form-auth">
              <form method="post" action="#">
                <div class="form-group row">
                  <label for="name" class="col-sm-4 col-form-label">Nama Pelanggan *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" value="" id="formName" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-4 col-form-label">Alamat *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="" name="street" id="formStreet" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-4 col-form-label">Nama Perumahan *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="" name="residence" id="formResidence" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="rt" class="col-sm-4 col-form-label">RT</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="rt" value="" id="formRt" placeholder="">
                  </div>
                  <label for="rw" class="col-sm-2 col-form-label">RW</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="rw" value="" id="formRw" placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="desa" class="col-sm-4 col-form-label">Desa / Dusun</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="village" value="" id="formVillage" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="keluarahan" class="col-sm-4 col-form-label">Kelurahan</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="urban_village" value="" id="formUrbanVillage"
                      placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kecamatan" class="col-sm-4 col-form-label">Kecamatan</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="subdistrict" value="" id="formSubdistrict"
                      placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kabupaten" class="col-sm-4 col-form-label">Kota</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="city" value="" id="formCity" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="provinsi" class="col-sm-4 col-form-label">Provinsi</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="province" value="" id="formProvince" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kodepos" class="col-sm-4 col-form-label">Kode Pos *</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="postal_code" value="" id="formPostalCode"
                      placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="hp" class="col-sm-4 col-form-label">Nomor Handphone *</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="phone" value="" id="formPhone" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="email" class="col-sm-4 col-form-label">Email *</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" value="" id="formEmail" placeholder="">
                  </div>
                </div>
              </form>
            </div>
            <div class="col-12 border-top-1 pt-3">
              <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-outline-primary mr-2" data-dismiss="modal">
                  Batal
                </button>
                <button type="submit" class="btn btn-primary" id="addAddressButton">
                  Tambah dan Gunakan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>