@extends('layouts.frontend',
            [
                'title'=>'TOS',
                'active'=>'tos',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header  bg-mata-air">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Term of Use</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        Find Up In
                    </p>
                    <h1 class="title">Term of Services</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-12 text-center">
                    <h2 class="text-brand text-uppercase">
                        Syarat dan Ketentuan Mata Air
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5">
                    <h3 class="text-brand text-uppercase">Pendahuluan</h3>
                    <p>
                        Syarat dan ketentuan berikut berlaku dan mengikat kepada seluruh pengunjung situs www.majalahmataair.co.id dan pelanggan Mata Air®.
                    </p>
                    <p>
                        Bacalah dengan teliti dan seksama sebelum melakukan pendaftaran langganan. Jika ada keterangan yang belum jelas mengenai syarat dan ketentuan ini, silakan menghubungi layanan pelanggan Mata Air® di (021) 791 93 715 atau email di info@majalahmataair.co.id
                    </p>

                    <h3 class="text-brand text-uppercase">Definisi</h3>
                    <p class="text-brand">Mata Air®</p>
                    <p>
                        Mata Air® adalah merek dagang terdaftar milik PT UFUK BARU. Mata Air® terbit setiap 3 (tiga) bulan sekali setiap tahun.
                    </p>
                    <p class="text-brand">Calon Pelanggan</p>
                    <p>
                        Pelanggan Mata Air® merupakan pendaftar langganan Mata Air® yang telah membayar biaya langganan, mengisi dan mengirimkan data langganan melalui situs www.majalahmataair.co.id namun belum tervalidasi data langganannya. Calon pelanggan akan diminta untuk memperbaiki data langganan jika ditemukan terdapat kesalahan atau kejanggalan dalam isian data tersebut.
                    </p>
                    <p class="text-brand">Pelanggan Mata Air®</p>
                    <p>
                        Pelanggan Mata Air® merupakan pendaftar langganan Mata Air® yang telah membayar biaya langganan, mengisi dan mengirimkan data langganan melalui situs www.majalahmataair.co.id dan telah tervalidasi data langganannya. Pelanggan Mata Air®akan mendapat pemberitahuan status langganan melalui email ketika data langganan selesai divalidasi.
                    </p>

                    <h3 class="text-brand text-uppercase">PRODUK DAN LAYANAN</h3>
                    <p class="text-brand">Mata Air®</p>
                    <p>
                        Mata Air® merupakan majalah sains, budaya dan spiritualitas yang isinya menyajikan topik-topik  utama seputar kehidupan manusia, seperti : sains-teknologi, psikologi, keragaman budaya, peradaban dan pendidikan.
                    </p>
                    <p>
                        Harga Mata Air® adalah Rp. 50.000,- per edisi.
                    </p>

                    <h3 class="text-brand text-uppercase">SISTEM LANGGANAN</h3>
                    <p>Mata Air® memiliki sistem langganan dengan rincian sebagai berikut:</p>
                    <ol>
                        <li>
                            1 (satu) tahun langganan dengan biaya Rp. 160.000,-. Pelanggan akan mendapatkan 4 (empat) edisi majalah yang akan dikirim ke alamat penerima sesuai dengan yang dituliskan di kolom alamat pada waktu proses pendaftaran.
                        </li>
                        <li>
                            b. 2 (dua) tahun langganan dengan biaya Rp. 320.000,-. Pelanggan akan mendapatkan 8 (delapan) edisi majalah yang akan dikirim ke alamat penerima sesuai dengan yang dituliskan di kolom alamat pada waktu proses pendaftaran.
                        </li>
                    </ol>

                    <h3 class="text-brand text-uppercase">PROSEDUR PENDAFTARAN LANGGANAN</h3>
                    <ol>
                        <li>Calon pelanggan melakukan pembayaran pada akun Bank Mandiri milik PT UFUK BARU.</li>
                        <li>Calon pelanggan mengakses situs resmi Mata Air® di www.majalahmataair.co.id.</li>
                        <li>Mengisi data diri dan melakukan submit data.</li>
                        <li>Calon pelanggan menunggu proses verifikasi oleh Mata Air® mulai dari validitas data dan pembayaran.</li>
                        <li>Setelah data di verifikasi, jika terdapat daya yang tidak valid maka calon pelanggan akan dikirimkan pemberitahuan melalui email untuk memperbaiki data tersebut. Jika data tersebut valid, maka akan dikirimkan notifikasi bahwa calon pelanggan tersebut telah resmi berlangganan Mata Air®.</li>
                    </ol>

                    <h3 class="text-brand text-uppercase">Definisi</h3>
                    <p class="text-brand">Mata Air®</p>
                    <p>
                        Mata Air® adalah merek dagang terdaftar milik PT UFUK BARU. Mata Air® terbit setiap 3 (tiga) bulan sekali setiap tahun.
                    </p>

                    <h3 class="text-brand text-uppercase">PEMBAYARAN</h3>
                    <p class="text-brand">TRANSFER BANK</p>
                    <p>
                        Mata Air® menerima pembayaran melalui rekening Bank MANDIRI dengan detail sebagai berikut :
                        <br>Atas Nama : PT. UFUK BARU
                        <br>No Rekening : 070-00-2020212-8
                        <br>Kantor Cabang : Mandiri Cabang Mampang
                    </p>
                    <p class="text-brand">KARTU KREDIT VISA DAN MASTERCARD</p>
                    <p>
                        Pembayaran Kartu Kredit dapat dilakukan dengan prosedur sebagai berikut :
                    </p>
                    <ol>
                        <li>Calon pelanggan mengakses situs resmi Mata Air® www.majalahmataair.co.id </li>
                        <li>Pelanggan dipersilakan untuk memilih paket langganan majalah yang diinginkan</li>
                        <li>
                            Setelah selesai memilih paket langganan  kemudian customer akan masuk kedalam payment page Mata Air® (PT UFUK BARU) dengan informasi pemesanan sebagai berikut :
                            <ol>
                                <li>Nama dan alamat pelanggan.</li>
                                <li>Jenis kartu kredit yang di gunakan</li>
                                <li>Nomer kartu dan CVV kartu kredit Visa atau Mastercard.</li>
                                <li>Jenis dan jumlah produk yang di pesane. Alamat pengiriman atau pengambilan produk.</li>
                            </ol>
                        </li>
                        <li>Apabila kartu kredit sudah 3D secure maka muncul tabel pop up yang meminta customer untuk memasukkan OTP (One Time Password) yang dikirimkan oleh Bank penerbit kartu  ke telefon yang nomornya sudah didaftarkan.</li>
                        <li>Setelah pemesanan produk pada payment page berhasil , kemudian akan ada approval  atas transaksi online tersebut.</li>
                        <li>Pelanggan di wajibkan meng-klik / menekan disclaimer (“I Accept”) sebelum melakukan approval pembayaran  online terhadap syarat dan ketentuan yang diberikan oleh  Mata Air® (PT UFUK BARU).</li>
                        <li>Pelanggan membayar sebagai kesediaan dan persetujuan transaksi pemesanan sebesar nilai transaksi tersebut. Hal tersebut ditandai dengan approval atau persetujuan dari Bank pemroses kartu yang telah melakukan pendebetan.</li>
                        <li>PT UFUK BARU berhak menolak transaksi pelanggan apabila dianggap tidak sesuai.</li>
                    </ol>

                    <h3 class="text-brand text-uppercase">PENGIRIMAN</h3>
                    <ol>
                        <li>Pengiriman akan dilakukan pada saat majalah terbit per tiga bulan ke alamat penerima yang dimasukkan saat proses pendaftaran.</li>
                        <li>Retour atau KEMBALI KE ALAMAT PENGIRIM dapat disebabkan oleh beberapa kondisi yaitu : TIDAK DIKENAL, ALAMAT KURANG JELAS, TELAH PINDAH, DITOLAK, MENINGGAL DUNIA, RUMAH KOSONG.</li>
                        <li>Pengiriman hanya dilakukan satu kali pada saat majalah terbit kealamat penerima majalah. Jika pengiriman majalah RETOUR atau KEMBALI KE ALAMAT PENGIRIM, maka untuk pengiriman pada edisi selanjutnya akan ditunda guna mengantisipasi terjadinya Retour lagi. Penundaan akan dilakukan selama belum ada konfirmasi alamat atau penerima baru yang disampaikan ke pihak Mata Air®.</li>
                        <li>Pengiriman majalah yang Retour ke alamat yang baru hanya dilakukan satu kali. Jika pengiriman dikemudian hari ke alamat yang baru masih terjadi Retour. Maka Mata Air® berhak menolak pengiriman untuk ketiga kalinya.</li>
                        <li>Pengiriman dilakukan dengan menggunakan jasa kurir PT POS INDONESIA.</li>
                        <li>Setiap pengiriman akan mendapatkan nomor resi yang dapat di cek pada situs resmi PT POS INDONESIA di www.posindonesia.com</li>
                        <li>Pelanggan Mata Air® dapat melakukan tracking pengiriman dan melihat riwayat pengiriman majalah dengan memasukkan nomer ID pelanggan pada situs www.majalahmataair.co.id </li>
                    </ol>

                    <h3 class="text-brand text-uppercase">KOMPLAIN</h3>
                    <p>
                        Penanganan Keluhan Nasabah dalam hal ini Calon Pelanggan dan/atau Pelanggan Mata Air® dapat dilakukan dengan prosedur sebagai berikut
                    </p>
                    <ol>
                        <li>Penanganan melalui telepon pada nomor telepon 021 79193715 </li>
                        <li>Penanganan keluhan melalui pelaporan pada situs resmi Mata Air® di www.majalahmataair.co.id/contact_us . form pelaporan akan di submit dan akan di review oleh admin untuk penanganan keluhan. </li>
                    </ol>

                    <h3 class="text-brand text-uppercase">PRIVASI DAN DATA PELANGGAN</h3>
                    <ol>
                        <li>Mata Air® menghargai setiap hak-hak pelanggan untuk dijaga kerahasian data-datanya.</li>
                        <li>Mata Air® tidak menjual atau membagikan informasi pribadi pelanggan Mata Air® kepada organisasi manapun untuk tujuan apapun.</li>
                        <li>Ketika terdapat bukti yang cukup kuat. Jika di kemudian hari terdapat indikasi atau telah terjadi penipuan, kami berhak untuk memberikan informasi kepada pihak berwenang untuk selanjutnya dilakukan investigasi.</li>
                    </ol>

                    <h3 class="text-brand text-uppercase">HAK INTELEKTUAL</h3>
                    <p>
                        Segala hal termasuk grafik/gambar/logo/merek dagang atau hal yang menyangkut properti yang terdapat pada situs Mata Air® tidak dapat didistribusikan, disebarluaskan atau dijual kepada pihak manapun, kecuali ada persetujuan khusus dengan pihak Mata Air®.
                    </p>

                    <h3 class="text-brand text-uppercase">DISCLAIMER</h3>
                    <p>
                        Mata Air® dapat sewaktu-waktu mengubah isi dari syarat dan ketentuan ini tanpa pemberitahuan terlebih dahulu kepada seluruh pelanggan. 
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


