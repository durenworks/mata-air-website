@extends('layouts.frontend',
            [
                'title'=>'Home',
                'active'=>'home',
                'description'=>'Selamat Datang di Majalah Mata Air',
                'categories' => $categories
            ]
        )

@section('content-css')
    <style type="text/css"> 
        
    </style>
@endsection

@section('content')

    <section class="home-slider">
        <div id="home-slider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php
                    $i = 0;
                ?>
                @foreach($articles->take(5) as $article)
                     <li data-target="#home-slider" data-slide-to="{{ $i }}" class="active"></li>
                    <?php
                        $i++
                    ?>
                @endforeach

                {{--<li data-target="#home-slider" data-slide-to="1"></li>--}}
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php
                    $i = 0;
                ?>
            @forelse($articles->take(5) as $article)

                <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
                    <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
                    <div class="carousel-caption">
                        <div class="desc col-lg-6 offset-lg-6 col-md-8 offset-md-4">
                            <p class="kategori"><a href="#">{{ $article->category->name }}</a></p>
                            <h3 class="title">{{ $article->title }}</h3>
                            <p class="author">
                                <i class="fas fa-edit"></i> {{ $article->author->name }}
                                <span class="divider">|</span>
                                <i class="fas fa-eye"></i> {{ $article->reader_count }} Views
                            </p>
                            <p class="summary">
                                {{ str_limit(strip_tags($article->content), $limit = 150, $end = '.') }}
                                <a href="#">Selengkapnya <i class="fas fa-arrow-right"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
                    $i++
                ?>
             @empty
                <div class="carousel-item active">
                    <img src="/images/slider-1.png" alt="First slide">
                    <div class="carousel-caption">
                        <div class="desc col-lg-6 offset-lg-6 col-md-8 offset-md-4">
                            <p class="kategori"><a href="#"></a></p>
                            <h3 class="title"></h3>
                            <p class="author">
                                <i class="fas fa-edit"></i>
                                <span class="divider">|</span>
                                <i class="fas fa-comment"></i>
                            </p>
                            <p class="summary">

                            </p>
                        </div>
                    </div>
                </div>
             @endforelse
            </div>
        </div>
    </section>

    <section class="blog-section">
        <div class="container">
            <div class="row">
                {{-- main-content --}}
                <div class="col-lg-8 blogs">
                    <div class="row m-0">
                        <div class="col-12">
                            <h2 class="text-uppercase blogs-title">Artikel Terbaru</h2>
                        </div>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" id="blogs-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All</a>
                        </li>
                        @foreach($categories as $category)
                            <li class="nav-item">
                                <a class="nav-link" id="category-{{$category->id}}-tab" data-toggle="tab" href="#category-{{$category->id}}" role="tab" aria-controls="category-{{$category->id}}" aria-selected="false">{{ucwords($category->name)}}</a>
                            </li>
                        @endforeach
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" id="blogs-tabs-content">
                        <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                            <div class="row m-0">
                            <?php $i=0; ?>
                            @forelse($articles->take(5) as $article)
                                @if ($i ==0)
                                    <div class="col-12">
                                        <div class="item">
                                            <div class="img-box">
                                                <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">{{ $article->category->name }}</a></p>
                                                <h3 class="title">{{ $article->title }}</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> {{ $article->author->name }}
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-eye"></i> {{ $article->reader_count }} Views
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit(strip_tags($article->content), $limit = 300, $end = '.') }}
                                                    <a href="#">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++ ?>
                                @else
                                    <div class="col-lg-6">
                                        <div class="item">
                                            <div class="img-box">
                                                <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="blog-cover">
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">{{ $article->category->name }}</a></p>
                                                <h3 class="title">{{ $article->title }}</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> {{ $article->author->name }}
                                                    <span class="divider">|</span>
                                                    Edisi 123
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-comment"></i> {{ $article->reader_count }} Views
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit(strip_tags($article->content), $limit = 300, $end = '.') }}
                                                    <a href="#">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @empty
                                <div class="col-12">
                                    Artikel tidak ditemukan
                                </div>
                            @endforelse
                            </div>
                        </div>
                        @foreach($categories as $category)
                        <div class="tab-pane fade" id="category-{{$category->id}}" role="tabpanel" aria-labelledby="category-{{$category->id}}-tab">
                            <div class="row m-0">
                            <?php $i=0; ?>
                            @forelse($articles->where('category_id', $category->id)->take(5) as $article)
                                @if ($i ==0)
                                    <div class="col-12">
                                        <div class="item">
                                            <div class="img-box">
                                                <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">{{ $article->category->name }}</a></p>
                                                <h3 class="title">{{ $article->title }}</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> {{ $article->author->name }}
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-eye"></i> {{ $article->reader_count }} Views
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit(strip_tags($article->content), $limit = 300, $end = '.') }}
                                                    <a href="#">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++ ?>
                                @else
                                    <div class="col-lg-6">
                                        <div class="item">
                                            <div class="img-box">
                                                <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="blog-cover">
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">{{ $article->category->name }}</a></p>
                                                <h3 class="title">{{ $article->title }}</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> {{ $article->author->name }}
                                                    <span class="divider">|</span>
                                                    Edisi 123
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-comment"></i> {{ $article->reader_count }} Views
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit(strip_tags($article->content), $limit = 300, $end = '.') }}
                                                    <a href="#">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @empty
                                <div class="col-12">
                                    Artikel tidak ditemukan
                                </div>
                            @endforelse
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="col-12">
                        <p class="text-center more-link">
                            <a href="#">Baca Selengkapnya</a>
                        </p>
                    </div>

                </div>
                {{-- sidebar --}}
                <div class="col-lg-4">
                    @include('includes._sidebar',[
                        'page'=>'home',
                    ]) 
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <hr class="row mt-4 mb-4">
    </div>
    <section class="editor-picks-section">
        <div class="container">
            <div class="row">
                <div class="col-12 section-title mb-2">
                    Editor Picks
                </div>
                @foreach ($editorPicks as $key => $value)
                    <div class="col-4 mb-3">
                        <div class="item">
                            <div class="img-box mb-2">
                                <img src="{{route('show.image', ['articles', $value->thumbnail])}}" alt="blog-cover">
                            </div>
                            <div class="desc">
                                <h3 class="title mb-3">{{ $value->title }}</h3>
                                <p class="author">
                                    {{ $value->published_at->format('d M Y') }}
                                    <span class="divider mr-2 ml-2">|</span>
                                    <i class="fas fa-edit"></i> {{ $value->author->name }}
                                </p>
                            </div>
                        </div>
                    </div>    
                @endforeach
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js')
    <?php
        $show_login = false;
    ?>
    @if (session('show_login'))
        <?php
        $show_login = true;
        ?>
    @endif
    @if ($show_login)
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#login-modal').modal('show');
            });
        </script>
    @endif
@endsection


