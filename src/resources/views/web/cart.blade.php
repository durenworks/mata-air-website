@extends('layouts.frontend',
[
'title'=>'Karir',
'active'=>'karir',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')

<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
          <li class="breadcrumb-item active">Cart</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="main-content" id="cartContent">
  <div class="container">
    <div class="row">
      <div class="title mt-4 mb-3">MY CART</div>
      <table class="table table-borderless">
        <thead class="thead-light">
          <th width="50px"><input type="checkbox" id="checkAll" class="form-control"></th>
          <th width="400px">Majalah</th>
          <th>Harga</th>
          <th>Qty</th>
          <th>Subtotal</th>
        </thead>
        <tbody>
          @php
          $total = 0;
          @endphp
          @forelse ($carts as $key => $cart)
          @php
          $subtotal = $cart->price * $cart->qty;
          $total += $subtotal;
          @endphp
          <tr>
            <td>
              <input type="checkbox" class="form-control">
            </td>
            <td>
              <div class="d-flex">
                <div class="col-6">
                  <img src="{{route('show.image', ['magazines_cover', $cart->cover])}}" alt="cover majalah mata air"
                    class="img-cart" />
                </div>
                <div class="col-6">
                  <b>
                    Edisi {{ $cart->roman_edition}} tahun {{ $cart->year }}
                  </b>
                </div>
              </div>
            </td>
            <td>
              <b>
                Rp {{ number_format($cart->price , 0, ',', '.') }}
              </b>
            </td>
            <td>
              <b>
                {{ $cart->qty }}
              </b>
            </td>
            <td>
              <b>
                Rp {{ number_format($subtotal , 0, ',', '.') }}
              </b>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="5" class="text-center">
              No data found
            </td>
          </tr>
          @endforelse
        </tbody>
      </table>

      <div class="d-flex justify-content-end w-100 act-button-wrapper mt-3 mb-3 border-bottom-1 border-top-1">
        <button type="button" class="btn btn-outline-primary mr-2">Delete Marked Items</button>
        <a href="{{ route('f.cart_remove_all')}}" class="btn btn-outline-primary">Empty Carts</a>
      </div>

      <div class="d-flex w-100 justify-content-end mb-5 border-bottom-1 pb-3">
        <div class="col-5">
          <div class="d-flex subtotal mb-3">
            <div class="w-50">
              Subtotal
            </div>
            <div class="w-50">
              Rp {{ number_format($total , 0, ',', '.') }}
            </div>
          </div>
          <div>
            Harap Periksa Bahwa anda memiliki judul dan jumlah yang benar sebelum melakukan checkout
          </div>
          <div class="d-flex w-100 mt-3">
            <div class="w-50 pr-2">
              <button type="button" class="btn btn-outline-primary w-100">Tambah Pembelian</button>
            </div>
            <div class="w-50">
              @if($auth)
              <a href="{{ route('f.checkout') }}" class="btn btn-primary w-100">Lanjutkan Checkout</a>
              @else
              <button type="button" class="btn btn-outline-primary w-100" data-toggle="modal"
                data-target="#checkout-email-modal">Lanjutkan Checkout</button>
              @endif
            </div>
          </div>
        </div>
      </div>

      @include('includes._payment_method')

    </div>
  </div>
</section>

@endsection

@section('modal')
@include('modal._checkout_email_modal')
@endsection

@section('content-js')
<script type="text/javascript">
</script>
@endsection