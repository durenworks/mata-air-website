@extends('layouts.frontend',
[
'title'=>'Karir',
'active'=>'karir',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">

</style>
@endsection

@section('content')

<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
          <li class="breadcrumb-item active">Cart</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="main-content" id="cartContent">
  <div class="container">
    <div class="row">
      <div class="title mt-4 w-100">PAYMENT</div>
      <div class="subtitle mb-3">
        [{{ $transaction->status}}] {{$transaction->invoice_number}}
      </div>
      <table class="table table-borderless">
        <thead class="thead-light">
          <th width="400px">Majalah</th>
          <th>Harga</th>
          <th>Qty</th>
          <th>Subtotal</th>
        </thead>
        <tbody>
          @php
          $total = 0;
          @endphp
          @foreach ($magazines as $key => $magazine)
          @php
          $price = $magazine->pivot->price;
          $subtotal = $magazine->pivot->price * $magazine->pivot->qty;
          $total += $subtotal;
          @endphp
          <tr>
            <td>
              <b>
                Edisi {{ $magazine->roman_edition}} tahun {{ $magazine->year }}
              </b>
            </td>
            <td>
              <b>
                Rp {{ number_format($price , 0, ',', '.') }}
              </b>
            </td>
            <td>
              <b>
                {{ $magazine->pivot->qty }}
              </b>
            </td>
            <td>
              <b>
                Rp {{ number_format($subtotal , 0, ',', '.') }}
              </b>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      <div class="border-top-1 border-bottom-1 w-100 mt-3 mb-3">
        <div class="subtitle mb-3 mt-3">Alamat Pengiriman</div>
      </div>
      @if(count($addresses))
      @include('includes._address_table')
      @endif

      <div class="d-flex w-100 justify-content-end mb-5 subtotal-wrapper pb-3 mt-5">
        <div class="col-5">
          <div class="d-flex subtotal mb-3">
            <div class="w-50">
              Subtotal Belanja
            </div>
            <div class="w-50">
              Rp {{ number_format($total , 0, ',', '.') }}
            </div>
          </div>
          <div class="d-flex subtotal mb-3 border-top-1 border-bottom-1 pt-3 pb-3">
            <div class="w-50">
              Total Tagihan
            </div>
            <div class="w-50">
              Rp {{ number_format($total , 0, ',', '.') }}
            </div>
          </div>
          <div class="d-flex w-100 mt-3">
            <button type="button" class="btn btn-primary w-100" id="payButton">Payment</button>
          </div>
        </div>
      </div>

      @include('includes._payment_method')

    </div>
  </div>
</section>

@endsection

@section('modal')
@include('modal._checkout_address_modal')
@endsection

@section('content-js')
<script
  src="{{ !env('VERITRANS_PRODUCTION', false) ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"
  data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">
  $('#payButton').on('click', function () {
    const snap_token = '{{ $transaction->snap_token }}'
    console.log(snap_token)
    snap.pay(snap_token, {
      // Optional
      onSuccess: function (result) {
        location.reload();
      },
      // Optional
      onPending: function (result) {
        location.reload();
      },
      // Optional
      onError: function (result) {
        location.reload();
      }
    });
  })
</script>
@endsection