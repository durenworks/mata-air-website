@extends('layouts.frontend',
[
'title'=>'Karir',
'active'=>'karir',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">

</style>
@endsection

@section('content')

<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
          <li class="breadcrumb-item active">Cart</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="main-content" id="cartContent">
  <div class="container">
    <div class="row">
      <div class="title mt-4 w-100">MY CART</div>
      <div class="subtitle mb-3">Ringkasan Pembelian Majalah Mata Air</div>
      <table class="table table-borderless">
        <thead class="thead-light">
          <th width="400px">Majalah</th>
          <th>Harga</th>
          <th>Qty</th>
          <th>Subtotal</th>
        </thead>
        <tbody>
          @php
          $total = 0;
          @endphp
          @foreach ($carts as $key => $cart)
          @php
          $price = $cart->price;
          $subtotal = $price * $cart->qty;
          $total += $subtotal;
          @endphp
          <tr>
            <td>
              <b>
                Edisi {{ $cart->roman_edition}} tahun {{ $cart->year }}
              </b>
            </td>
            <td>
              <b>
                Rp {{ number_format($price , 0, ',', '.') }}
              </b>
            </td>
            <td>
              <b>
                {{ $cart->qty }}
              </b>
            </td>
            <td>
              <b>
                Rp {{ number_format($subtotal , 0, ',', '.') }}
              </b>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      <div class="border-top-1 border-bottom-1 w-100 mt-3 mb-3">
        <div class="subtitle mb-3 mt-3">Alamat Pengiriman</div>
        <div class="hidden" id="singleAddress">
          <div class="subtitle mb-3">Alamat 1</div>
          <div class="d-flex customer-info">
            <div class="col-2">Nama Pelanggan</div>
            <div class="col-10" id="textName"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Alamat Pelanggan</div>
            <div class="col-10" id="textStreet"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Nama Perumahan</div>
            <div class="col-10" id="textResidence"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">RT</div>
            <div class="col-10" id="textRt"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">RW</div>
            <div class="col-10" id="textRw"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Desa / Dusun</div>
            <div class="col-10" id="textVillage"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Kelurahan</div>
            <div class="col-10" id="textUrbanVillage"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Kecamatan</div>
            <div class="col-10" id="textSubdistrict"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Kabupaten</div>
            <div class="col-10" id="textCity"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Provinsi</div>
            <div class="col-10" id="textProvince"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Kode Pos</div>
            <div class="col-10" id="textPostalCode"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Nomor Handphone</div>
            <div class="col-10" id="textPhone"></div>
          </div>
          <div class="d-flex customer-info">
            <div class="col-2">Email</div>
            <div class="col-10" id="textEmail"></div>
          </div>
        </div>
      </div>

      <div class="w-100 mb-3 border-bottom-1 pb-3 {{ count($addresses) ? 'hidden' : ''}}">

        <a href="{{ route('f.checkout.add_address')}}" class="btn btn-outline-primary mr-2">Kirim ke Beberapa
          Alamat</a>
        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
          data-target="#checkout-address-modal">Masukkan Alamat</button>
      </div>
      @if(count($addresses))
      @include('includes._address_table')
      @endif

      <div class="w-100 {{ count($addresses) > 0 ? '' : 'hidden'}}" id="removeAddressWrapper">
        <div class="w-100 d-flex justify-content-end border-bottom-1 pb-3" id="">
          <a href="{{ route('f.checkout.remove_address')}}" class="btn btn-remove-address">
            <i class="fas fa-trash mr-2"></i> Hapus Alamat Pengiriman
          </a>
        </div>
      </div>

      <div class="d-flex w-100 justify-content-end mb-5 subtotal-wrapper pb-3 mt-5">
        <div class="col-5">
          <div class="d-flex subtotal mb-3">
            <div class="w-50">
              Subtotal Belanja
            </div>
            <div class="w-50">
              Rp {{ number_format($total , 0, ',', '.') }}
            </div>
          </div>
          <div class="d-flex subtotal mb-3 border-top-1 border-bottom-1 pt-3 pb-3">
            <div class="w-50">
              Total Tagihan
            </div>
            <div class="w-50">
              Rp {{ number_format($total , 0, ',', '.') }}
            </div>
          </div>
          <form method="post" action="{{ route('f.do_checkout') }}">
            <div class="d-flex w-100 mt-3">
              {{ csrf_field() }}
              <input type="hidden" name="addresses" value="{{{ json_encode($addresses)}}}" id="addresses" />
              <div class="w-50 pr-2">
                <button type="button" class="btn btn-outline-primary w-100">Edit Pembelian Anda</button>
              </div>
              <div class="w-50">
                <button type="submit" class="btn btn-primary w-100">Lanjutkan Pembayaran</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      @include('includes._payment_method')

    </div>
  </div>
</section>

@endsection

@section('modal')
@include('modal._checkout_address_modal')
@endsection

@section('content-js')
<script type="text/javascript">
  $('#addAddressButton').on('click', function () {
    const formData = [{
      name: $('#formName').val() || '',
      street: $('#formStreet').val() || '',
      residence: $('#formResidence').val() || '',
      rt: $('#formRt').val() || '',
      rw: $('#formRw').val() || '',
      village: $('#formVillage').val() || '',
      urban_village: $('#formUrbanVillage').val() || '',
      subdistrict: $('#formSubdistrict').val() || '',
      city: $('#formCity').val() || '',
      province: $('#formProvince').val() || '',
      postal_code: $('#formPostalCode').val() || '',
      handphone: $('#formPhone').val() || '',
      email: $('#formEmail').val() || '',
    }]

    $('#textName').html(formData[0].name)
    $('#textStreet').html(formData[0].street)
    $('#textResidence').html(formData[0].residence)
    $('#textRt').html(formData[0].rt)
    $('#textRw').html(formData[0].rw)
    $('#textVillage').html(formData[0].village)
    $('#textUrbanVillage').html(formData[0].urban_village)
    $('#textSubdistrict').html(formData[0].subdistrict)
    $('#textCity').html(formData[0].city)
    $('#textProvince').html(formData[0].province)
    $('#textPostalCode').html(formData[0].postal_code)
    $('#textPhone').html(formData[0].phone)
    $('#textEmail').html(formData[0].email)

    $('#addresses').val(JSON.stringify(formData))
    $('#singleAddress').removeClass('hidden')
    $('#checkout-address-modal').modal('hide')
  })
</script>
@endsection