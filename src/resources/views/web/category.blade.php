@extends('layouts.frontend',
            [
                'title'=>'Kategori Page',
                'active'=>'kategori',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-eyeglasses">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Kategori {{ ucwords($category->name) }}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        What's up in
                    </p>
                    <h1 class="title">{{ ucwords($category->name) }}</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="gal-blogs br pr-md-4">
                        @forelse($articles as $article)
                            <div class="row item">
                                <div class="col-lg-5 col-md-6">
                                    <div class="img-box">
                                        <a href="{{route('f.article','slug')}}">
                                            <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-6">
                                    <div class="desc">
                                        <p class="kategori"><a href="{{route('f.category',$article->category->slug)}}">{{ ucwords($article->category->name) }}</a></p>
                                        <h3 class="title">{{ ucwords($article->title) }}</h3>
                                        <p class="author">
                                            <a href="#"><i class="fas fa-edit"></i> {{ ucwords($article->author->name) }}</a>
                                            <span class="divider">|</span>
                                            <a href="#"><i class="fas fa-eye"></i> {{ $article->reader_count }} Views</a>
                                        </p>
                                        <p class="summary">
                                            {{ str_limit('Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ad, exercitationem beatae fugiat hic cumque nemo soluta quaerat dolor aliquam doloremque perspiciatis sit quis sequi quisquam fugit perferendis veritatis reiciendis!', $limit = 300, $end = '.') }}
                                            <a href="{{route('f.article',$article->slug)}}">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>    
                            </div>
                        @empty
                            <div class="row item">
                                Tidak ada artikel
                            </div>
                        @endforelse
                    </div>
                    <div class="row mt-5 mb-5">
                        <div class="col-12 text-center more-link">
                            <a href="#" class="">Baca Selengkapanya <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


