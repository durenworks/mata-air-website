@extends('layouts.frontend',
            [
                'title'=>'Tetesan Pembaca Page',
                'active'=>'pembaca',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header p-rel z-1">
        <div class="image-bg">
            <img src="/images/blog-1.png" alt="blog-cover">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Kiriman Tetesan dari Pembaca</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <div class="detail-page-header"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="title-heading-page">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    
                    
                    <div class="bg-box">
                        <div class="bg-line"></div>
                    </div>
                    <div class="bg-white">
                    </div>
                    <p class="category"><a href="#">Budaya</a></p>
                    <h2 class="title">Tabir Illahi pada Penciptaan Gen</h2>
                    <p class="author">
                        <a href="#"><i class="fas fa-edit"></i> M. fethullah Gulen</a>
                        <span class="divider">|</span>
                        <i class="fa fa-calendar" aria-hidden="true"></i> 01/12/2018
                        <span class="divider">|</span>
                        <a href="#"><i class="fas fa-comment"></i> 10 Reading</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8 br pr-md-4">
                    <div class="row">
                        <div class="col-12">
                            <p>
                                Dengan keteraturan yang begitu sempurna, di dalam tubuh makhluk hidup terdapat sintesa molekul organik seperti  protein, enzim dan hormon yang berhubungan dengan jutaan proses metabolisme dan berperan penting dalam pengendalian aktivitas reproduksi. Secara singkat dapat dikatakan bahwa, dengan ilmu dan kekuasaan-Nya yang tak terbatas kode dari semua makhluk hidup telah tertulis pada sel-sel yang dikenal sebagai molekul DNA (Deoksiribonucleid acid) dan RNA. Sandi atau kode yang merupakan manifestasi dari Ilmu Yang Maha Tinggi ini hanya sebuah tabir yang menyembunyikan Sang Maha Kuasa di balik Penciptaan tersebut.
                            </p>
                            <p>
                                DNA manusia terbentuk dari sekitar 3 milyar pasang basa nitrogen  (adenina/guanina dan sitosina/timina), dan di dalam inti selnya terdapat 23 paket kromosom yang tersimpan terpisah. Informasi yang dienkripsi dalam program ini bisa disamakan seperti 23 jilid ensiklopedi. Setiap jilid mengandung berbagai jenis informasi yang dibutuhkan agar sel-sel dapat bekerja. Sejauh yang dapat kita ketahui, sebagian informasi yang dikodekan di DNA adalah untuk mensintesis hampir sekitar 100 jenis protein berbeda yang dihasilkan tubuh. Berasal dari asam amino manakah protein-protein ini akan dibuat dan bagaimana urutan dari asam-asam amino tersebut telah dikodekan di dalam informasi genetik. Jika satu saja dari asam-asam amino ini salah urutannya dan tidak dapat dikoreksi melalui mekanisme perbaikannya maka protein ini akan bermasalah. Jika kesalahan ini terdapat pada area yang kritis maka fungsi bagian tersebut tidak akan berjalan dan hal ini akan menyebabkan berbagai macam penyakit.
                            </p>
                            <p>
                                DNA adalah sebuah rantai yang panjang, tetapi harus termuat di dalam volume (inti)  yang kecil. Jika tiga milyar pasang basa terdapat dalam satu rantai saja maka panjangnya akan berkisar satu meter. Bagaimana mungkin rantai atau benang dengan panjang satu meter dapat dimuatkan ke dalam sebuah tempat yang volumenya seperseribunya ujung jarum ? Perlu diketahui bahwa di dalam tubuh manusia rantai DNA terbagi menjadi 23 bagian dan di dalam setiap kromosomnya rantai DNA yang panjangnya 4,5 cm terbungkus dengan sebuah metode yang paling ideal. Setelah memahami konsep DNA ini maka program kompresi yang saat ini banyak di gunakan pada komputer ternyata tidak ada apa-apanya. Ketika kompresi dilakukan tidak satupun rantai boleh terpotong dari tempatnya, tidak boleh tercampur satu sama lain dan huruf-huruf yang menjadi kode informasi 
                            </p>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="sharethis-inline-share-buttons"></div>
                        </div>
                    </div>
                    <div class="row pagination-def">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1"><i class="fa fa-chevron-left mr-3" aria-hidden="true"></i> Previous Article</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next Article <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-12 profile-sidebar">
                            <div class="img-round">
                                <img src="/images/blog-1.png" alt="profile-cover">
                            </div>
                            <h4 class="author-name">Maulana Susilo</h4>
                            <p class="author-title">Contributing Writter</p>
                            <p class="page-date">1 Desember 2018</p>
                        </div>
                    </div>
                    <div class="row mt-3 mb-3">
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c5123f1249e440017a47332&product=inline-share-buttons' async='async'></script>

@endsection


