@extends('layouts.frontend',
            [
                'title'=>'Akun Saya',
                'active'=>'akun',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Akun</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center mb-3">
                    <div class="row centering">
                        <div class="col-auto col-xs-12">
                            {{--<div class="img-round">--}}
                                {{--<img src="{{ ucwords(Auth::guard('web')->user()->avatar) }}" alt="profile-cover">--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-auto col-xs-12 text-left">
                            <h4 class="author-name font-weight-bold">Hi...<br>{{ ucwords(Auth::guard('web')->user()->name) }}</h4>
                            {{--<p class="author-title">UI/UX Designer Wannabe</p>--}}
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <ul class="nav nav-tabs nav-justified mt-0" id="account-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="koleksi-tab-link" data-toggle="tab" href="#koleksi-tab" role="tab" aria-controls="koleksi-tab" aria-selected="true">Koleksi Majalah</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="status-tab-link" data-toggle="tab" href="#status-tab" role="tab" aria-controls="status-tab" aria-selected="false">Status Berlangganan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="transaksi-tab-link" data-toggle="tab" href="#transaksi-tab" role="tab" aria-controls="transaksi-tab" aria-selected="false">Transaksi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profil-tab-link" data-toggle="tab" href="#profil-tab" role="tab" aria-controls="profil-tab" aria-selected="false">Profil Saya</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container mt-5 mb-5">
            <div class="tab-content" id="subscribe-tab-content">
                <div class="tab-pane fade show active" id="koleksi-tab" role="tabpanel" aria-labelledby="koleksi-tab-link">
                    <form id="form-archive-filter">
                        <div class="row">
                            <div class="col-lg-auto col-md-12">
                                <div class="form-group">
                                    <label class="font-weight-bold">Cari Arsip Majalah Mata Air : </label>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" name="tahun-terbit" id="tahun-terbit">
                                        <option>Pilih Tahun Terbit</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" name="edisi-terbit" id="edisi-terbit">
                                        <option>Pilih Edisi Terbit</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row gal-magazine-profile">
                        @if ($user->subscription_status)
                            @foreach($magazines as $magazine)
                                <div class="col-lg-3 col-md-4 col-sm-6">
                                    <div class="item">
                                        <a href="#">
                                            <div class="img-box">
                                                <img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}" alt="cover majalah mata air">
                                            </div>
                                            <div class="desc text-center">
                                                <h3 class="edition font-weight-bold">Edisi {{ $magazine->roman_edition }} {{ $magazine->year }}</h3>
                                            </div>
                                            <div class="item-overlay">
                                                <p class="info">
                                                    Unduh dan Baca
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-12 text-center">
                                <p class="mb-0">Anda Belum Berlangganan Atau Masa Berlangganan Anda Telah Habis</p>
                                <p class="mb-0">Ingin Berlangganan?</p>
                                <a href="{{route('f.subscribe')}}" class="second-link"><strong>(klik disini)</strong></a>
                            </div>
                        @endif
                    </div>
                    
                    <div class="row pagination-def mt-5">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1"><i class="fa fa-chevron-left mr-3" aria-hidden="true"></i> Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="status-tab" role="tabpanel" aria-labelledby="status-tab-link">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-uppercase text-brand text-center">
                                Status Berlangganan Majalah Digital Mata Air
                            </h2>
                        </div>
                    </div>
                    <div class="row subs-status">
                        <div class="col-lg-5 col-md-6">
                            <div class="img-box">
                                <img src="/images/sub-img.png" alt="majalah mata air">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    Status
                                </div>
                                <div class="col-lg-9 col-sm-6">
                                    : {{ $user->subscription_status_text }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    Expired Date
                                </div>
                                <div class="col-lg-9 col-sm-6">
                                    : {{ $user->expired_date }}
                                </div>
                            </div>
                            <div class="row action">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-9 text-right">
                                    {{--<a href="#" class="btn btn-secondary">Berhenti</a>--}}
                                    <a href="{{route('f.subscribe')}}" class="btn btn-main">Perpanjang</a>
                                </div>
                                @php
                                    $disabled = false;    
                                @endphp
                                @if($disabled)
                                    <div class="disabled-overlay"></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <p class="mb-0">Ingin Berlangganan?</p>
                            <a href="{{route('f.subscribe')}}" class="second-link"><strong>(klik disini)</strong></a>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="transaksi-tab" role="tabpanel" aria-labelledby="transaksi-tab-link">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <h2 class="text-uppercase text-brand text-center">
                                Daftar Transaksi
                            </h2>
                        </div>
                    </div>

                    @foreach($transactions as $transaction)
                        <div class="row list-transaksi">
                            <div class="col-lg-4 col-md-6">
                                <p class="label">No. Tagihan</p>
                                <p class="info lg">{{ $transaction->invoice_number }}</p>
                                <p class="date">{{ indonesianDate($transaction->created_at) }}</p>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p class="label">Total Tagihan</p>
                                <p class="info">Rp. {{ number_format($transaction->total , 0, ',', '.') }} </p>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p class="label">Status Tagihan</p>
                                <p class="info">{{ $transaction->status_text }}</p>
                            </div>
                            <div class="col-lg-2 col-md-6">
                                <a href="#" class="btn btn-block btn-secondary" data-toggle="modal" data-target="#modal-trx-{{ $transaction->id }}">Detail</a>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="tab-pane fade" id="profil-tab" role="tabpanel" aria-labelledby="profil-tab-link">
                    <form class="fe-form" method="post" action="{{ route('f.profile.post') }}">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama Pelanggan *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" value="{{ ucwords($user->name) }}" id="name" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-3 col-form-label">Alamat Pengiriman *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ ucwords($user->street) }}"name="street" id="alamat" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-3 col-form-label">Nama Perumahan *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ ucwords($user->residence) }}" name="residence" id="alamat" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rt" class="col-sm-3 col-form-label">RT</label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" name="rt" value="{{ ucwords($user->rt) }}" id="rt" placeholder="">
                            </div>
                            <label for="rw" class="col-sm-3 col-form-label">RW</label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" name="rw" value="{{ ucwords($user->rw) }}" id="rw" placeholder="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="desa" class="col-sm-3 col-form-label">Desa / Dusun</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="village" value="{{ ucwords($user->village) }}" id="desa" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="keluarahan" class="col-sm-3 col-form-label">Kelurahan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="urban_village" value="{{ ucwords($user->urban_village) }}" id="keluarahan" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kecamatan" class="col-sm-3 col-form-label">Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="subdistrict" value="{{ ucwords($user->subdistrict) }}" id="kecamatan" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kabupaten" class="col-sm-3 col-form-label">Kabupaten</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="city" value="{{ ucwords($user->city) }}" id="kabupaten" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="provinsi" class="col-sm-3 col-form-label">Provinsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="province" value="{{ ucwords($user->province) }}" id="provinsi" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kodepos" class="col-sm-3 col-form-label">Kode Pos *</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="postal_code" value="{{ ucwords($user->postal_code) }}"  id="kodepos" placeholder="">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="hp" class="col-sm-3 col-form-label">Nomor Handphone *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="phone" value="{{ ucwords($user->phone) }}" id="hp" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email *</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" id="email" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="occupation" value="{{ ucwords($user->occupation) }}" id="pekerjaan" placeholder="">
                            </div>
                        </div>

                        {!! Form::token() !!}

                        <div class="row action-tabs mt-5">
                            <div class="col-sm-6 text-left">
                                
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                            </div>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
    
    @foreach($transactions as $transaction)
        <!-- Modal -->
        <div class="modal fade modal-trx" id="modal-trx-{{$transaction->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-trx-{{$transaction->id}}-label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="text-uppercase text-brand text-center">
                                    Detail Transaksi
                                </h3>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="label">No.Tagihan</p>
                                <h3 class="no-trx b">{{ $transaction->invoice_number }}</h3>
                            </div>
                            <div class="col-lg-6">
                                <p class="label">Tanggal Tagihan</p>
                                <p class="info b">{{ indonesianDate($transaction->created_at, false) }}</p>
                            </div>
                            <div class="col-12">
                                <p class="label">Keterangan Tagihan</p>
                                <p class="info">
                                    - langganan Majalah Mata Air, Paket Langganan 1 Tahun
                                </p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-12">
                                <p class="label">Total Tagihan</p>
                                <p class="info b">Rp. {{ number_format($transaction->total , 0, ',', '.') }}</p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="label">Status Tagihan</p>
                                <p class="info b">{{ $transaction->status_text }}</p>
                            </div>
                            @if ($transaction->status == 'PAI')
                                <div class="col-lg-6">
                                    <p class="label">Tanggal Pembayaran</p>
                                    <p class="info b">{{ indonesianDate($transaction->paid_at, false) }}</p>
                                </div>
                            @endif
                        </div>
                        <hr/>
                        @if ($transaction->status == 'INV')
                            <div class="row">
                                <div class="col-12">
                                    <p class="label">Keterangan :</p>
                                    <p class="info">
                                        Harap selesaikan pembayaran tagihan untuk membaca majalah Digital Mata Air. <br/>
                                        Tagihan akan dibatalkan secara otomatis jika tidak melakukan pembayaran.
                                    </p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


