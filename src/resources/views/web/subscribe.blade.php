@extends('layouts.frontend',
            [
                'title'=>'Subscribe Page',
                'active'=>'langganan',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">    
    #form-field-edisi{
        display: none;
    }    
    </style>
@endsection

@section('content')

    <section class="page-header bg-langganan">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Berlangganan</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        find up in
                    </p>
                    <h1 class="title">Berlangganan Majalah Mata Air</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="regist br pr-md-4">
                        <div class="row">
                            <div class="col-12">
                                <h2 class="text-center text-brand">Cara Berlangganan Majalah Mata Air</h2>
                            </div>
                        </div>

                        <ul class="nav nav-tabs nav-justified" id="subscribe-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pendaftaran-tab-link" data-toggle="tab" href="#pendaftaran-tab" role="tab" aria-controls="pendaftaran-tab" aria-selected="true">Form Pendaftaran</a>
                            </li>
                        </ul>


                        <div class="tab-content" id="subscribe-tab-content">
                            <div class="tab-pane fade show active" id="pendaftaran-tab" role="tabpanel" aria-labelledby="pendaftaran-tab">
                                <form class="fe-form" method="post" onsubmit="return submitForm();">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Nama Pelanggan *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" value="{{ ucwords($user->name) }}" id="name" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alamat" class="col-sm-3 col-form-label">Alamat *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="{{ ucwords($user->street) }}"name="street" id="street" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alamat" class="col-sm-3 col-form-label">Nama Perumahan *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="{{ ucwords($user->residence) }}" name="residence" id="residence" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rt" class="col-sm-3 col-form-label">RT</label>
                                        <div class="col-sm-3">
                                            <input type="number" class="form-control" name="rt" value="{{ ucwords($user->rt) }}" id="rt" placeholder="">
                                        </div>
                                        <label for="rw" class="col-sm-3 col-form-label">RW</label>
                                        <div class="col-sm-3">
                                            <input type="number" class="form-control" name="rw" value="{{ ucwords($user->rw) }}" id="rw" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="desa" class="col-sm-3 col-form-label">Desa / Dusun</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="village" value="{{ ucwords($user->village) }}" id="village" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="keluarahan" class="col-sm-3 col-form-label">Kelurahan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="urban_village" value="{{ ucwords($user->urban_village) }}" id="urban_village" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="kecamatan" class="col-sm-3 col-form-label">Kecamatan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="subdistrict" value="{{ ucwords($user->subdistrict) }}" id="subdistrict" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="kabupaten" class="col-sm-3 col-form-label">Kota</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="city" value="{{ ucwords($user->city) }}" id="city" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="provinsi" class="col-sm-3 col-form-label">Provinsi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="province" value="{{ ucwords($user->province) }}" id="province" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="kodepos" class="col-sm-3 col-form-label">Kode Pos *</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="postal_code" value="{{ ucwords($user->postal_code) }}"  id="postal_code" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="hp" class="col-sm-3 col-form-label">Nomor Handphone *</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="phone" value="{{ ucwords($user->phone) }}" id="phone" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 col-form-label">Email *</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" value="{{ $user->email }}" id="email" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="occupation" value="{{ ucwords($user->occupation) }}" id="occupation" placeholder="">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <hr class="mb-5 mt-5"/>
                                        </div>
                                    </div>
                                    <div class="row package">
                                        <div class="col-12 desc text-center">
                                            <h3 class="title text-brand">Paket Berlangganan Majalah Mata Air</h3>
                                            <p class="sub-title">Nikmati Keuntungan Dengan Berlangganan Majalah Mata Air</p>
                                            <p><strong>Harga Berlangganan</strong><p>
                                        </div>

                                        <div class="card mx-auto text-center">
                                            <div class="card-header">
                                                <strong><span class="package-type">1 Tahun (4 Edisi)</span></strong>
                                            </div>
                                            <div class="card-body">
                                                <p class="keterangan">
                                                    Rp. <span class="price">{{ number_format(getSubscriptionPrice() , 0, ',', '.') }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row action-tabs mt-5">
                                        <div class="col-sm-6 text-left">

                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <button type="submit" class="btn btn-primary" id="submit">Checkout</button>
                                        </div>
                                    </div>
                                    {!! Form::token() !!}
                                </form>

                                {{--<div class="payment-method">--}}
                                    {{--<div class="col-12 text-center">--}}
                                        {{--<h4 class="text-brand">Metode Pemabayaran Paket Berlangganan Majalah Mata Air</h4>--}}
                                    {{--</div>--}}
                                    {{--<ul class="nav nav-pills" id="payment-method-tab" role="tablist">--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link active" id="card-tab-link" data-toggle="tab" href="#card-tab" role="tab" aria-controls="card-tab" aria-selected="true">Card Payment</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="bank-tab-link" data-toggle="tab" href="#bank-tab" role="tab" aria-controls="bank-tab" aria-selected="false">Bank Transfer</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="cardless-tab-link" data-toggle="tab" href="#cardless-tab" role="tab" aria-controls="cardless-tab" aria-selected="false">Cardless Credit</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="direct-tab-link" data-toggle="tab" href="#direct-tab" role="tab" aria-controls="direct-tab" aria-selected="false">Direct Debit</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="ewallet-tab-link" data-toggle="tab" href="#ewallet-tab" role="tab" aria-controls="ewallet-tab" aria-selected="false">e-Wallet Debit</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="counter-tab-link" data-toggle="tab" href="#counter-tab" role="tab" aria-controls="counter-tab" aria-selected="false">Over the Counter</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}

                                    {{--<div class="tab-content" id="payment-method-tab-content">--}}
                                        {{--<div class="tab-pane fade show active" id="card-tab" role="tabpanel" aria-labelledby="card-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran online dari kartu kredit/debit dari semua Bank yang berlogo VISA/MasterCard/JCB/Amex.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/card.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="tab-pane fade" id="bank-tab" role="tabpanel" aria-labelledby="bank-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran transfer melalui ATM, mobile, atau internet banking, dengan notifikasi real-time.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/bank.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="tab-pane fade" id="cardless-tab" role="tabpanel" aria-labelledby="cardless-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran cicilan tanpa kartu kredit.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/cardless.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="tab-pane fade" id="direct-tab" role="tabpanel" aria-labelledby="direct-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran internet banking dari berbagai Bank ternama.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/direct.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="tab-pane fade" id="ewallet-tab" role="tabpanel" aria-labelledby="ewallet-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran e-Wallet dari akun/nomor/PIN ponsel pelanggan.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/ewallet.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="tab-pane fade" id="counter-tab" role="tabpanel" aria-labelledby="counter-tab-link">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-12 text-center">--}}
                                                    {{--<p>--}}
                                                        {{--Terima pembayaran dari Toserba dan Kios di seluruh Indonesia.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-12">--}}
                                                    {{--<div class="img-box">--}}
                                                        {{--<img src="/images/kios.png" alt="logo">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                            <div class="tab-pane fade" id="pembayaran-tab" role="tabpanel" aria-labelledby="pembayaran-tab">
                                <div class="row">
                                    <div class="col-12 text-center mt-4">
                                        <p>loading snap midtrans</p>
                                    </div>
                                </div>
                                <div class="row action-tabs mt-5">
                                    <div class="col-sm-6 text-left">
                                        <a href="#" class="btn btn-secondary text-uppercase" id="prev-pembayaran">Sebelumnya</a>                     
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <a href="#" class="btn btn-primary text-uppercase" id="btn-konfirmasi">Konfirmasi</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="konfirmasi-tab" role="tabpanel" aria-labelledby="konfirmasi-tab">
                                <div class="row mt-5 pt-4">
                                    <div class="col-12">
                                        <p>
                                            <strong>
                                                Konfirmasi Pembayaran Paket Berlangganan<br>
                                                Majalah Mata Air
                                            </strong>
                                        </p>
                                        <h3 class="text-brand">Ringkasan Paket Berlangganan</h3>
                                    </div>
                                </div>
                                <div class="row action-tabs mt-5">
                                    <div class="col-sm-6 text-left">
                                        <a href="#" class="btn btn-secondary text-uppercase" id="prev-konfirmasi">Sebelumnya</a>                     
                                    </div>
                                    <div class="col-sm-6 text-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
        $('#btn-checkout').click(function (e) { 
            e.preventDefault();
            $('#subscribe-tab a[href="#pembayaran-tab"]').tab('show') // Select tab by name
        });
        $('#btn-konfirmasi').click(function (e) { 
            e.preventDefault();
            $('#subscribe-tab a[href="#konfirmasi-tab"]').tab('show') // Select tab by name
        });
        $('#prev-pembayaran').click(function (e) { 
            e.preventDefault();
            $('#subscribe-tab a[href="#pendaftaran-tab"]').tab('show') // Select tab by name
        });
        $('#prev-konfirmasi').click(function (e) { 
            e.preventDefault();
            $('#subscribe-tab a[href="#pembayaran-tab"]').tab('show') // Select tab by name
        });
        // $("#form-field-edisi").hide();

        $("#check-fisik").change(function (e) {
            e.preventDefault();
            $("#form-field-edisi").toggle("slow");
        });
    </script>

    <script src="{{ !env('VERITRANS_PRODUCTION', false) ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script>
        function submitForm() {
            // Kirim request ajax
            $.post("{{ route('f.subscribe.post') }}",
                {
                    _method: 'POST',
                    _token: '{{ csrf_token() }}',
                    name: $('input#name').val(),
                    street: $('input#street').val(),
                    residence: $('input#residence').val(),
                    rt: $('input#rt').val(),
                    rw: $('input#rw').val(),
                    village: $('input#village').val(),
                    urban_village: $('input#urban_village').val(),
                    city: $('input#city').val(),
                    province: $('input#province').val(),
                    postal_code: $('input#postal_code').val(),
                    phone: $('input#phone').val(),
                    occupation: $('input#occupation').val(),
                },
                function (data, status) {
                    snap.pay(data.snap_token, {
                        // Optional
                        onSuccess: function (result) {
                            location.reload();
                        },
                        // Optional
                        onPending: function (result) {
                            location.reload();
                        },
                        // Optional
                        onError: function (result) {
                            location.reload();
                        }
                    });
                });
            return false;
        }
    </script>
@endsection


