@extends('layouts.frontend',
            [
                'title'=>'Karir',
                'active'=>'karir',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-mata-air">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Karir</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        We're Hiring
                    </p>
                    <h1 class="title">Karir di Mata Air</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-3">
                <div class="col-12 text-center">
                    <h2 class="text-brand">
                        Silahkan isi form berikut :
                    </h2>
                </div>
            </div>

            <form class="fe-form mb-5" id="career-form">
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label">Nama Lengkap *</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" placeholder="">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="rt" class="col-sm-3 col-form-label">Tempat, Tanggal Lahir</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="rt" placeholder="">
                    </div>  
                    <div class="col-sm-5">
                        {{-- <input type="number" class="form-control" id="rw" placeholder=""> --}}
                        <input class="datepicker" data-date-format="dd/mm/yyyy">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alamat" class="col-sm-3 col-form-label">Alamat Lengkap</label>
                    <div class="col-sm-9">
                         <textarea class="form-control" name="alamat" id="alamat" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Alamat Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="telp" class="col-sm-3 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="telp" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hp" class="col-sm-3 col-form-label">Nomor Handphone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="hp" placeholder="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="jeniskelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="jeniskelamin" id="jeniskelamin1" value="Laki-Laki">
                            <label class="form-check-label" for="jeniskelamin1">Laki-Laki</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="jeniskelamin" id="jeniskelamin2" value="Perempuan">
                            <label class="form-check-label" for="jeniskelamin2">Perempuan</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pendidikan" class="col-sm-3 col-form-label">Pendidikan Terakhir</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="pendidikan" id="pendidikan">
                            <option>Pilih Salah Satu</option>
                            <option>SD</option>
                            <option>SMP</option>
                            <option>SMA/SMK</option>
                            <option>D3</option>
                            <option>S1</option>
                            <option>S2</option>
                            <option>S3</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="hp" class="col-sm-3 col-form-label">Upload Foto / CV</label>
                    <div class="col-sm-9">
                        <input type="file" class="form-control-file" name="upload" id="upload" placeholder="">
                    </div>
                </div>


                <div class="row action-tabs mt-5">
                    <div class="col-sm-6 text-left">
                        
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="#" class="btn btn-main text-uppercase">Kirim Lamaran</a>
                    </div>
                </div>
            
            </form>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
    $(function () {
        $(".datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
        }).datepicker('update', new Date());
    });
    </script>
@endsection


