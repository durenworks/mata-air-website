@extends('layouts.frontend',
            [
                'title'=>'FAQ',
                'active'=>'faq',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-mata-air">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        Find Up In
                    </p>
                    <h1 class="title">FAQ</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-12 text-center">
                    <h2 class="text-brand">
                        Pertanyaan Umum (Frequently Asked Question)
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5">
                    <div class="accordion fe-accordion" id="faq-list">
                        <div class="card">
                            <div class="card-header" id="heading-1">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Apakah Majalah Mata Air Itu?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-1" class="collapse show" aria-labelledby="heading-1" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Majalah Mata Air merupakan majalah sains, budaya dan spiritualitas yang isinya menyajikan topik-topik  utama seputar kehidupan manusia, seperti : sains-teknologi, psikologi, keragaman budaya, peradaban pendidikan, dll.
                                    </p>
                                    <p>
                                        Majalah Mata Air mencoba untuk mengungkap alam semesta dan segala entitas yang ada didalamnya dengan pendekatan menyeluruh yang sesuai dengan pendekatan dan bahasa zaman.
                                    </p>
                                    
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-2">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-2" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Berapa Harga Majalah Mata Air?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-2" class="collapse" aria-labelledby="heading-2" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Harga Majalah Mata Air adalah Rp. 55.000,- per edisi majalah.
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-3">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-3" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Apa yang dimaksud dengan sistem langganan?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-3" class="collapse" aria-labelledby="heading-3" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Sistem langganan adalah Anda berlangganan Majalah Mata Air periode tertentu. Saat ini tersedia 1 jenis paket langganan yaitu :
                                    </p>
                                    <p>
                                        1 tahun langganan (4 edisi) dengan biaya Rp. 180.000,-. (termasuk Disc. 20%)
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-4">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-4" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Apa keuntungan berlangganan?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-4" class="collapse" aria-labelledby="heading-4" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Dengan memilih paket langganan yang tersedia anda akan mendapatkan tawaran menarik berupa potongan 20% dari biaya langganan.
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-5">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-5" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Bagaimana Cara Berlangganan Majalah Mata Air?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-5" class="collapse" aria-labelledby="heading-5" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Anda dapat berlangganan Majalah Mata Air dengan membuka halaman beranda www.majalahmataair.co.id, kemudian klik tombol berlangganan pada pojok kanan atas layar. Selanjutnya isi data dan unggah bukti pembayaran yang telah di-scan atau foto.
                                    </p>
                                    <p>
                                        Kami akan menghubungi Anda melalui email dan tunggu konfirmasi dari kami.
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-6">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-6" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Kapan Majalah Mata Air terbit ?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-6" class="collapse" aria-labelledby="heading-6" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Majalah Mata Air terbit per 3 bulan untuk setiap edisinya. 
                                    </p>
                                    <p>
                                        Kami akan menghubungi Anda melalui email dan tunggu konfirmasi dari kami.
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading-7">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-7" aria-expanded="true" aria-controls="collapseOne">
                                        <li>Kemana saya harus menghubungi berkaitan dengan langganan dan komplain?</li>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-7" class="collapse" aria-labelledby="heading-7" data-parent="#faq-list">
                                <div class="card-body">
                                    <p>
                                        Anda bisa memberikan laporan pada form kontak kami. selanjutkan akan kami tindak lanjuti sesegera mungkin.
                                        <br/>Anda juga bisa menghubungi kami pada :
                                        <br/>Telp. 021 747 180 39
                                        <br/>Email : info@majalahmataair.com 
                                    </p>
                                    <p>
                                        Kami akan menghubungi Anda melalui email dan tunggu konfirmasi dari kami.
                                    </p>
                                    <p class="text-right">
                                        <span class="rating-faq mr-4">Apakah Informasi ini Berguna? </span>
                                        <a href="#" class="btn btn-third ml-1 mr-1">Ya</a>
                                        <a href="#" class="btn btn-secondary ml-1 mr-1">Tidak</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


