@extends('layouts.frontend',
            [
                'title'=>'Tulis Tetesan Page',
                'active'=>'pembaca',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
    <!-- Include stylesheet -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Tulis Tetesan Anda</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="desc-letter br pr-md-4">
                        <h2 class="text-brand mb-3">Form Data Diri Penulis</h2>
                        
                        <form class="fe-form" method="post" action="{{ route('f.readerletter.post') }}"  enctype="multipart/form-data">
                            <div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
                                <label for="name" class="col-sm-3 col-form-label">Nama Penulis *</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="{{ old('name') }}">
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
                                <label for="email" class="col-sm-3 col-form-label">Alamat Email *</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email" value="{{ old('email') }}">
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telp" class="col-sm-3 col-form-label">Nomor Telepon</label>
                                <div class="col-sm-9">
                                    <input type="text" name="phone_fixed" class="form-control" id="telp" placeholder="Telp." value="{{ old('phone_fixed') }}">
                                </div>
                            </div>
                            <div class="form-group row  {{ $errors->has('phone_mobile') ? 'has-error' : ''}}">
                                <label for="hp" class="col-sm-3 col-form-label">Nomor Handphone *</label>
                                <div class="col-sm-9">
                                    <input type="text" name="phone_mobile" class="form-control" id="hp" placeholder="Nomor HP" value="{{ old('phone_mobile') }}">
                                    {!! $errors->first('phone_mobile', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                            <div class="form-group row {{ $errors->has('occupation') ? 'has-error' : ''}}">
                                <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan *</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="occupation" id="pekerjaan">
                                        <option value="0">Pilih Salah Satu</option>
                                        <option value="Pegawai Swasta">Pegawai Swasta</option>
                                        <option value="Pegawai Negeri">Pegawai Negeri</option>
                                        <option value="Wirswasta">Wirswasta</option>
                                        <option value="Pelajar">Pelajar</option>
                                        <option value="Peneliti">Peneliti</option>
                                        <option value="Prakisi">Praktisi</option>
                                        <option value="Dosen">Dosen</option>
                                    </select>
                                    {!! $errors->first('occupation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}">
                                <label for="judul" class="col-sm-3 col-form-label">Judul Artikel *</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="title" id="judul" placeholder="Judul" value="{{ old('title') }}">
                                    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            
                            <div class="form-group row {{ $errors->has('category') ? 'has-error' : ''}}">
                                <label for="kategori" class="col-sm-3 col-form-label">Kategori Artikel</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="category" id="kategori">
                                        <option value="0">Pilih Salah Satu</option>
                                        @foreach(getCategories() as $category)
                                            <option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('category', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('thumbnail') ? 'has-error' : ''}}">
                                <label for="thumbnail" class="col-sm-3 col-form-label">Thumbnail *</label>
                                <div class="col-sm-9">
                                    <input type="file" name="thumbnail" class="form-control" id="thumbnail">
                                    {!! $errors->first('thumbnail', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('content') ? 'has-error' : ''}}">
                                <div class="col-12">
                                    <textarea class="form-control" name="content" id="konten" rows="8" value="{{ old('content') }}"></textarea>
                                    {!! $errors->first('content', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="agree" checked>
                                        <label class="form-check-label" for="agree">
                                            Saya sudah mengisi data ini dengan benar <a href="#">Syarat dan Kondisi atau Term of Services dari Penulisan Mata Air</a>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            {!! Form::token() !!}
                            <div class="form-group row">
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-primary" id="submit">Kirim Artikel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <!-- Include the Quill library -->
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'konten' );
    </script>
@endsection


