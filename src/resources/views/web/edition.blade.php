@extends('layouts.frontend',
[
'title'=>'Edisi Page',
'active'=>'edisi',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')

<section class="page-header bg-book">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
					<li class="breadcrumb-item active">Edisi</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12 header-title text-center">
				<p class="sub-title">
					Find up in
				</p>
				<h1 class="title">Edisi Majalah Mata Air</h1>
			</div>
		</div>
	</div>
</section>
<section class="main-content">
	<div class="container">
		<div class="row mt-5 mb-5">
			<div class="col-lg-8">
				<div class="gal-magazine br pr-md-4">
					<div class="row">
						<div class="col-12">
							<div class="item featured">
								<?php $magazine = $latestMagz; ?>
								<div class="row">
									<div class="col-auto">
										<div class="img-box">
											<img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}"
												alt="Majalah Mata Air" />
										</div>
									</div>
									<div class="col">
										<div class="desc">
											<p class="intro">Majalah Mata Air Edisi Terbaru :</p>
											<h4 class="edition">"
												{{ getMonth($magazine->published_at) . ' ' . $magazine->year }} "
											</h4>
											<p class="description">
												{{ str_limit($magazine->description, $limit = 500, $end = '.') }}
											</p>
											<p class="action">
												<a href="{{ route('f.magazine_detail', $magazine->id)}}" class="btn btn-primary">Beli
													Majalah</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@php
					$year = Request::query('year');
					$edition = Request::query('edition');
					@endphp
					<form id="formArchiveFilter" action="{{ route('f.edition') }}" method="GET">
						<div class=" row">
							<div class="col-lg-auto col-md-12">
								<div class="form-group">
									<label>Cari Arsip Majalah Mata Air : </label>
								</div>
							</div>
							<div class="col-lg col-md-6 col-sm-12">
								<div class="form-group">
									<select class="form-control" name="year" id="tahunTerbit">
										<option value="">Pilih Tahun Terbit</option>
										@foreach ($years as $item)
										<option value="{{$item->year}}" {{$year == $item->year ? 'selected' : ''}}>
											{{ $item->year }}
										</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg col-md-6 col-sm-12">
								<div class="form-group">
									<select class="form-control" name="edition" id="edisiTerbit">
										<option value="">Pilih Edisi Terbit</option>
										@foreach ($editions as $item)
										<option value="{{$item->number_edition}}" {{$edition == $item->number_edition ? 'selected' : ''}}>
											{{ $item->number_edition }}
										</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</form>
					<div class="row">
						@foreach ($magazines as $key => $magazine)
						<div class="col-lg-6 col-md-6 col-12">
							<div class="item">
								<div class="row">
									<div class="col-auto">
										<div class="img-box">
											<img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}"
												alt="cover majalah mata air" />
										</div>
									</div>
									<div class="col pl-0">
										<div class="desc">
											<h4 class="edition text-uppercase">
												Edisi {{ $magazine->roman_edition}} Tahun {{ $magazine->year }}
											</h4>
											<p class="description">
												{{ str_limit($magazine->description, $limit = 90, $end = '.') }}
											</p>
											<p class="action">
												<a href="{{ route('f.magazine_detail', $magazine->id)}}" class="btn btn-primary">Beli
													Majalah</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach

					</div>
				</div>
				<div class="row pagination-def">
					<div class="col-12">
						<nav aria-label="Page navigation example">
							{!! $magazines->appends(Request::except('page'))->links('vendor.pagination.default') !!}
							{{-- <ul class="pagination justify-content-center">
								<li class="page-item disabled">
									<a class="page-link" href="#" tabindex="-1">Previous</a>
								</li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item">
									<a class="page-link" href="#">Next</a>
								</li>
							</ul> --}}
						</nav>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				@include('includes._sidebar')
			</div>
		</div>
	</div>
</section>

@endsection

@section('modal')
@endsection

@section('content-js')
<script type="text/javascript">
	$('#tahunTerbit').on('change', function () {
		$('#formArchiveFilter').submit()
	})
	$('#edisiTerbit').on('change', function () {
		$('#formArchiveFilter').submit()
	})
</script>
@endsection