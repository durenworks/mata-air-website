@extends('layouts.frontend',
[
'title'=>'Karir',
'active'=>'karir',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')

<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
          <li class="breadcrumb-item active">Detail Majalah</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="main-content mt-4" id="magazineDetail">
  <div class="container">
    <div class="row">
      <div class="col-3">
        <div class="img-box">
          <img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}" alt="cover majalah mata air" />
        </div>
      </div>
      <div class="col-8">
        <div class="title">
          EDISI {{ $magazine->roman_edition}}
        </div>
        <div class="title mb-3">
          TAHUN {{ $magazine->year }}
        </div>
        <div class="description mb-3">
          {{ $magazine->description }}
        </div>
        <div class="row justify-content-between link">
          <div class="col">
            <a href="#" class="btn btn-link p-0">Unduh Sampel</a>
          </div>
        </div>
        <div class="row justify-content-between border-top-1 mt-3 pt-4">
          <div class="col">
            {{-- <div class="discount-price">
              Rp. 75.000
            </div> --}}
            <div class="fix-price">
              Rp. {{ number_format(getSubscriptionPrice() , 0, ',', '.') }}
            </div>
          </div>
          <div class="col-5">
            <div>
              AVAILABLE (100 copies available)
            </div>
            <div class="font-12">
              biasanya terkirim dalam 8-14 hari kerja
            </div>
            <form action="{{ route('f.cart_add', $magazine->id)}}" method="post">
              {{ csrf_field() }}
              <div class="d-flex mt-2">
                <div class="w-50 pr-4">
                  <input type="number" class="form-control" name="qty">
                </div>
                <div class="w-50">
                  <button class="btn btn-primary w-100">
                    Add to cart
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      @php
      $year = Request::query('year');
      $edition = Request::query('edition');
      @endphp
      <form id="formArchiveFilter" action="{{ route('f.magazine_detail', $magazine->id) }}" method="GET">
        <div class="row border-top-1 mt-4 pt-3">
          <div class="col-lg-auto col-md-12">
            <div class="form-group">
              <label>Cari Arsip Majalah Mata Air : </label>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group">
              <select class="form-control" name="year" id="tahunTerbit">
                <option value="">Pilih Tahun Terbit</option>
                @foreach ($years as $item)
                <option value="{{$item->year}}" {{$year == $item->year ? 'selected' : ''}}>
                  {{ $item->year }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="form-group">
              <select class="form-control" name="edition" id="edisiTerbit">
                <option value="">Pilih Edisi Terbit</option>
                @foreach ($editions as $item)
                <option value="{{$item->number_edition}}" {{$edition == $item->number_edition ? 'selected' : ''}}>
                  {{ $item->number_edition }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
      </form>

      <div class="row gal-magazine mt-4 w-100">
        @foreach ($magazines as $key => $magazine)
        <div class="col-lg-4 col-md-4 col-12">
          <div class="item">
            <div class="row">
              <div class="col-auto">
                <div class="img-box">
                  <img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}"
                    alt="cover majalah mata air" />
                </div>
              </div>
              <div class="col pl-0">
                <div class="desc">
                  <h4 class="edition text-uppercase">
                    Edisi {{ $magazine->roman_edition}} Tahun {{ $magazine->year }}
                  </h4>
                  <p class="description">
                    {{ str_limit($magazine->description, $limit = 90, $end = '.') }}
                  </p>
                  <p class="action">
                    <a href="{{ route('f.magazine_detail', $magazine->id)}}" class="btn btn-primary">Beli Majalah</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach

      </div>

      <div class="row pagination-def w-100 mb-4 border-bottom-1">
        <div class="col-12">
          <nav aria-label="Page navigation example">
            {!! $magazines->appends(Request::except('page'))->links('vendor.pagination.default') !!}
          </nav>
        </div>
      </div>

      @include('includes._payment_method')

    </div>
  </div>
</section>

@endsection

@section('modal')
@endsection

@section('content-js')
<script type="text/javascript">
  $('#tahunTerbit').on('change', function () {
    $('#formArchiveFilter').submit()
  })
  $('#edisiTerbit').on('change', function () {
    $('#formArchiveFilter').submit()
  })
</script>
@endsection