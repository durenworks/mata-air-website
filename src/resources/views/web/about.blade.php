@extends('layouts.frontend',
            [
                'title'=>'Tentang Kami',
                'active'=>'tentang',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-mata-air">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        Who We Are
                    </p>
                    <h1 class="title">Tentang Mata Air</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-12 text-center">
                    <h2 class="text-brand">
                        " Membaca Mata Air Membaca Kehidupan"
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <ul class="nav nav-tabs nav-justified mt-0" id="about-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="profil-tab-link" data-toggle="tab" href="#profil-tab" role="tab" aria-controls="profil-tab" aria-selected="true">Profil Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="redaksi-tab-link" data-toggle="tab" href="#redaksi-tab" role="tab" aria-controls="redaksi-tab" aria-selected="false">Redaksi dan Manajamen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="dewan-tab-link" data-toggle="tab" href="#dewan-tab" role="tab" aria-controls="dewan-tab" aria-selected="false">Dewan Penasihat</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-content mb-5" id="about-tab-content">
                <div class="tab-pane fade show active" id="profil-tab" role="tabpanel" aria-labelledby="profil-tab-link">
                    <div class="row">
                        <div class="col-12 mt-4 mb-4">
                            <i class="fa fa-quote-left fa-3x pull-left mr-2"></i>
	
                            <p class="first-p">Mengapa kata mata air yang diusung sebagai nama dari majalah ini, memiliki sejarah panjang, namun pemikiran yang ingin diutarakan pada pembacanya adalah karena dalam kehidupan kita mata air adalah salah satu sumber utama kehidupan yang bersumber dari Rahmat Ilahi, ia akan mengalirkan nafas kejernihan pada banyak elemen hidup,  maka berangkat dari filosofi ini Majalah MATA AIR diharapkan dapat mengetengahkan esensi kebenaran dan mengalirkan keindahannya serta kesejukannya ke dalam  berbagai  cabang ilmu. Selayaknya kejernihan dan kesegaran setiap tetes air yang dipancarkannya maka penikmat artikel-artikel di majalah ini akan mendapatkan ilmu yang mampu menyejukkan hati dan pikiran pembacanya.  Menjadi sumber dan mata air yang jernih dalam sains, sejarah, budaya, spiritualitas, peradaban dan sosial. </p>

                            <p>Ketika menyuguhkan sebuah bahan bacaan yang mengusung nilai-nilai spiritualitas , ilmiah, budaya dan peradaban maka artikel-artikel Mata Air akan tetap dapat memberikan pencerahan bagi semua kalangan tanpa dibatasi oleh sekat-sekat ras, suku dan golongan tertentu. </p>

                            <p>Cakrawala pemikiran yang diangkat pada setiap artikel haruslah sampai kepada pemahaman bahwa segala sesuatu yang ada di alam semesta ini, dan apapun yang ada di luar indera kita sekalipun adalah manifestasi dari kebesaran Sang Pencipta dan seterusnya bahwa semua penemuan ilmiah, bahasa, budaya, dan peradaban adalah hanya sebuah gerbang untuk mengungkap dimensi lebih luas dari kebenaran tersebut yang akan membuat kita mampu menyadari esensi dan makna segala penciptaan. </p>
                            <p>Keunikan dari Mata Air adalah majalah ini memiliki kontributor dari seluruh penjuru dunia terutama dari Indonesia, Turki, Amerika, Arab dan Eropa. Para ahli pada masing-masing bidangnya menorehkan tinta emas keilmuan dengan kesantunan dan ketawaduan sebagai seorang hamba. Pengalaman kurun waktu 35 tahun pada majalah-majalah yang sebelumnya yang telah diterbitkan di berbagai bahasa menjadikan Mata Air menjadi majalah ke delapan yang pada akhirnya diterbitkan di Indonesia. Oleh karenanya anda akan menemukan banyak kesamaan visi Mata Air dengan majalah SIZINTI yang diterbitkan dalam bahasa Turki, Hira (Arab), Fountain (Inggris), dieFontane (Jerman), Ebru (Prancis), ΓPAHИ (Rusia), RevistaCASCADA (Spanyol).</p>
                            <p>Kehadiran majalah ini diharapkan akan mengisi kekosongan yang dirasa oleh publik Indonesia pada sebuah bahan bacaan yang didedikasikan bagi nilai-nilai akhlak dan kebenaran. Semoga semua artikel kami akan membawa kesejukan baru bagi Bumi Pertiwi ini. </p>
                            <p>Pergeseran pemikiran yang dialami umat manusia pada beberapa abad terakhir ini membawa kita juga pada sebuah keterasingan. Falsafah materialis yang bergerak dengan kebutaannya sehingga merasa cukup hanya dengan melihat pada penampakan saja , falsafah materialis ini pertama-tama  memecah pengetahuan,  kemudian meniadakan keharmonisan, keserasian dan hubungan di antara bagian-bagian pecahan tersebut. Jalan-jalan yang membawa umat manusia di era ini kepada hakikat penciptaan telah tersumbat satu per satu.</p>
                            <p>Dengan keadaan ini sekian lama masyarakat dilanda kehampaan pemikiran. Sementara teori-teori dan arus pemikiran palsu yang diketengahkan untuk mengisi kekosongan yang luar biasa ini tetaplah tidak bisa menjadi solusi. Setiap penelitian yang dilakukan, semua penemuan yang berhasil didapatkan; sebaliknya justru menguak pada pertanyaan dan permasalahan baru. Pada kenyataannya dengan tangannya ilmu pengetahuan telah mengunci lidah  entitas dan persepsi  manusia.</p>
                            <p>Hari ini adalah sebuah hari yang baru... pencarian hakikat kebenaran   pada saat ini adalah sebagaimana kabar gembira saat menyongsong hadirnya musim semi . Sekaranglah saatnya untuk mengungkap alam semesta dan entitasnya dengan sebuah pendekatan menyeluruh yang sesuai dengan pemahaman dan bahasa zaman. Sekarang adalah waktunya membaca MATA AIR , membaca dengan sinar akal dan cahaya kalbu  serta mencerahkan manusia. </p>
                            <p>MATA AIR menyuguhkan sebuah kesempatan bagi mereka yang ingin membaca dan menginterpretasikan kehidupan dengan benar. MATA AIR adalah sebuah majalah yang menjadi wadah bagi  para ilmuwan terpilih  Indonesia dan Mancanegara, yang berfokus pada manusia, menilai semua aspek-aspek kehidupan dengan sudut pandang yang berbeda dengan menjadikan manusia sebagai pusatnya sehingga menghasilkan pemikiran-pemikiran yang berbeda dan membicarakan hal-hal baru.</p>
                            <p>MATA AIR yang edisi perdananya terbit pada bulan Januari 2014 ini, membawa  ilmu pengetahuan dan informasi yang telah melewati filter kalbu dan akal ke dalam lembaran-lembaran majalah dengan bahasa cinta dan toleransi.  </p>
                            <p>MATA AIR bertujuan untuk mengajak penikmatnya menyadari hubungan antara materi dan esensi, bermula dari karya seni yang ada pada penciptaan menuju pada Sang Pencipta, berbagi pengalaman dan sumbangan ilmu kepada pembacanya agar dapat memberikan kontribusi pada kehidupan dan kemanusiaan.</p>
                            <p>Untuk mewujudkan tujuan ini, MATA AIR yang memiliki pengetahuan  yang mencakup pembahasan semua unit kehidupan, akan menerbitkan artikel-artikel yang membuka cakrawala  dari ilmu-ilmu agama hingga aspek kehidupan sosial, dari ilmu fisika hingga kimia, dimulai dari astronomi sampai pada matematika, dari sejarah sampai geografi, dari pendidikan hingga psikologi, menjelaskan budaya dan terus berujung hingga ke aspek seni, yang kesemua bidang ini diharapkan dapat menambah wawasan berfikir kita. </p>
                            <p>Pada setiap edisinya MATA AIR selalu memberi tempat bagi pendekatan-pendekatan universal yang menyajikan dunia keyakinan kita pada kemanusiaan, menyuguhkan pula contoh-contoh teladan dari peradaban Islam  guna membangun keharmonisan dan perdamaian  global agar para pembacanya mendapatkan bimbingan darinya.</p>
                            <p>Ada banyak alasan bagi kita untuk kembali mengeksplorasi kehidupan, karena  telah ada MATA AIR dalam kehidupan kita. </p>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="redaksi-tab" role="tabpanel" aria-labelledby="redaksi-tab-link">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 mt-4 mb-4 text-center">
                            <p class="font-weight-bold">
                                Penerbit:
                            </p>
                            <h3 class="text-brand">
                                PT Ufuk Baru
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mb-4 text-center">
                            <h4 class="font-weight-bold">
                                Manajemen Majalah Mata Air
                            </h4>
                        </div>
                    </div>
                    <div class="row gal-redaksi">
                        @for($key=0;$key<5;$key++)
                            <div class="col-md-4">
                                {{--<div class="img-box">--}}
                                    {{--<img src="/images/profile-{{$key}}.png" alt="cover majalah mata air">--}}
                                {{--</div>--}}
                                <div class="desc">
                                    <h4 class="text-brand font-weight-bold">
                                        Nama Redaksi {{$key}}
                                    </h4>
                                    <p class="text-uppercase">
                                        Jabatan dari Manajamen Sesuai Nama Diatas
                                    </p>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>

                <div class="tab-pane fade" id="dewan-tab" role="tabpanel" aria-labelledby="dewan-tab-link">
                    <div class="row">
                        <div class="col-12 mt-4 mb-4 text-center">
                            <h4 class="font-weight-bold">
                                Dewan Penasehat
                            </h4>
                        </div>
                    </div>
                    <div class="row gal-redaksi">
                        @for($key=0;$key<13;$key++)
                            <div class="col-md-4">
                                {{--<div class="img-box">--}}
                                    {{--<img src="/images/avatar-default.png" alt="cover majalah mata air">--}}
                                {{--</div>--}}
                                <div class="desc">
                                    <h4 class="text-brand font-weight-bold">
                                        Dewan Penasehat {{$key}}
                                    </h4>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
        // Replace source
        $('img').on("error", function() {
            $(this).attr('src', '/images/avatar-default.png');
        });
    </script>
@endsection


