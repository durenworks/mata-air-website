@extends('layouts.frontend',
            [
                'title'=>'Kontak Kami',
                'active'=>'kontak',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header p-rel mh-300">
        <div class="image-bg">
            <a href="https://www.google.com/maps?ll=-6.274226,106.830521&z=19&t=p&hl=id-ID&gl=US&mapclient=apiv3" class="contact-maps">
                <img src="/images/maps.png" alt="page-cover">
            </a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Kontak Kami</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-12 text-center">
                    <h2 class="text-brand">
                        Hubungi Mata Air
                    </h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-12 mb-3">
                    <h3 class="text-brand">Alamat Kantor :</h3>
                    <p class="mb-3">
                        Jl. Raya Lenteng Agung Rukan Tanjung Mas Raya Esatate Blok B1 No.9<br>
                        Tanjung Barat - Jakarta Selatan - INDONESIA
                    </p>
                    <h3 class="text-brand">Telepon :</h3>
                    <p class="mb-3">
                        021 - 74718039
                    </p>
                    <h3 class="text-brand">Faksimili :</h3>
                    <p class="">
                        021 - 74718039
                    </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-12 mt-4 text-center">
                    <h2 class="text-brand">
                    Get in Touch With Us
                    </h2>
                </div>
                <div class="col-lg-8 offset-lg-2">
                    <ul class="nav nav-tabs nav-justified mt-0" id="contact-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pendapat-tab-link" data-toggle="tab" href="#pendapat-tab" role="tab" aria-controls="pendapat-tab" aria-selected="true">Pendapat anda tentang Mata Air</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="komplain-tab-link" data-toggle="tab" href="#komplain-tab" role="tab" aria-controls="komplain-tab" aria-selected="false">Komplain kepada Mata Air</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-content mb-5" id="contact-tab-content">
                <div class="tab-pane fade show active" id="pendapat-tab" role="tabpanel" aria-labelledby="pendapat-tab-link">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 mt-4 mb-4">
                            <form class="fe-form">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label">Nama *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" placeholder="">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Alamat Email *</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pesan" class="col-sm-3 col-form-label">Pesan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="pesan" id="pesan" rows="7"></textarea>
                                    </div>
                                </div>

                                <div class="row action-tabs mt-5">
                                    <div class="col-sm-6 text-left">
                                        
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <a href="#" class="btn btn-main" id="btn-checkout">Kirim Pesan</a>
                                    </div>
                                </div>
                            
                            </form>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="komplain-tab" role="tabpanel" aria-labelledby="komplain-tab-link">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 mt-4 mb-4">
                            <form class="fe-form">
                                <div class="form-group row">
                                    <label for="id" class="col-sm-3 col-form-label">No ID *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="id" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label">Nama *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" placeholder="">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Alamat Email *</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pesan" class="col-sm-3 col-form-label">Pesan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="pesan" id="pesan" rows="7"></textarea>
                                    </div>
                                </div>

                                <div class="row action-tabs mt-5">
                                    <div class="col-sm-6 text-left">
                                        
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <a href="#" class="btn btn-main" id="btn-checkout">Kirim Pesan</a>
                                    </div>
                                </div>
                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


