@extends('layouts.frontend',
            [
                'title'=>'Karir',
                'active'=>'karir',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-mata-air">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Karir</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        We're Hiring
                    </p>
                    <h1 class="title">Karir di Mata Air</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-12 text-center">
                    <h2 class="text-brand mb-4">
                        Ingin Bergabung Dengan Kami?
                    </h2>
                    <p>Berkolaborasi dengan tim terbaik dalam jurnalisme sains. Semesta kemungkinan menunggu.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Lowongan Karir yang Tersedia</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5">
                    <div class="accordion fe-accordion" id="career-list">
                        @for($key=0;$key<4;$key++)
                            <div class="card">
                                <div class="card-header" id="heading-{{$key}}">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse-{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                                            <li>Lowongan {{$key}}</li>
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse-{{$key}}" class="collapse {{($key == 0) ? ' show' : ''}}" aria-labelledby="heading-{{$key}}" data-parent="#career-list">
                                    <div class="card-body">
                                        <p class="text-brand">Persyaratan yang dibutuhkan :</p>
                                        <ul>
                                            @for($i=0;$i<4;$i++)
                                                <li>Syarat {{$i}}</li>
                                            @endfor
                                        </ul>
                                        <p class="text-right">
                                            <a href="{{route('f.career.form')}}" class="btn btn-main">Ajukan Lamaran</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


