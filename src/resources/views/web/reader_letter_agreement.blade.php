@extends('layouts.frontend',
            [
                'title'=>'Tulis Tetesan Page',
                'active'=>'pembaca',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Tulis Tetesan Anda</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        Find up in
                    </p>
                    <h1 class="title">Tulis Tetesan Anda</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="desc-letter br pr-md-4">
                        <h2 class="text-brand">Aturan Pengiriman Artikel</h2>
                        <p>
                            Editor berhak untuk mengedit dan mengkoreksi semua artikel dan tulisan yang masuk ke redaksi akan tetapi inti dari artikel tersebut akan tetap dipertahankan. Jika banyak terdapat inkonsistensi gaya atau cara penulisan, maka artikel tersebut akan dikembalikan kepada penulis untuk direvisi atau ditinjau lebih lanjut dan selanjutnya dapat diajukan kembali. Semua artikel yang telah masuk ke meja redaksi tidak akan dikembalikan. Semua tulisan yang masuk harus asli atau original dan belum pernah dipublikasikan dalam bentuk apapun.
                        </p>
                        <p>
                            Judul artikel, nama lengkap dan atau nama pena penulis, alamat, nomor telepon, alamat e-mail, latar belakang pendidikan, universitas afiliasi dan jika ada, publikasi lainnya. Artikel tidak boleh lebih dari 1.500-2.000 kata (maksimum 4 halaman). Jika ada gambar atau foto yang berkaitan dengan artikel harus dikirimkan berikut catatan atau keterangan foto tersebut. Abstraksi singkat dari tulisan harus dicantumkan.
                        </p>
                        <p>
                            Sumber, kutipan dan catatan kaki harus sesuai dengan kaidah ilmiah. Jika penulis menggunakan gaya tulisan lain maka semua kutipan harus konsisten dan lengkap. Mengabjadkan referensi bibliografi dalam catatan akhir. Referensi harus berisi : Nama penulis, tahun publikasi, judul, tempat publikasi, penerbit dan nomor halaman. Kutipan dalam teks harus tercantum : nama penulis, tahun, nomor halaman. Kutipan dari kitab suci harus dicetak miring. Memberikan nama bab/surah dan nomor, serta jumlah ayat. Ilustrasi asli atau gambar dikirimkan dalam bentuk hardcopy dan atau softcopy dalam format Adobe Photoshop atau Adobe Illustrator. Bagian dari tulisan-tulisan yang telah dipublikasikan pada majalah ini dapat dikutip dengan mencantumkan judul artikel, edisi dan nama majalah secara lengkap sebagai sumber kutipan.
                        </p>
                        <p>
                            Majalah ini ditujukan untuk pembaca umum. Kata-kata asing atau istilah khusus tertentu dapat dijelaskan dalam glosarium, atau diterjemahkan/ dijelaskan segera setelah kata tersebut. Pendapat dan pandangan yang diungkapkan oleh kontributor majalah adalah pendapat dan pandangan mereka sendiri sebagai penulis, dan tidak selalu mengekspresikan sudut pandang Mata Air , sehingga editor tidak bertanggung jawab atas pandangan tersebut.
                        </p>
                        <p class="mt-3">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="" id="" value="checkedValue" checked>
                                Semua tulisan dapat dikirimkan ke alamat redaksi atau ke e-mail: <a href="mailto:redaksi@majalahmataair.co.id">redaksi@majalahmataair.co.id</a>
                              </label>
                            </div>
                        </p>
                        <p class="text-right  mt-5 mb-5">
                            <a href="#" class="btn btn-primary" id="btn-agreement">Selanjutnya</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
        $('#btn-agreement').attr('href','/reader-letter-form');
        $('.form-check-input').click(function() {
            if ($(this).is(':checked')) {
                $('#btn-agreement').attr('href','/reader-letter-form');
            } else {
                if ($('.form-check-input').filter(':checked').length < 1){
                    $('#btn-agreement').attr('href','#');
                }
            }
        });
    </script>
@endsection


