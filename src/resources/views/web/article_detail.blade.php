@extends('layouts.frontend',
            [
                'title'=>'Article Page',
                'active'=>'kategori',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header p-rel z-1">
        <div class="image-bg">
            <img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{route('f.category',$article->category->slug)}}">Kategori {{ ucwords($article->category->name) }}</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <div class="detail-page-header"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="title-heading-page">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    
                    
                    <div class="bg-box">
                        <div class="bg-line"></div>
                    </div>
                    <div class="bg-white">
                    </div>
                    <p class="category"><a href="{{route('f.category',$article->category->slug)}}">{{ ucwords($article->category->name) }}</a></p>
                    <h2 class="title">{{ ucwords($article->category->title) }}</h2>
                    <p class="author">
                        <a href="#"><i class="fas fa-edit"></i> {{ ucwords($article->author->name) }}</a>
                        <span class="divider">|</span>
                        <a href="#"><i class="fas fa-eye"></i> {{ $article->reader_count }} Reading</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8 br pr-md-4">
                    <div class="row">
                        <div class="col-12">
                            {!!  $article->content !!}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="sharethis-inline-share-buttons"></div>
                        </div>
                    </div>
                    <div class="row pagination-def">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item {{ $previous == '' ? 'disabled' : '' }}">
                                        <a class="page-link" href="{{route('f.article',$previous)}}" tabindex="-1"><i class="fa fa-chevron-left mr-3" aria-hidden="true"></i> Previous Article</a>
                                    </li>
                                    <li class="page-item {{ $next == '' ? 'disabled' : '' }}">
                                        <a class="page-link" href="{{route('f.article',$next)}}">Next Article <i class="fa fa-chevron-right ml-3" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-12 profile-sidebar">
                            <div class="img-round">
                                <img src="/images/blog-1.png" alt="profile-cover">
                            </div>
                            <h4 class="author-name">{{ ucwords($article->author->name) }}</h4>
                            <p class="author-title">{{ ucwords($article->author->occpation) }}</p>
                            <p class="page-date">{{ indonesianDate($article->published_at) }}</p>
                        </div>
                    </div>
                    <div class="row mt-3 mb-3">
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c5123f1249e440017a47332&product=inline-share-buttons' async='async'></script>

@endsection


