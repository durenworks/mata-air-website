@extends('layouts.frontend',
            [
                'title'=>'Gallery Page',
                'active'=>'galeri',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header bg-gallery">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Galeri</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center mb-1">
                    <p class="sub-title">
                        Find up in
                    </p>
                    <h1 class="title mb-4">Galeri Majalah Mata Air</h1>
                    <!-- Nav tabs -->
                    <ul class="nav text-center justify-content-center" id="gallery-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="photos-tab-link" data-toggle="tab" href="#photos-tab" role="tab" aria-controls="photos-tab" aria-selected="true">Photos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="video-tab-link" data-toggle="tab" href="#videos-tab" role="tab" aria-controls="videos-tab" aria-selected="false">Videos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container mt-5 mb-5">
            <!-- Tab panes -->
            <div class="tab-content" id="gallery-tabs-content">
                <div class="tab-pane fade show active" id="photos-tab" role="tabpanel" aria-labelledby="photos-tab">
                    <div class="row">
                        @foreach($photos as $key => $photo)
                            <div class="col-lg-4 col-sm-6">
                                <div class="item">
                                    <div class="img-box">
                                        <a data-fancybox="photos" href="{{ route('show.image',['galleries', $photo->items->first()->image])}}">
                                            <img src="{{ route('show.image',['galleries', $photo->items->first()->image])}}">
                                        </a>
                                    </div>
                                    <div class="d-none">
                                        @foreach($photo->items as $item)
                                            <a data-fancybox="photos" href="{{ route('show.image',['galleries', $item->image])}}"></a>
                                        @endforeach
                                    </div>
                                    <div class="desc">
                                        <h4 class="title">
                                            {{ $photo->tittle }}
                                        </h4>
                                        <p class="info">
                                            <span class="date">{{ indonesianDate($photo->created_at) }}</span>
                                            <span>|</span>
                                            <span class="count">[{{ $photo->items->count() }} Foto]</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row pagination-def mt-5">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="videos-tab" role="tabpanel" aria-labelledby="videos-tab">
                    <div class="row">
                        @foreach($videos as $video)
                            <div class="col-lg-4 col-sm-6">
                                <div class="item">
                                    <div class="img-box">
                                        <iframe width="100%" height="100%" src="{{ $video->url }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                    </div>
                                    <div class="desc">
                                        <h4 class="title">
                                            ${{ $video->tittle }}
                                        </h4>
                                        <p class="info">
                                            <span class="date">{{ indonesianDate($video->created_at) }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row pagination-def mt-5">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
</div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

    <script type="text/javascript">
    </script>
@endsection


