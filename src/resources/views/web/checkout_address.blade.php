@extends('layouts.frontend',
[
'title'=>'Karir',
'active'=>'karir',
'description'=>'Selamat Datang di Majalah Mata Air',
]
)

@section('content-css')
<style type="text/css">
</style>
@endsection

@section('content')

<section class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
          <li class="breadcrumb-item active">Cart</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="main-content" id="checkoutAddress">
  <div class="container">
    <div class="row">
      <div class="title mt-4 w-100">KIRIM KE BEBERAPA ALAMAT</div>
      <div class="subtitle mb-3">Daftar Alamat Pengiriman</div>

      <div class="border-top-1 pt-3 w-100 mt-3 mb-3">
        @include('includes._address_table')
      </div>
    </div>
    @if (count($addresses))
    <div class="row mt-2 mb-3">
      <div class="col-12 border-bottom-1">
        <a href="{{ route('f.checkout.remove_address')}}" class="btn btn-link">Ubah Alamat</a>
      </div>
      <div class="col-3 mt-3 offset-md-9">
        <a href="{{ route('f.checkout') }}" class="btn btn-primary w-100">Lanjut ke Pembayaran</a>
      </div>
    </div>
    @else
    <div class="row mb-3">
      <div class="col-6 text-center">
        <img src="/images/download_icon.png" class="download-icon" />
        <div class="step mt-1 mb-2">Step 1</div>
        <div class="step">Download Formulir</div>
        <div class="step mb-2">Daftar Alamat Pengiriman</div>
        <a href="/docs/mataair_address_template.xlsx" download>
          <img src="/images/download_excel_icon.png" class="download-excel-icon" />
        </a>
      </div>
      <div class="col-6 text-center" style="border-left: 1px solid #DDDDDD">
        <img src="/images/upload_icon.png" class="download-icon" />
        <div class="step mt-1 mb-2">Step 2</div>
        <div class="step">Upload Formulir</div>
        <div class="step mb-2">Daftar Alamat Pengiriman</div>
        <div class="row justify-content-center">
          <div class="col-6 d-flex">
            <img src="/images/upload_file_icon.png" class="w-100" id="uploadFileIcon" />
          </div>
        </div>
        <div class="row justify-content-center mt-2">
          <div class="col-6 d-flex">
            <div class="w-50 pr-1">
              <button href="{{ route('f.checkout.add_address')}}" id="cancelButton"
                class="btn btn-outline-primary w-100">Cancel
              </button>
            </div>
            <div class="w-50">
              <form method="post" action="{{ route('f.checkout.add_address')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" class="hidden" id="formFile" name="file" />
                <button type="submit" id="uploadButton" class="btn btn-primary w-100" disabled="true">Upload
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif

  </div>
</section>

@endsection

@section('modal')
@endsection

@section('content-js')
<script type="text/javascript">
  $('#uploadFileIcon').on('click', function () {
    $('#formFile').click()
  })

  $('#formFile').on('change', function () {
    const value = $(this).val()
    if (value)
      $('#uploadButton').removeAttr('disabled')
    else
      $('#uploadButton').attr('disabled')
  })

  $('#cancelButton').on('click', function () {
    $('#formFile').value = null
    $('#uploadButton').attr('disabled', true)
  })
</script>
@endsection