@extends('layouts.frontend',
            [
                'title'=>'Penulis Page',
                'active'=>'penulis',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="">Penulis</a></li>
                        <li class="breadcrumb-item active">Taufiq Ismail</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <div class="row centering">
                        <div class="col-auto col-xs-12">
                            <div class="img-round">
                                <img src="/images/blog-1.png" alt="profile-cover">
                            </div>
                        </div>
                        <div class="col-auto col-xs-12 text-left">
                            <h4 class="author-name">Taufiq Ismail</h4>
                            <p class="author-title">Contributing Writter</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="gal-blogs br pr-md-4">
                        @for($key=1;$key<=4;$key++)
                            <div class="row item">
                                <div class="col-lg-5 col-md-6">
                                    <div class="img-box">
                                        <a href="{{route('f.article','slug')}}">
                                            <img src="/images/blog-{{$key}}.png" alt="blog-cover">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-6">
                                    <div class="desc">
                                        <p class="kategori"><a href="{{route('f.category','slug')}}">Budaya</a></p>
                                        <h3 class="title">Sekolah Ideal Masa Depan</h3>
                                        <p class="author">
                                            <a href="#"><i class="fas fa-edit"></i> M. fethullah Gulen</a>
                                            <span class="divider">|</span>
                                            Edisi 123 
                                            <span class="divider">|</span>
                                            <a href="#"><i class="fas fa-comment"></i> 10 Reading</a>
                                        </p>
                                        <p class="summary">
                                            {{ str_limit('Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ad, exercitationem beatae fugiat hic cumque nemo soluta quaerat dolor aliquam doloremque perspiciatis sit quis sequi quisquam fugit perferendis veritatis reiciendis!', $limit = 300, $end = '.') }}
                                            <a href="{{route('f.article','slug')}}">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>    
                            </div>
                        @endfor
                    </div>
                    <div class="row pagination-def mt-5 mb-5">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row sidebar p-0">
                        <div class="col-lg-12 col-md-6 col-12 item">
                            <div class="col-12 p-0">
                                <h3 class="title">Tentang Penulis</h3>
                            </div>
                            <div class="col-12 p-0 content mb-2">
                                <p class="desc">
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Asperiores amet reiciendis deserunt exercitationem ducimus officiis minus aut nulla officia repellendus in consequuntur nihil distinctio illo velit itaque impedit, laboriosam ratione.
                                </p>
                            </div>
                        </div>
                    </div>
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


