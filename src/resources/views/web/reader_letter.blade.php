@extends('layouts.frontend',
            [
                'title'=>'Kiriman Pembaca Page',
                'active'=>'pembaca',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Kiriman Tetesan dari Pembaca</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 header-title text-center">
                    <p class="sub-title">
                        Find up in
                    </p>
                    <h1 class="title">Kiriman Tetesan dari Pembaca</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-lg-8">
                    <div class="gal-letter br pr-md-4">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="letter-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link">Kategori :</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All</a>
                            </li>
                            @for($key=0;$key<=2;$key++)
                                <li class="nav-item">
                                    <a class="nav-link" id="category-{{$key}}-tab" data-toggle="tab" href="#category-{{$key}}" role="tab" aria-controls="category-{{$key}}" aria-selected="false">category-{{$key}}</a>
                                </li>
                            @endfor
                        </ul>

                        <div class="row">
                            <div class="col-12">
                                <hr/>
                            </div>
                        </div>

                        <!-- Tab panes -->
                        <div class="tab-content" id="letter-tabs-content">
                            <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                <div class="row m-0">
                                    @for($key2=0;$key2<=5;$key2++)
                                    <div class="col-lg-6">
                                        <div class="item">
                                            <div class="img-box">
                                                <a href="{{route('f.readerletterdetail',$key2)}}">
                                                    <img src="/images/blog-{{$key2}}.png" alt="blog-cover">
                                                </a>
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">Budaya</a></p>
                                                <h3 class="title">Sekolah Ideal Masa Depan</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> M. fethullah Gulen
                                                    <span class="divider">|</span>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> 01/12/2018
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-comment"></i> 10 Reading
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit('Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ad, exercitationem beatae fugiat hic cumque nemo soluta quaerat dolor aliquam doloremque perspiciatis sit quis sequi quisquam fugit perferendis veritatis reiciendis!', $limit = 300, $end = '.') }}
                                                    <a href="{{route('f.readerletterdetail',$key2)}}">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @for($key=0;$key<=2;$key++)
                            <div class="tab-pane fade" id="category-{{$key}}" role="tabpanel" aria-labelledby="category-{{$key}}-tab">
                                <div class="row m-0">
                                    @for($key2=0;$key2<=3;$key2++)
                                    <div class="col-lg-6">
                                        <div class="item">
                                            <div class="img-box">
                                                <a href="{{route('f.readerletterdetail',$key2)}}">
                                                    <img src="/images/blog-{{$key2}}.png" alt="blog-cover">
                                                </a>
                                            </div>
                                            <div class="desc">
                                                <p class="kategori"><a href="#">Budaya</a></p>
                                                <h3 class="title">Sekolah Ideal Masa Depan</h3>
                                                <p class="author">
                                                    <i class="fas fa-edit"></i> M. fethullah Gulen
                                                    <span class="divider">|</span>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i> 01/12/2018
                                                    <span class="divider">|</span>
                                                    <i class="fas fa-comment"></i> 10 Reading
                                                </p>
                                                <p class="summary">
                                                    {{ str_limit('Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ad, exercitationem beatae fugiat hic cumque nemo soluta quaerat dolor aliquam doloremque perspiciatis sit quis sequi quisquam fugit perferendis veritatis reiciendis!', $limit = 300, $end = '.') }}
                                                    <a href="{{route('f.readerletterdetail',$key2)}}">Baca Selengkapnya <i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>


                    <div class="row pagination-def mt-5 mb-5">
                        <div class="col-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('includes._sidebar') 
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


