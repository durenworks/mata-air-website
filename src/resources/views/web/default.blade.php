@extends('layouts.frontend',
            [
                'title'=>'Default Page',
                'active'=>'default',
                'description'=>'Selamat Datang di Majalah Mata Air',
            ]
        )

@section('content-css')
    <style type="text/css">        
    </style>
@endsection

@section('content')

    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>About Page <i class="fa fa-times"></i> </h1>
                    <p class="test-default">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo error quaerat facere voluptatibus velit beatae perferendis optio odio magnam incidunt ea consequuntur expedita placeat, reiciendis at, excepturi numquam non cum.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo error quaerat facere voluptatibus velit beatae perferendis optio odio magnam incidunt ea consequuntur expedita placeat, reiciendis at, excepturi numquam non cum.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo error quaerat facere voluptatibus velit beatae perferendis optio odio magnam incidunt ea consequuntur expedita placeat, reiciendis at, excepturi numquam non cum.
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('modal') 
@endsection

@section('content-js') 
    <script type="text/javascript">
    </script>
@endsection


