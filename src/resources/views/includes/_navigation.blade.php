<nav class="" id="top-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-4 top-socmed text-center text-md-left">
                <a href="https://web.facebook.com/majalahmataair?_rdc=1&_rdr">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="https://twitter.com/majalahmataair">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="#">
                    <i class="fab fa-instagram"></i>
                </a>
                {{--<a href="https://plus.google.com/104921064985589333286">--}}
                {{--<i class="fab fa-google-plus-g"></i>--}}
                {{--</a>--}}
                <a href="https://www.youtube.com/channel/UCVzTv9t_1SVbmH1F1qGBFQw">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
            <div class="col-md-4 text-center tagline">
                Membaca Mata Air Membaca Kehidupan
            </div>
            <div class="col-md-4 text-center text-md-right top-right-menu">
                <ul class="navbar-nav-top">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="world-site" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Mata Air Dunia</a>
                        <div class="dropdown-menu" aria-labelledby="world-site">
                            <a class="dropdown-item" href="http://www.hiramagazine.com/">Arab</a>
                            <a class="dropdown-item" href="http://www.fountainmagazine.com/">Inggris</a>
                            <a class="dropdown-item" href="http://diefontaene.de/">Jerman</a>
                            <a class="dropdown-item" href="http://www.ebrumagazine.com/">Perancis</a>
                            <a class="dropdown-item" href="http://www.noviyegrani.com/">Rusia</a>
                            <a class="dropdown-item" href="http://www.revistacascada.com/">Spanyol</a>
                            <a class="dropdown-item" href="http://www.sizinti.com.tr/">Turki</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="form-search-icon"><i class="fas fa-search"></i></a>
                    </li>
                    @if(\Illuminate\Support\Facades\Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('f.account')}}" id="form-search-icon"><i
                                class="fas fa-user"></i></a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="form-search-icon" data-toggle="modal"
                            data-target="#login-modal"><i class="fas fa-user"></i></a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('f.cart')}}"><i class="fas fa-shopping-bag"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#fullscreen-menu"><i
                                class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="nav-search-form">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4">
                <a class="navbar-brand" href="/">
                    <img src="/images/logo.png" alt="">
                </a>
            </div>
            <div class="col-lg-9 col-md-8">
                <form>
                    <div class="form-row align-items-center">
                        <div class="col-md-10 col-sm-8">
                            <label class="sr-only" for="searchforminput">form pencarian</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-search"></i></div>
                                </div>
                                <input type="text" class="form-control" id="searchforminput"
                                    placeholder="Ketik untuk melakukan pencarian">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <button type="submit" class="btn btn-primary mb-2 btn-block text-uppercase">CARI</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="main-menu">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="/images/logo.png" alt="">
        </a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#main-menu-nav"
            aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="main-menu-nav">
            <ul class="navbar-nav ml-auto">
                {{-- edisi normal link --}}
                <li class="d-lg-none nav-item {{(isset($active) && $active == 'edisi') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.edition')}}">Edisi</a>
                </li>
                {{-- edisi mega menu --}}
                <li class="d-none d-lg-block nav-item {{(isset($active) && $active == 'edisi') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.edition')}}" id="edisi-nav-menu-btn">Edisi</a>
                </li>
                {{-- end edisi link --}}
                <li class="nav-item dropdown {{(isset($active) && $active == 'kategori') ? ' active' : ''}}">
                    <a class="nav-link dropdown-toggle" href="#" id="category-nav-main" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Kategori</a>
                    <div class="dropdown-menu" aria-labelledby="category-nav-main">
                        {{--<a class="dropdown-item" href="{{route('f.category','Sains')}}">Sains</a>--}}
                        @foreach(getCategories() as $category)
                        <a class="dropdown-item"
                            href="{{route('f.category',$category->slug)}}">{{ ucwords($category->name) }}</a>
                        @endforeach
                    </div>
                </li>

                {{-- penulis normal link --}}
                <li class="d-lg-none nav-item {{(isset($active) && $active == 'penulis') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.writer','default')}}">Penulis</a>
                </li>
                {{-- penulis mega menu --}}
                <li class="d-none d-lg-block nav-item {{(isset($active) && $active == 'penulis') ? ' active' : ''}}">
                    <a class="nav-link" href="#" id="penulis-nav-menu-btn">Penulis</a>
                </li>
                {{-- end penulis link --}}

                <li class="nav-item {{(isset($active) && $active == 'galeri') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.gallery')}}">Galeri</a>
                </li>
                <li class="nav-item dropdown {{(isset($active) && $active == 'pembaca') ? ' active' : ''}}">
                    <a class="nav-link dropdown-toggle" href="#" id="pembaca-nav-main" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">Tetesan dari Pembaca</a>
                    <div class="dropdown-menu" aria-labelledby="pembaca-nav-main">
                        <a class="dropdown-item" href="{{route('f.readerlettersubmit')}}">Kirim Artikel</a>
                        <a class="dropdown-item" href="{{route('f.readerletter')}}">Tetes Air</a>
                    </div>
                </li>
                <li class="nav-item {{(isset($active) && $active == 'tentang') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.about')}}">Tentang Kami</a>
                </li>
                <li class="nav-item {{(isset($active) && $active == 'langganan') ? ' active' : ''}}">
                    <a class="nav-link" href="{{route('f.subscribe')}}">Berlangganan</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="edisi-nav-menu">
    <div class="container">
        <div class="row justify-content-center gal-magazine">
            @foreach(getLatestMagazines() as $magazine)
            <div class="col-lg-2">
                <div class="item">
                    <p class="text-uppercase text-center">Edisi {{ $magazine->roman_edition }} Tahun
                        {{ $magazine->year }}</p>
                    <div class="img-box">
                        <a href="{{ route('f.magazine_detail', $magazine->id)}}">
                            <img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}"
                                alt="Majalah Mata Air">
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="{{route('f.edition')}}" class="btn btn-primary">EDISI Selengkapnya <i
                        class="fas fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<div id="penulis-nav-menu">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 pr-0">
                <ul class="nav flex-column nav-pills" id="penulis-pills-tab" role="tablist" aria-orientation="vertical">
                    @for($key=0;$key<5;$key++) <li class="nav-item {{(isset($key) && $key == 0) ? ' lactive' : ''}}">
                        <a class="nav-link {{(isset($key) && $key == 0) ? ' active' : ''}}"
                            id="penulis-pills-{{$key}}-tab" data-toggle="pill" href="#penulis-pills-{{$key}}" role="tab"
                            aria-controls="penulis-pills-{{$key}}" aria-selected="true">Nama Penulis {{$key}} <i
                                class="fa fa-arrow-right float-right" aria-hidden="true"></i> </a>
                        </li>
                        @endfor

                </ul>
            </div>
            <div class="col-lg-9 pl-0">
                <div class="tab-content" id="penulis-pills-tabContent">
                    @for($key=0;$key<5;$key++) <div
                        class="tab-pane fade {{(isset($key) && $key == 0) ? ' show active' : ''}}"
                        id="penulis-pills-{{$key}}" role="tabpanel" aria-labelledby="penulis-pills-{{$key}}-tab">
                        <div class="row gal-blog m-0">
                            @for($i=0;$i<4;$i++) <div class="col-lg-3">
                                <div class="item">
                                    <a href="#">
                                        <div class="img-box">
                                            <img src="/images/blog-{{$i}}.png" alt="blog-cover">
                                        </div>
                                        <div class="desc">
                                            <p class="kategori"><a href="#">Sains</a></p>
                                            <p class="title">Misteri Hibernasi</p>
                                            <p class="author">
                                                <i class="fas fa-edit"></i> Nama Penulis {{$key}}
                                                <br>
                                                <i class="fas fa-comment"></i> 10 Reading
                                                <br>
                                                <span class="edisi">Edisi 123</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                        </div>
                        @endfor()
                </div>
                <div class="row">
                    <div class="col-12 text-center action">
                        <a href="{{route('f.writer',$key)}}" class="btn btn-primary">Selengkapnya <i
                                class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>
</div>
</div>