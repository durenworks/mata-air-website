<!-- Modal Full screen Menu-->
<div class="modal fade" id="fullscreen-menu" tabindex="-1" role="dialog" aria-labelledby="fullscreen-menu" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a class="navbar-brand" href="/">
                    <img src="/images/logo.png" alt="" height="60px">
                </a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row mb-3">
                        <div class="col-lg-4 col-md-6">
                            <ul class="fs-nav-menu coloring-brand">
                                <li>
                                    <a href="{{route('f.edition')}}">Edisi</a>
                                </li>
                                <li>
                                    <a href="{{route('f.category','default')}}">Kategori</a>
                                </li>
                                <li>
                                    <a href="{{route('f.writer','default')}}">Penulis</a>
                                </li>
                                <li>
                                    <a href="{{route('f.gallery')}}">Galeri</a>
                                </li>
                                <li>
                                    <a href="{{route('f.readerletter')}}">Tetes Air</a>
                                </li>
                                <li>
                                    <a href="{{route('f.subscribe')}}">Berlangganan</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <ul class="fs-nav-menu">
                                <li>
                                    <a href="{{route('f.about')}}">Tentang Kami</a>
                                </li>
                                <li>
                                    <a href="{{route('f.career')}}">Karir</a>
                                </li>
                                <li>
                                    <a href="{{route('home')}}">Bantuan</a>
                                </li>
                                <li>
                                    <a href="{{route('f.tos')}}">Kebijakan Privasi</a>
                                </li>
                                <li>
                                    <a href="{{route('f.faq')}}">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{route('f.tos')}}">Ketentuan Penggunaan</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-4 col-12 mb-4">
                                    <h3>Buletin Mata Air</h3>
                                    <p>Dapatkan berita terbaru yang dikirmkan ke kotak masuk Anda.</p>
                                    <div>
                                        <form class="newsletter-form">
                                            <div class="form-row align-items-center">
                                                <div class="col-md-8 col-sm-7">
                                                    <div class="input-group">
                                                        <input type="email" class="form-control" id="newsletter-form-input" placeholder="Masukkan email anda">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-5">
                                                    <button type="submit" class="btn btn-block">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-4 col-12 mb-4">
                                    <div class="col-12">
                                        <p class="title">Follow Majalah Mata Air</p>
                                    </div>
                                    <div class="col-12 content text-center social-media">
                                        <a href="https://web.facebook.com/majalahmataair?_rdc=1&_rdr">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a href="https://twitter.com/majalahmataair">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                        <a href="https://plus.google.com/104921064985589333286">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                        <a href="https://www.youtube.com/channel/UCVzTv9t_1SVbmH1F1qGBFQw">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                </div>
                                <?php
                                    $magazines = getLatestMagazines();
                                    if ($magazines) {
                                        $magazine = $magazines->first();
                                    }

                                ?>
                                <div class="col-lg-12 col-md-4 col-12">
                                    @if ($magazine)
                                        <div class="row">
                                            <div class="col-lg-5 col-md-6 col-12">
                                                <div class="img-box">
                                                    <a href="#">
                                                        <img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}" alt="Sampul Majalah">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-6 col-12">
                                                <h3 class="title">Majalah Mata Air Edisi Terbaru:</h3>
                                                <p>
                                                    <strong><span>"{{ ucwords(getMonth($magazine->published_at)) . ' ' . $magazine->year }}"</span></strong>
                                                    <a href="#"><i class="fas fa-arrow-right"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3 class="app-title">Majalah Digital Mata Air</h3>
                            <div class="col-12 content text-center download-app">
                                <div class="img-box">
                                    <a href="#">
                                        <img src="/images/google-play.png" alt="Get on Google Play">
                                    </a>
                                </div>
                                <div class="img-box">
                                    <a href="#">
                                        <img src="/images/app-store.png" alt="Get on Apple Store">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer ">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p>
                                &copy;  2018 <a href="http://majalahmataair.co.id"  target="_blank">majalahmataair.co.id</a> All rights reserved
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Login -->
<div class="modal fade auth-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Masuk" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12 form-title text-center">
                            <h2>Masuk</h2>
                            <div class="img-box">
                                <img src="/images/logo.png" alt="" height="60px">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 col-12 form-auth">
                            <form method="post" action="{{ route('login.post') }}">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <input type="email" class="form-control" name="email" id="login-email" aria-describedby="email-help" placeholder="Username / email">
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="login-password" placeholder="Password">
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="check-auth">
                                    <label class="form-check-label" for="check-auth">Biarkan Saya Tetap Masuk</label>
                                </div>
                                {!! Form::token() !!}
                                <button type="submit" class="btn btn-block btn-primary text-uppercase">Log In</button>
                            </form>
                        </div>
                        <div class="col-12 form-info text-center">
                            <p class="forgot-auth">
                                <a href="#">lupa kata sandi?</a>
                            </p>
                            <p class="register">
                                Belum punya akun?. <a href="#" data-dismiss="modal"  data-toggle="modal" data-target="#register-modal">Buat Akun Sekarang</a>
                            </p>
                            {{--<div class="row login-oauth align-items-center">--}}
                                {{--<div class="col-6 text-right">--}}
                                    {{--<p>--}}
                                        {{--or sign in with--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                                {{--<div class="col-6 text-left">--}}
                                    {{--<p>--}}
                                        {{--<a href="#"><i class="fab fa-facebook-f"></i></a>--}}
                                        {{--<span class="divider">|</span>--}}
                                        {{--<a href="#"><i class="fab fa-google-plus-g"></i></a>--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- Modal Register -->
<div class="modal fade auth-modal" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="Daftar" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12 form-title text-center">
                            <h2>Masuk</h2>
                            <div class="img-box">
                                <img src="/images/logo.png" alt="" height="60px">
                            </div>
                        </div>
                        <div class="col-md-8 offset-md-2 col-12 form-auth">
                            <form method="post" action="{{ route('register.post') }}">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="name" id="register-name" aria-describedby="name-help" placeholder="Name">
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <input type="email" class="form-control" name="email" id="register-email" aria-describedby="email-help" placeholder="email">
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                    <input type="password" class="form-control" name="password" id="register-password" placeholder="Password">
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                    <input type="password" class="form-control" name="password_confirmation" id="re-register-password" placeholder="Retpr Password">
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group">
                                    <p class="text-justify"><small>
                                        Membuat akun berarti anda menerima <a href="#">Syarat & Ketentuan</a> dan <a href="#">Kebijakan Privasi</a> Majalah Mata Air    
                                    </small></p>
                                </div>
                                {!! Form::token() !!}
                                <button type="submit" class="btn btn-block btn-primary text-uppercase">Buat Akun Saya</button>
                            </form>
                        </div>
                        <div class="col-12 form-info text-center">
                            <p class="register">
                                Sudah Punya Akun?. <a href="#" data-dismiss="modal"  data-toggle="modal" data-target="#login-modal">Masuk di sini</a>
                            </p>
                            <div class="row login-oauth align-items-center">
                                <div class="col-6 text-right">
                                    <p>
                                        or sign in with
                                    </p>
                                </div>
                                <div class="col-6 text-left">
                                    <p>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <span class="divider">|</span>
                                        <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
