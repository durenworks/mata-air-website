<div class="d-flex justify-content-center w-100">
  <div class="payment-method">
    <div class="col-12 text-center">
      <h4 class="text-brand">Metode Pemabayaran Paket Berlangganan Majalah Mata Air</h4>
    </div>
    <ul class="nav nav-pills justify-content-center" id="payment-method-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="card-tab-link" data-toggle="tab" href="#card-tab" role="tab"
          aria-controls="card-tab" aria-selected="true">Card Payment</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="bank-tab-link" data-toggle="tab" href="#bank-tab" role="tab" aria-controls="bank-tab"
          aria-selected="false">Bank Transfer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="cardless-tab-link" data-toggle="tab" href="#cardless-tab" role="tab"
          aria-controls="cardless-tab" aria-selected="false">Cardless Credit</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="direct-tab-link" data-toggle="tab" href="#direct-tab" role="tab"
          aria-controls="direct-tab" aria-selected="false">Direct Debit</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="ewallet-tab-link" data-toggle="tab" href="#ewallet-tab" role="tab"
          aria-controls="ewallet-tab" aria-selected="false">e-Wallet Debit</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="counter-tab-link" data-toggle="tab" href="#counter-tab" role="tab"
          aria-controls="counter-tab" aria-selected="false">Over the Counter</a>
      </li>
    </ul>

    <div class="tab-content" id="payment-method-tab-content">
      <div class="tab-pane fade show active" id="card-tab" role="tabpanel" aria-labelledby="card-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran online dari kartu kredit/debit dari semua Bank yang berlogo
              VISA/MasterCard/JCB/Amex.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/card.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="bank-tab" role="tabpanel" aria-labelledby="bank-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran transfer melalui ATM, mobile, atau internet banking, dengan notifikasi real-time.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/bank.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="cardless-tab" role="tabpanel" aria-labelledby="cardless-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran cicilan tanpa kartu kredit.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/cardless.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="direct-tab" role="tabpanel" aria-labelledby="direct-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran internet banking dari berbagai Bank ternama.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/direct.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="ewallet-tab" role="tabpanel" aria-labelledby="ewallet-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran e-Wallet dari akun/nomor/PIN ponsel pelanggan.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/ewallet.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="counter-tab" role="tabpanel" aria-labelledby="counter-tab-link">
        <div class="row">
          <div class="col-12 text-center">
            <p>
              Terima pembayaran dari Toserba dan Kios di seluruh Indonesia.
            </p>
          </div>
          <div class="col-12">
            <div class="img-box">
              <img src="/images/kios.png" class="op-center" alt="logo">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>