<div class="sidebar" id="{{(isset($page) && $page == 'home') ? ' ' : 'page-sidebar'}}">
	<div class="row">
		<div class="col-lg-12 col-md-6 col-12 item {{(isset($page) && $page == 'home') ? ' ' : ' d-none'}}">
			<div class="col-12 p-0">
				<h3 class="title">Galeri Video Terbaru</h3>
			</div>
			<div class="col-12 p-0 content">
				<div class="vid-box">
					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/TA6XrTjLB9Q" frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<div class="col-lg-12 col-md-6 col-12 item">
			<div class="col-12 p-0">
				<h3 class="title">Majalah Digital Mata Air</h3>
			</div>
			<div class="col-12 p-0 content download-app">
				<div class="img-box">
					<a href="#">
						<img src="/images/google-play.png" alt="Get on Google Play">
					</a>
				</div>
				<div class="img-box">
					<a href="#">
						<img src="/images/app-store.png" alt="Get on Apple Store">
					</a>
				</div>
			</div>
		</div>

		<div class="col-lg-12 col-md-6 col-12 item">
			<div class="col-12 p-0">
				<h3 class="title">Follow Majalah Mata Air</h3>
			</div>
			<div class="col-12 p-0 content social-media">
				<a href="#">
					<i class="fab fa-facebook-f"></i>
				</a>
				<a href="#">
					<i class="fab fa-twitter"></i>
				</a>
				<a href="#">
					<i class="fab fa-instagram"></i>
				</a>
				{{--<a href="#">--}}
				{{--<i class="fab fa-google-plus-g"></i>--}}
				{{--</a>--}}
				<a href="#">
					<i class="fab fa-youtube"></i>
				</a>
			</div>
		</div>

		<?php
        $magazines = getLatestMagazines()->take(3);
        $magazine = $magazines->first();
        ?>

		{{--<div class="col-lg-12 col-md-6 col-12 item">--}}
		{{--<div class="col-12 p-0">--}}
		{{--<h3 class="title">Edisi Terbaru Majalah Mata Air</h3>--}}
		{{--</div>--}}
		{{----}}
		{{--<div class="col-12 p-0 content side-majalah">--}}
		{{--@if (!empty($magazine))--}}
		{{--<p class="title">--}}
		{{--"{{ getMonth($magazine->published_at) . ' '. $magazine->year}} "--}}
		{{--</p>--}}
		{{--<div class="img-box">--}}
		{{--<a href="#">--}}
		{{--<img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}">--}}
		{{--</a>--}}
		{{--</div>--}}
		{{--@else--}}
		{{--<p class="edition">--}}
		{{--Data tidak ditemukan--}}
		{{--</p>--}}
		{{--@endif--}}
		{{--</div>--}}
		{{--</div>--}}

		<div class="col-lg-12 col-md-6 col-12 item {{(isset($page) && $page == 'home') ? ' d-none' : ''}}">
			<div class="col-12 p-0">
				<h3 class="title">Most Read Article</h3>
			</div>
			<div class="col-12 p-0 featured-mini-post">
				@forelse(getLatestArticles() as $article)
				<div class="item">
					<a href="#">
						<div class="row">
							<div class="col-auto">
								<div class="img-box">
									<img src="{{route('show.image', ['articles', $article->thumbnail])}}" alt="{{ $article->title }}">
								</div>
							</div>
							<div class="col">
								<div class="desc">
									<h3 class="title">{{ $article->title }}</h3>
									<p class="author">
										<i class="fas fa-edit"></i> {{ $article->author->name }}
									</p>
								</div>
							</div>
						</div>
					</a>
				</div>
				@empty
				<div class="item">
					<p class="edition">
						<i class="fas fa-edit"></i> Tidak ada data ditemukan
					</p>
				</div>
				@endforelse

			</div>
		</div>
	</div>

	<div class="row {{(isset($page) && $page == 'home') ? ' ' : ' d-none'}}">
		<div class="col-lg-12 col-md-6 col-12 item mb-2">
			<div class="col-12 p-0">
				<h3 class="title">Edisi Terbaru Majalah Mata Air</h3>
			</div>
		</div>
		@forelse($magazines as $magazine)
		<div class="col-lg-12 col-md-6 col-12 item">
			<div class="col-12 p-0 content side-majalah mt-lg-0 mt-md-5 mt-0">
				<p class="edition">
					Edisi {{ $magazine->roman_edition }} Tahun {{ $magazine->year }}
				</p>
				<div class="img-box">
					<a href="#">
						<img src="{{route('show.image', ['magazines_cover', $magazine->cover])}}" alt="Majalah Mata Air">
					</a>
				</div>
				{{--<p class="mt-4">--}}
				{{--<a href="#">--}}
				{{--Lihat Selengkapnya--}}
				{{--</a>--}}
				{{--</p>--}}
			</div>
		</div>
		@empty
		<div class="col-lg-12 col-md-6 col-12 item">
			<div class="col-12 p-0 content side-majalah mt-lg-0 mt-md-5 mt-0">
				<p class="edition">
					Data Tidak Ditemukan
				</p>
			</div>
		</div>
		@endforelse
	</div>
</div>