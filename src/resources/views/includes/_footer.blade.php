

<section class="about-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 text-center">
                <div class="img-box">
                    <img src="/images/logo-brown.png" alt="logo">
                </div>
                <h3 class="text-uppercase title">Tentang Majalah Mata Air</h3>
                <p class="text-justify sub-title">
                    Mengapa kata mata air yang diusung sebagai nama dari majalah ini, memiliki sejarah panjang, namun pemikiran yang ingin diutarakan pada pembacanya adalah karena dalam kehidupan kita mata air adalah salah satu sumber utama kehidupan yang bersumber dari Rahmat Ilahi, ia akan mengalirkan nafas kejernihan pada banyak elemen hidup, maka berangkat dari filosofi ini Majalah MATA AIR diharapkan dapat mengetengahkan esensi kebenaran dan mengalirkan keindahannya serta kesejukannya ke dalam berbagai cabang ilmu.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <p>
                    &copy;  2018 <a href="http://majalahmataair.co.id"  target="_blank">majalahmataair.co.id</a> All rights reserved
                </p>
            </div>
            <div class="col-md-6 text-center text-md-right">
                <p>
                    <a href="{{route('f.about')}}" target="_blank">About Us</a>
                    <a href="{{route('home')}}" target="_blank">Help</a>
                    <a href="{{route('f.tos')}}" target="_blank">Privacy Policy</a>
                    <a href="{{route('f.faq')}}" target="_blank">FAQ</a>
                    <a href="{{route('f.tos')}}" target="_blank">Term Of Use</a>
                </p>
            </div>
        </div>
    </div>
    <div class="go-to-top">
        <i class="fas fa-chevron-up"></i>
    </div>
</section>