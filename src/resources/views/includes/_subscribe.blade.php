<section class="subscribe-form">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <h3 class="text-uppercase title">Majalah Mata Air Newsletter</h3>
                <p class="sub-title">Dapatkan kabar dari berita paling penting yang dikirimkan ke kotak masuk email Anda</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-lg-2">
                <div class="container">
                    <form>
                        <div class="form-group row align-items-center">
                            <div class="col-sm-8 p-0">
                                <label for="formsubscribe" class="sr-only">Subsribe</label>
                                <input type="email" class="form-control" id="formsubscribe" placeholder="Masukan Email Anda">
                            </div>
                            <div class="col-sm-4 p-0">
                                <button type="submit" class="btn btn-block btn-main">Berlangganan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>