<table class="table table-bordered table-sm font-size-12">
  <thead class="thead-light">
    <tr>
      <td width='30px'>No</td>
      <td width='70px'>Nama Pelanggan</td>
      <td width='70px'>Alamat</td>
      <td width='40px'>Nama Perumahan</td>
      <td width='30px'>RT</td>
      <td width='30px'>RW</td>
      <td width='40px'>Desa</td>
      <td width='40px'>Kelurahan</td>
      <td width='40px'>Kecamatan</td>
      <td width='50px'>Kabupaten</td>
      <td width='50px'>Propinsi</td>
      <td width='60px'>Kode Pos</td>
      <td width='70px'>Nomor Handphone</td>
    </tr>
  </thead>
  <tbody>
    @forelse ($addresses as $key => $address)
    <tr>
      <td>{{ $key + 1 }}</td>
      <td>{{ $address->name }}</td>
      <td>{{ $address->street }}</td>
      <td>{{ $address->residence }}</td>
      <td>{{ $address->rt }}</td>
      <td>{{ $address->rw }}</td>
      <td>{{ $address->village }}</td>
      <td>{{ $address->urban_village }}</td>
      <td>{{ $address->subdistrict }}</td>
      <td>{{ $address->city }}</td>
      <td>{{ $address->province }}</td>
      <td>{{ $address->postal_code }}</td>
      <td>{{ $address->handphone }}</td>
    </tr>
    @empty
    <tr style="height: 30px">
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @endforelse
  </tbody>
</table>