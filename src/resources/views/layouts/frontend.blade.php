<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <link rel="icon" href="/favicon.ico">

        <meta name="description" content="{{{ isset($description) ? $description : 'Majalah Mata Air.' }}}">
        <meta name="author" content="{{{ isset($author) ? $author : 'Majalah Mata Air' }}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{{ isset($title) ? $title : 'Majalah Mata Air' }}} | Majalah Mata Air</title>

        <link href="{{ mix('css/app-frontend.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

        <link href="{{ mix('css/style.css') }}" rel="stylesheet">
        <link href="{{ mix('css/custom.css')}}" rel="stylesheet" />
        
        <style type="text/css">
            div{
                /* border: 1px solid red; */
            }
        </style>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        {{-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> --}}

        <!-- Custom styles for this template -->
        @yield("content-css")

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <header class="sticky-top">
            @include('includes._navigation' )
        </header>

        <div class="main-content-wrapper">
            @yield('content')
        </div>

        @include('includes._subscribe')  
        <footer>
            @include('includes._footer')  
        </footer>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        @include('includes._main-modal')
        @yield('modal')

        <script src="{{ mix('js/app-frontend.js') }}"></script>
        <script src="{{ mix('js/custom.js') }}"></script>
        
        <script>
            // $('#login-modal').modal('show');
        </script>
        @yield('content-js')
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        {{-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> --}}
    </body>
</html>
