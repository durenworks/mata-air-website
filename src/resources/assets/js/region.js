// set global moustache tags
Mustache.tags = ['{%', '%}'];

$(function () {
    $('#province').change();
});

$('#province').on('change', function(e){
    let id = $(this).val();
    let url = $('#city_form').attr('href');
    //console.log(url);
    $('#city').prop('disabled', true);
    $.ajax({
        url: url,
        method: "get",
        data: { province: id},
        success: function(data){
            data = JSON.parse(data);
            let template = "{% #list %}<option value='{% city_id %}'>{% type %} {% city_name %}</option>{% /list %}";

            // because Mustache doesn't like anonymous arrays of objects
            let rendered_template = Mustache.to_html(template, {"list":data} );

            $('#city').html(rendered_template);

            bind();
            let selectedCity = $('#hidSelectedCity').val();

            //console.log(selectedCity);
            if (selectedCity != '' || selectedCity != '0') {
                //console.log(selectedCity);
                $('#city').val(selectedCity);
                $('#hidSelectedCity').val('0');

                //$('#city').change();
            }

            $('#city').trigger('change');

            if(id === '' || id == null)
                $('#city').prop('disabled', true);
            else
                $('#city').prop('disabled', false);
        }
    });
});

$('#city').on('change', function(e){
    let id = $(this).val();
    let url = $('#subdistrict_form').attr('href');
    $('#subdistrict').prop('disabled', true);
    $.ajax({
        url: url,
        method: "get",
        data: { city: id},
        success: function(data){
            //console.log(data);
            try {
                data = JSON.parse(data);

                let template = "{% #list %}<option value='{% subdistrict_id %}'>{% subdistrict_name %}</option>{% /list %}";
                // because Mustache doesn't like anonymous arrays of objects
                let rendered_template = Mustache.to_html(template, {"list": data});
                $('#subdistrict').html(rendered_template);

                bind();

                if (id === '' || id == null)
                    $('#subdistrict').prop('disabled', true);
                else {
                    $('#subdistrict').prop('disabled', false);
                }
            }
            catch (e) {
                console.log(e);
            }
        }
    });
});

function bind(){
    $('#city').off('change', getSubdistrict).on('change', getSubdistrict);
}

function getSubdistrict(){
    let id = $(this).val();
    let url = $('#subdistrict_form').attr('href');
    //console.log(url);
    if (url != null) {
        $.ajax({
            url: url,
            method: "get",
            data: {city: id},
            success: function (data) {
                data = JSON.parse(data);

                let template = "{% #list %}<option value='{% subdistrict_id %}'>{% subdistrict_name %}</option>{% /list %}";
                // because Mustache doesn't like anonymous arrays of objects
                let rendered_template = Mustache.to_html(template, {"list": data});
                $('#subdistrict').html(rendered_template);

                bind();

                if (id == '' || id == null)
                    $('#subdistrict').prop('disabled', true);
                else
                    $('#subdistrict').prop('disabled', false);

            }
        });
    }
}