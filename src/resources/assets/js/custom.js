$(document).ready(function () {
    $('.go-to-top').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    }); 
    $(window).trigger('resize');
});
$(window).on('resize', function () {
    // do whatever
    var getCarouselHeight = $('#home-slider .carousel-caption').height();
    console.log(getCarouselHeight);
    $('#home-slider .carousel-indicators').css('bottom',getCarouselHeight);
});

$("#form-search-icon").click(function() {
    $(".nav-search-form").toggle( "slow" );
});
$("#edisi-nav-menu-btn").click(function (e) {
    e.preventDefault();
    $("#edisi-nav-menu").toggle("slow");
});
$("#penulis-nav-menu-btn").click(function (e) {
    e.preventDefault();
    $("#penulis-nav-menu").toggle("slow");
});
$("#penulis-pills-tab .nav-link").click(function (e) { 
    e.preventDefault();
    $('#penulis-pills-tab .nav-item').removeClass('lactive');
    $(this).parent().addClass('lactive')

});