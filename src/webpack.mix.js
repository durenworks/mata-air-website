let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(webpack => {
  return {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default']
      })
    ]
  }
})

mix
  .js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/custom.js', 'public/js')
  .js('resources/assets/js/app-frontend.js', 'public/js')
  .js('resources/assets/js/region.js', 'public/js')
  .scripts(
    ['resources/assets/js/vendor/mustache/mustache.js'],
    'public/js/mustache.js'
  )
  .sass('resources/assets/sass/app-frontend.scss', 'public/css')
  .sass('resources/assets/sass/app.scss', 'public/css')
  // .less('resources/assets/less/style.less', 'public/css')
  .sass('resources/assets/sass/frontend/style.scss', 'public/css')
  .styles(['resources/assets/css/style.css'], 'public/css/custom.css')
  .copyDirectory('resources/assets/static/images', 'public/images')
  .copyDirectory('resources/assets/images', 'public/images')
  .copyDirectory('resources/assets/webfonts', 'public/webfonts')
  .browserSync('laradminator.local')
  .version()
  .sourceMaps()
